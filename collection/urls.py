"""collection URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django.shortcuts import redirect
from core.viewsets import ProtectedTemplateView
from django.conf import settings
from django.conf.urls.static import static
from app.views.webhook import C2CEventWebhookView, C2CCallerInfoView, RazorpayWebhookView

urlpatterns = [
    path('', lambda x: redirect('/customer/')),
    path('api/', include('app.urls')),
    path('utility/admin/', admin.site.urls),
    path('login/', TemplateView.as_view(template_name='registration/login.html'), name='login'),
    path('import-export/', ProtectedTemplateView.as_view(template_name='import-export.html'), name='import-export'),
    path('customer/', ProtectedTemplateView.as_view(template_name='customer.html'), name='customer'),
    path('customer/edit/', ProtectedTemplateView.as_view(template_name='customer-details.html'), name='customer-details'),
    path('notice-issued/', ProtectedTemplateView.as_view(template_name='notice-issued.html'), name='notice-issued'),
    path('postcard-issued/', ProtectedTemplateView.as_view(template_name='postcard-issued.html'), name='postcard-issued'),
    path('dashboard/', ProtectedTemplateView.as_view(template_name='admin-dashboard.html'), name='dashboard'),
    path('dashboard-grafana/', ProtectedTemplateView.as_view(template_name='dashboard-grafana.html'), name='dashboard-grafana'),
    path('user/dashboard/', ProtectedTemplateView.as_view(template_name='user-dashboard.html'), name='user-dashboard'),
    path('user/callback/', ProtectedTemplateView.as_view(template_name='callback.html'), name='callback'),
    path('user/ptp/', ProtectedTemplateView.as_view(template_name='ptp.html'), name='ptp'),
    path('user/emi-followup/', ProtectedTemplateView.as_view(template_name='emi-followup.html'), name='emi-followup'),
    path('manage-lead/', ProtectedTemplateView.as_view(template_name='manage-lead.html'), name='manage-lead'),
    path('payment/', ProtectedTemplateView.as_view(template_name='payment.html'), name='payment'),
    path('settlement/', ProtectedTemplateView.as_view(template_name='settlement.html'), name='settlement'),
    path('data', C2CEventWebhookView.as_view(), name='c2c_event_api_view'),
    path('get-caller-info', C2CCallerInfoView.as_view(), name='get-caller-info'),
    path('webhook/razorpay/payment-link/', RazorpayWebhookView.as_view(), name='webhook/payment-link/'),
    path('payment-link/', ProtectedTemplateView.as_view(template_name='payment-link.html'), name='payment-link'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = 'LINTEL'
admin.site.site_title = 'LINTEL'
