"""
Created:          19/06/2020
Post-History:
Title:            This module is intended to provide Application Logs.
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

from .config import CFG
from django.conf import settings
import os

log_path = CFG['log']['log-path']
file_path = os.path.join(log_path, CFG['log']['file-name'])

if not os.path.isdir(log_path):
    os.makedirs(log_path)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': CFG['log']['disable_existing_loggers'],
    'formatters': {
        'simple': {
            'format': '{asctime}-{name}-{levelname} : {message}',
            'style': '{',
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': file_path,
            'formatter': 'simple',
            'backupCount': CFG['log']['backupCount'],
            'maxBytes': CFG['log']['maxBytes']
        },
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['file', 'console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'django.security': {
            'handlers': ['file', 'console'],
            'level': 'ERROR',
            'propagate': False,
        },
        '': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}


# Send error logs to Slack, if not local.
if not CFG['main']['debug']:
    LOGGING['handlers']['slack-error'] = {
            'level': 'ERROR',
            'api_key': settings.SLACK_BOT_USER_TOKEN,
            'class': 'slacker_log_handler.SlackerLogHandler',
            'channel': settings.CHANNEL,
            'username': settings.SLACK_USER_NAME,
            # 'icon_url': '',
            'icon_emoji': ':exclamation:',
        }
    LOGGING['loggers']['django.request']['handlers'].append('slack-error')
    LOGGING['loggers']['django.security']['handlers'].append('slack-error')
