"""
Created:          19/06/2020
Post-History:
Title:            This module is intended to provide Configuration Settings
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

from YamJam import yamjam

CFG = yamjam('/etc/debt-collection-utility/config.yaml')
