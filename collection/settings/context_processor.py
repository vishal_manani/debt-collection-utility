from django.conf import settings
from app.helper import user_group_list


def static_file_version(request):
    return {'version': settings.RESTART_TIME}


def get_user_group_list(request):
    return user_group_list(request)