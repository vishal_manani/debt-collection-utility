"""
Created:          19/06/2020
Post-History:
Title:            This module is intended to provide Database Connectivity
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

from .config import CFG

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': CFG['database']['name'],
        'USER': CFG['database']['username'],
        'PASSWORD': CFG['database']['password'],
        'HOST': CFG['database']['host'],
        'PORT': CFG['database']['port'],
    }
}