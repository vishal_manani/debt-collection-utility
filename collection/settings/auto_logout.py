from django.utils.deprecation import MiddlewareMixin
from django.contrib.auth import logout


class AdminRestrictMiddleware(MiddlewareMixin):
    def process_request(self, request):
        access_token = request.COOKIES.get('access_token', None)
        if not access_token:
            logout(request)
        else:
            return
