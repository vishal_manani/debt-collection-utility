"""
Created:          10/07/2020
Post-History:
Title:            This module is intended to provide Db update based on Webhook Notifications.
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import logging
from .models import ClickToCallEventDetail
from core.response import success_response, error_response
from django.db import IntegrityError

log = logging.getLogger("os_dialer_event")

EVENT_MAP = dict()


def register(api_name):
    def wrapper(api_function):
        EVENT_MAP[api_name] = api_function
        return api_function
    return wrapper


@register('update_ivr_data')
def update_ivr_data(event):
    try:
        uuid = event.get('UUID', None)
        event_name = event.get('Event-Name', None)
        timestamp = event.get('Timestamp', None)
        file_name = event.get('file_name', None)
        duration = event.get('duration', 0)
        ivr_name = event.get('ivr_name', None)
        call_status = event.get('call_status', None)
        mos = event.get('mos', 0)
        rtp_in_count = event.get('rtp_in_count', 0)
        cause_code = event.get('cause_code', 0)
        hangup_cause = event.get('hangup_cause', None)
        ClickToCallEventDetail.objects.create(
            uuid_id=uuid,
            event_name=event_name,
            timestamp=timestamp,
            file_name=file_name,
            duration=duration,
            ivr_name=ivr_name,
            call_status=call_status,
            mos=mos,
            rtp_in_count=rtp_in_count,
            cause_code=cause_code,
            hangup_cause=hangup_cause
        )
        return success_response('Successfully Received Event Notification')
    except IntegrityError as exc:
        log.error("[os_dialer_event -> UUID not found ]" + str(exc))
        return error_response('UUID not found!', errors=str(exc), status_code=404)
    except Exception as exc:
        log.error("[os_dialer_event -> update_ivr_data ]" + str(exc))
        raise
