"""
Created:          24/06/2020
Post-History:     09/07/2020
Title:            This module is intended to provide Collection Models.
"""

__version__ = '0.0.1'
__author__ = 'Vishal Manani'

from django.db import models
from .user import EmailUser


class Disposition(models.Model):
    name = models.CharField(max_length=250, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)


class SubDisposition(models.Model):
    name = models.CharField(max_length=250, null=True)
    disposition = models.ForeignKey(Disposition, null=True, on_delete=models.CASCADE, related_name='disposition')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)


class PaymentMode(models.Model):
    name = models.CharField(max_length=250, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)


class SubPaymentMode(models.Model):
    name = models.CharField(max_length=250, null=True)
    payment_mode = models.ForeignKey(PaymentMode, null=True, on_delete=models.CASCADE, related_name='payment_mode')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)


class PTPType(models.Model):
    name = models.CharField(max_length=250, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)


class Customer(models.Model):
    application_id = models.CharField(max_length=250, primary_key=True)
    loan_agreement_number = models.CharField(max_length=250, null=True, blank=True)
    name = models.CharField(max_length=250, null=True, blank=True)
    last_name = models.CharField(max_length=250, null=True, blank=True)
    address = models.TextField(max_length=250, null=True, blank=True)
    alternate_address = models.TextField(max_length=250, null=True, blank=True)
    permanent_address = models.TextField(max_length=250, null=True, blank=True)
    contact_number_1 = models.CharField(max_length=15, null=True, blank=True)
    contact_number_2 = models.CharField(max_length=15, null=True, blank=True)
    contact_number_3 = models.CharField(max_length=15, null=True, blank=True)
    branch = models.CharField(max_length=250, null=True, blank=True)
    product = models.CharField(max_length=250, null=True, blank=True)
    asset_make = models.CharField(max_length=250, null=True, blank=True)
    registration_number = models.CharField(max_length=250, null=True, blank=True)
    loan_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    loan_booking_date = models.CharField(max_length=250, null=True, blank=True)
    loan_maturity_date = models.CharField(max_length=250, null=True, blank=True)
    disbursal_date = models.CharField(max_length=250, null=True, blank=True)
    allocation_date = models.CharField(max_length=250, null=True, blank=True)
    emi_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    reference_1_name = models.CharField(max_length=250, null=True, blank=True)
    reference_1_contact_number = models.CharField(max_length=15, null=True, blank=True)
    reference_1_address = models.TextField(max_length=250, null=True, blank=True)
    reference_2_name = models.CharField(max_length=250, null=True, blank=True)
    reference_2_contact_number = models.CharField(max_length=15, null=True, blank=True)
    reference_2_address = models.TextField(max_length=250, null=True, blank=True)
    email_user = models.ForeignKey(EmailUser, on_delete=models.SET_NULL, null=True, related_name='user_lead', blank=True)
    email_user_assign_timestamp = models.DateTimeField(null=True, blank=True)
    lead_status = models.BooleanField(default=True)
    last_status = models.ForeignKey(Disposition, on_delete=models.SET_NULL, null=True, blank=True, related_name='last_disposition_status')
    last_status_date_time = models.DateTimeField(null=True, blank=True)
    total_followup = models.IntegerField(default=0)
    branch_id = models.CharField(max_length=250, null=True, blank=True)
    state_name = models.CharField(max_length=250, null=True, blank=True)
    lead_month = models.CharField(max_length=250, null=True, blank=True)
    collection_manager = models.CharField(max_length=250, null=True, blank=True)
    collection_manager_contact_number = models.CharField(max_length=250, null=True, blank=True)
    collection_manager_email = models.CharField(max_length=250, null=True, blank=True)
    cluster_manager = models.CharField(max_length=250, null=True, blank=True)
    cluster_manager_email = models.CharField(max_length=250, null=True, blank=True)
    state_manager = models.CharField(max_length=250, null=True, blank=True)
    zonal_manager = models.CharField(max_length=250, null=True, blank=True)
    territory_name = models.CharField(max_length=250, null=True, blank=True)
    territory_code = models.CharField(max_length=250, null=True, blank=True)
    bucket = models.CharField(max_length=20, null=True, blank=True)
    actual_bucket = models.CharField(max_length=20, null=True, blank=True)
    ncm = models.CharField(max_length=250, null=True, blank=True)
    repayment_mode = models.CharField(max_length=250, null=True, blank=True)
    contact_person = models.CharField(max_length=250, null=True, blank=True)
    agency_name = models.CharField(max_length=250, null=True, blank=True)
    reason_of_bounce = models.CharField(max_length=250, null=True, blank=True)
    standard_reason = models.CharField(max_length=250, null=True, blank=True)
    scheme = models.CharField(max_length=250, null=True, blank=True)
    supplier = models.CharField(max_length=250, null=True, blank=True)
    tenure = models.CharField(max_length=250, null=True, blank=True)
    dealer_code = models.CharField(max_length=250, null=True, blank=True)
    gross_loan_to_value = models.CharField(max_length=250, null=True, blank=True)
    net_loan_to_value = models.CharField(max_length=250, null=True, blank=True)
    dme_name = models.CharField(max_length=250, null=True, blank=True)
    sales_executive_name = models.CharField(max_length=250, null=True, blank=True)
    sales_executive_code = models.CharField(max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    ivr_status = models.BooleanField(default=False)
    is_notice_issued = models.BooleanField(default=False)
    is_post_card_issued = models.BooleanField(default=False)
    vehicle_status = models.CharField(max_length=250, null=True, blank=True)
    calling_status = models.BooleanField(default=True)

    def __str__(self):
        return str(self.application_id)


class EMI(models.Model):
    application_id = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='emi_detail')
    due_date = models.CharField(max_length=250, null=True, blank=True)
    first_emi_due_date = models.CharField(max_length=250, null=True, blank=True)
    emi_due = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    bounce_charges = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    late_payment_charges = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    tos = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    pos = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.application_id)


class Followup(models.Model):
    application_id = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='followup')
    disposition = models.ForeignKey(Disposition, on_delete=models.SET_NULL, null=True, blank=True, related_name='disposition_for_followup')
    sub_disposition = models.ForeignKey(SubDisposition, on_delete=models.SET_NULL, null=True, blank=True, related_name='sub_disposition_for_followup')
    ptp_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    ptp_date = models.DateTimeField(null=True, blank=True)
    ptp_status = models.BooleanField(default=False)
    payment_mode = models.ForeignKey(PaymentMode, on_delete=models.SET_NULL, null=True, blank=True, related_name='payment_mode_for_followup')
    ptp_type = models.ForeignKey(PTPType, on_delete=models.SET_NULL, null=True, blank=True, related_name='ptp_type_for_followup')
    settlement_percent = models.IntegerField(null=True, blank=True)
    settlement_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    remarks = models.TextField(max_length=250, null=True, blank=True)
    callback_date_time = models.DateTimeField(null=True, blank=True)
    callback_status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    email_user = models.ForeignKey(EmailUser, on_delete=models.SET_NULL, null=True, blank=True, related_name='user_for_followup')

    def __str__(self):
        return str(self.application_id)


class IVR(models.Model):
    application_id = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True,related_name='ivr')
    status = models.CharField(max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.status)


class List(models.Model):
    name = models.CharField(max_length=250, null=True, blank=True)
    file_name = models.CharField(max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    t_count = models.IntegerField(default=0, help_text="total_count")
    c_count = models.IntegerField(default=0, help_text="create_count")
    u_count = models.IntegerField(default=0, help_text="Update_count")
    email_user = models.ForeignKey(EmailUser, on_delete=models.SET_NULL, null=True, related_name='user_list',
                                   blank=True)

    def __str__(self):
        return self


class LeadRecycle(models.Model):
    application_id = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='lead_recycle')
    month = models.CharField(max_length=250, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    list_id = models.ForeignKey(List, on_delete=models.SET_NULL, null=True,  related_name='list_lead')

    def __str__(self):
        return str(self.application_id)


class ClickToCallEvent(models.Model):
    uuid = models.CharField(max_length=250, primary_key=True, unique=True, null=False)
    application_id = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='click_to_call')
    phone_number = models.CharField(max_length=15, null=True)
    extension = models.CharField(max_length=15, null=True)
    is_active = models.BooleanField(default=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.uuid)


class ClickToCallEventDetail(models.Model):
    uuid = models.ForeignKey(ClickToCallEvent, on_delete=models.CASCADE, related_name='click_to_call_event')
    event_name = models.CharField(max_length=250, null=True)
    timestamp = models.CharField(max_length=250, null=True)
    file_name = models.CharField(max_length=250, null=True)
    recording_file = models.CharField(max_length=250, null=True)
    duration = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    length = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    ivr_name = models.CharField(max_length=250, null=True)
    call_status = models.CharField(max_length=250, null=True)
    mos = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    rtp_in_count = models.IntegerField(default=0)
    cause_code = models.IntegerField(default=0)
    hangup_cause = models.CharField(max_length=250, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.uuid)


class CampaignSyncSetting(models.Model):
    last_sync_timestamp = models.DateTimeField(auto_now=True)
    sync_in_process = models.BooleanField(default=False)

    def __str__(self):
        return str(self)


class PaymentInfo(models.Model):
    application_id = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='payment_info')
    amount = models.FloatField(default=0)
    transaction_id = models.CharField(max_length=250, null=True)
    payment_mode = models.CharField(max_length=250, null=True)
    sub_payment_mode = models.CharField(max_length=250, null=True)
    payment_date = models.CharField(max_length=250, null=True)
    email_user = models.ForeignKey(EmailUser, on_delete=models.SET_NULL, null=True, related_name='user_payment_info')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.application_id)


class AgentConfirmedPaymentInfo(models.Model):
    application_id = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='agent_confirmed_payment_info')
    amount = models.FloatField(default=0)
    transaction_id = models.CharField(max_length=250, null=True)
    payment_mode = models.ForeignKey(PaymentMode, on_delete=models.SET_NULL, null=True, blank=True, related_name='payment_mode_for_agent_payment')
    sub_payment_mode = models.ForeignKey(SubPaymentMode, on_delete=models.SET_NULL, null=True, blank=True, related_name='sub_payment_mode_for_agent_payment')
    bank_branch = models.CharField(max_length=250, null=True)
    type = models.CharField(max_length=250, null=True)
    receipt = models.FileField(null=True)
    email_user = models.ForeignKey(EmailUser, on_delete=models.SET_NULL, null=True, related_name='user_confirmed_payment_info')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.application_id)


class Settlement(models.Model):
    application_id = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='settlement_application_id')
    amount = models.FloatField(default=0)
    is_email_sent = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)
    email_user = models.ForeignKey(EmailUser, on_delete=models.SET_NULL, null=True, related_name='user_settlement')
    payment_info = models.ForeignKey(PaymentInfo, on_delete=models.SET_NULL, null=True, related_name='settlement_payment_info')
    is_active = models.BooleanField(default=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    approved_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return str(self.application_id)


class PaymentLink(models.Model):
    application_id = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='payment_link_application_id')
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    mobile_number = models.CharField(max_length=15, null=True, blank=True)
    email_user = models.ForeignKey(EmailUser, on_delete=models.SET_NULL, null=True, related_name='user_payment_link')
    payment_id = models.CharField(max_length=250, null=True, blank=True)
    reference_id = models.CharField(max_length=250, null=True, blank=True)
    payment_url = models.CharField(max_length=250, null=True, blank=True)
    payment_status = models.CharField(max_length=250, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
