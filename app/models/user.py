"""
Created:          19/06/2020
Post-History:     23/06/2020
Title:            This module is intended to provide Custom Email User Model.
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

from django.contrib import auth
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin, Permission, _user_get_permissions
)
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail


class StateManager(models.Manager):
    """
    The manager for the auth's State model.
    """
    use_in_migrations = True

    def get_by_natural_key(self, name):
        return self.get(name=name)


class State(models.Model):
    """
    State Model
    """
    name = models.CharField(_('name'), max_length=150, unique=True)
    objects = StateManager()

    class Meta:
        verbose_name = _('state')
        verbose_name_plural = _('states')

    def __str__(self):
        return self.name

    def natural_key(self):
        return self.name


class Gateway(models.Model):
    """
    GateWay Model
    """
    name = models.CharField(_('name'), max_length=150)
    prefix = models.CharField(_('prefix'), max_length=150, null=True, blank=True)

    def __str__(self):
        return self.name

    def natural_key(self):
        return self.name


class EmailUserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password
        :param str email: user email
        :param str password: user password
        :param bool is_staff: whether user staff or not
        :param bool is_superuser: whether user admin or not
        :return custom_user.models.EmailUser user: user
        :raise ValueError: email is not set
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        is_active = extra_fields.pop("is_active", True)
        user = self.model(email=email, is_staff=is_staff, is_active=is_active,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """
        Creates and saves a User with the given email and password
        :param email: user email
        :param password: user password
        :param extra_fields:
        :return: custom_user.models.EmailUser user: regular user
        """
        is_staff = extra_fields.pop("is_staff", False)
        return self._create_user(email, password, is_staff, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password
        :param email: user email
        :param password: user password
        :param extra_fields:
        :return: custom_user.models.EmailUser user: admin user
        """
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class AbstractEmailUser(AbstractBaseUser, PermissionsMixin):
    """
    Abstract User with the same behaviour as Django's default User.

    AbstractEmailUser does not have username field. Uses email as the
    USERNAME_FIELD for authentication.

    Use this if you need to extend EmailUser.

    Inherits from both the AbstractBaseUser and PermissionMixin.

    The following attributes are inherited from the superclasses:
        * password
        * last_login
        * is_superuser
    """
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), max_length=255, unique=True, db_index=True)
    is_staff = models.BooleanField(_('staff status'), default=False, help_text=_('Designates whether '
                                                                                 'the user can log into '
                                                                                 'this admin site.'))
    is_active = models.BooleanField(_('active'), default=True, help_text=_('Designates whether this user should be '
                                                                           'treated as active. Unselect this instead '
                                                                           'of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    extension = models.CharField(_('extension'), max_length=30, blank=True, null=True)
    states = models.ManyToManyField(
        State,
        verbose_name=_('states'),
        blank=True,
        help_text=_(
            'The states this user belongs to'
        ),
    )
    dispositions = models.ManyToManyField(
        to='app.Disposition',
        verbose_name=_('dispositions'),
        blank=True,
        help_text=_(
            'The disposition this user belongs to'
        ),
    )

    team_lead = models.ForeignKey('self', limit_choices_to={'groups__name': "team lead"}, blank=True, null=True,
                                  on_delete=models.SET_NULL)
    gateway = models.ForeignKey(Gateway, on_delete=models.SET_NULL, null=True, blank=True, related_name='user_gateway')

    objects = EmailUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:  # noqa: D101
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True

    def get_full_name(self):
        """
        Return User Full Name
        :return: first_name last_name
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns User short name
        :return: first_name
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Send an email to this User.
        :param subject:
        :param message:
        :param from_email:
        :param kwargs:
        :return:
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class EmailUser(AbstractEmailUser):
    """
    Concrete class of AbstractEmailUser.
    Use this if you don't need to extend EmailUser.
    """
    class Meta(AbstractEmailUser.Meta):  # noqa: D101
        swappable = 'AUTH_USER_MODEL'
