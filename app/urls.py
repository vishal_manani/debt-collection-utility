"""
Created:          19/06/2020
Post-History:     07/07/2020
Title:            This module is intended to provide Application URLs
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

from django.urls import path
from .views import *
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'disposition', DispositionViewSet, basename="disposition")
router.register(r'sub-disposition', SubDispositionViewSet, basename="sub-disposition")
router.register(r'all-sub-disposition', GetAllSubDispositionViewSet, basename="all-sub-disposition")
router.register(r'payment-mode', PaymentModeViewSet, basename="payment-mode")
router.register(r'sub-payment-mode', SubPaymentModeViewSet, basename="sub-payment-mode")
router.register(r'ptp-type', PTPTypeViewSet, basename="ptp-type")
router.register(r'state', StateViewSet, basename="state")
router.register(r'disposition-filter', DispositionForFilterViewSet, basename="disposition-filter")
router.register(r'customer', CustomerView, basename="customer")
router.register(r'search/customer', CustomerSearchView, basename="search/customer")
router.register(r'followup', FollowupView, basename="followup")
router.register(r'callback', CallbackView, basename="callback")
router.register(r'search/callback', CallbackSearchView, basename="search/callback")
router.register(r'ptp', PTPView, basename="ptp")
router.register(r'search/ptp', PTPSearchView, basename="search/ptp")
router.register(r'filter-notice-postcard', FilterByNoticeOrPostCard, basename="filter-notice-postcard")
router.register(r'manage-lead', ManageLeadView, basename="manage-lead")
router.register(r'email-user', UserViewSet, basename="email-user")
router.register(r'campaign', CampaignViewSet, basename="campaign")
router.register(r'emi-followup', EMIFollowupView, basename="emi-followup")
router.register(r'payment-info', PaymentInfoViewSet, basename="payment-info")
router.register(r'settlement-info', SettlementViewSet, basename="settlement-info")
router.register(r'search-application', SearchByApplication, basename="search-application")
router.register(r'filter/customer', CustomerFilterView, basename="filter/customer")
router.register(r'payment-link-info', PaymentLinkViewSet, basename="payment-link-info")

urlpatterns = [
    path('login/', LoginApiView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('file-upload/', UploadFileView.as_view(), name='file-upload'),
    path('user-dashboard/', UserDashboardView.as_view(), name='user-dashboard'),
    path('admin-dashboard/', AdminDashboardView.as_view(), name='admin-user-dashboard'),
    path('assign-lead/', AssignLead.as_view(), name='assign-lead'),
    path('file-assign-lead/', FileAssignLead.as_view(), name='file-assign-lead'),
    path('file-payment-info/<int:type>/', FilePaymentInfo.as_view(), name='file-payment-info'),
    path('file-settlement/', FileSettlementApiView.as_view(), name='file-settlement'),
    path('file-generic/', FileUploadGenericApiView.as_view(), name='file-generic'),
    path('c2c/', ClickToCall.as_view(), name='c2c'),
    path('c2c/log/application-id/', ClickToCallLogByApplicationId.as_view(), name='c2c-log-application-id'),
    path('campaign/start/', CampaignStartApiView.as_view(), name='campaign-start'),
    path('campaign/stop/', CampaignStopApiView.as_view(), name='campaign-stop'),
    # path('ivr/notifications/', IvrNotificationsApiView.as_view(), name='ivr-notifications'),
    path('agent-confirmed-payment-info/', AgentConfirmedPaymentInfoViewSet.as_view(), name='agent-confirmed-payment-info'),
    path('download/csv/', DownloadCsvApiView.as_view(), name='download-csv'),
    path('report/date/csv/', ReportByDateRange.as_view(), name='report-by-date-range'),
    path('update-customer-status/', UpdateCustomerStatus.as_view(), name='update-customer-status'),
    path('check-ptp-exists/', CheckPTPExists.as_view(), name='check-ptp-exists')
]

urlpatterns += router.urls
