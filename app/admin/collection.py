import logging
from django.contrib import admin
from django.utils.html import format_html
from app.models.collection import *
from rangefilter.filter import DateRangeFilter
from core.utils import to_dict
from service.email import settlement_mail, settlement_reminder_mail
from datetime import datetime
from collections import defaultdict
from django.contrib import messages
from django_admin_listfilter_dropdown.filters import (
    DropdownFilter, ChoiceDropdownFilter, RelatedDropdownFilter
)
from service.razorpay import RazorpayApiManager

razorpay_api = RazorpayApiManager.get_instance()

log = logging.getLogger("Collection-admin")


class DispositionAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_at', 'updated_at', 'is_active')
    search_fields = ('name', )
    ordering = ('name',)

    def has_delete_permission(self, request, obj=None):
        return None


class SubDispositionAdmin(admin.ModelAdmin):

    list_display = ('name', 'disposition', 'created_at', 'updated_at', 'is_active')
    search_fields = ('name', )
    ordering = ('name',)

    def has_delete_permission(self, request, obj=None):
        return None


class PaymentModeAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_at', 'updated_at', 'is_active')
    search_fields = ('name', )
    ordering = ('name',)

    def has_delete_permission(self, request, obj=None):
        return None


class SubPaymentModeAdmin(admin.ModelAdmin):

    list_display = ('name', 'payment_mode', 'created_at', 'updated_at', 'is_active')
    search_fields = ('name', )
    ordering = ('name',)

    def has_delete_permission(self, request, obj=None):
        return None


class PTPTypeAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_at', 'updated_at', 'is_active')
    search_fields = ('name', )
    ordering = ('name',)

    def has_delete_permission(self, request, obj=None):
        return None


class EMIInline(admin.StackedInline):
    model = EMI
    can_delete = False
    max_num = 1


class FollowupInline(admin.StackedInline):
    model = Followup
    can_delete = False
    max_num = 1


class IVRInline(admin.StackedInline):
    model = IVR
    can_delete = False
    max_num = 1


class CustomerAdmin(admin.ModelAdmin):
    inlines = (EMIInline, FollowupInline, IVRInline)

    list_display = ('application_id', 'name', 'last_status', 'state_name', 'lead_month', 'is_active', 'email_user_name', 'updated_at', 'created_at')
    search_fields = ('application_id', 'loan_agreement_number', 'name', 'contact_number_1', 'contact_number_2', 'contact_number_3')
    ordering = ('application_id', 'created_at')
    list_filter = (
        ('is_active', DropdownFilter),
        ('last_status', RelatedDropdownFilter),
        ('email_user', RelatedDropdownFilter),
        ('state_name', DropdownFilter),
        ('lead_month', DropdownFilter),
        ('created_at', DateRangeFilter),
    )

    def email_user_name(self, obj):
        first_name = obj.email_user.first_name if obj.email_user else ''
        last_name = obj.email_user.last_name if obj.email_user else ''
        return first_name + ' ' + last_name
    email_user_name.short_description = 'Agent'


class ListAdmin(admin.ModelAdmin):
    list_display = ('file_name', 'name', 't_count', 'c_count', 'u_count', 'email_user', 'created_at')
    search_fields = ('file_name', 'name', 't_count', 'c_count', 'u_count')
    ordering = ('-created_at',)
    list_filter = (
        ('created_at', DateRangeFilter),
    )

    def has_delete_permission(self, request, obj=None):
        return None

    def has_change_permission(self, request, obj=None):
        return None

    def has_add_permission(self, request):
        return None


class LeadRecycleAdmin(admin.ModelAdmin):
    list_display = ('application_id', 'month', 'updated_at', 'created_at')
    search_fields = ('application_id', 'month')
    ordering = ('-updated_at',)
    list_filter = (
        'month',
        ('created_at', DateRangeFilter),
        ('updated_at', DateRangeFilter),
    )

    def has_delete_permission(self, request, obj=None):
        return None

    def has_add_permission(self, request):
        return None

    def has_change_permission(self, request, obj=None):
        return None


class SettlementAdmin(admin.ModelAdmin):
    list_display = ('application_id', 'get_bucket', 'tos', 'pos', 'amount', 'tos_percentage', 'tos_waiver_percentage', 'is_email_sent', 'is_approved', 'approved_date', 'payment',  'email_user_name', 'collection_manager_email',
                    'cluster_manager_email', 'updated_at', 'created_at', 'is_active')
    fields = ('application_id', 'amount', 'is_email_sent', 'is_approved', 'approved_date', 'email_user', 'is_active',)
    search_fields = ('application_id__application_id', 'amount', 'email_user__first_name', 'email_user__last_name')
    ordering = ('-updated_at',)
    list_filter = (
        'is_approved',
        'is_email_sent',
        'is_active',
        ('email_user', RelatedDropdownFilter),
        ('updated_at', DateRangeFilter),
        ('created_at', DateRangeFilter),
    )

    actions = ['send_email_to_manager', 'reset_settlement', 'send_reminder_email_to_manager']

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        if not request.user.is_superuser:
            actions = []
        return actions

    def get_bucket(self, obj):
        return obj.application_id.bucket
    get_bucket.short_description = 'BKT'

    def email_user_name(self, obj):
        return obj.email_user.first_name + ' ' + obj.email_user.last_name
    email_user_name.short_description = 'Agent'

    def collection_manager_email(self, obj):
        return obj.application_id.collection_manager_email
    collection_manager_email.short_description = 'Collection Manager'

    def cluster_manager_email(self, obj):
        return obj.application_id.cluster_manager_email
    cluster_manager_email.short_description = 'Cluster Manager'

    def pos(self, obj):
        emi = EMI.objects.get(application_id=obj.application_id)
        pos = emi.pos
        return pos
    pos.short_description = 'POS'

    def tos(self, obj):
        emi = EMI.objects.get(application_id=obj.application_id)
        tos = emi.tos
        return tos
    tos.short_description = 'TOS'

    def tos_percentage(self, obj):
        emi = EMI.objects.get(application_id=obj.application_id)
        tos = float(emi.tos)
        tos_percentage = round(float(obj.amount / tos * 100), 2)
        return tos_percentage
    tos_percentage.short_description = 'Settlement % on TOS	'

    def tos_waiver_percentage(self, obj):
        emi = EMI.objects.get(application_id=obj.application_id)
        tos = float(emi.tos)
        tos_percentage = round(float(obj.amount / tos * 100), 2)
        tos_waiver_percentage = round(float(100 - tos_percentage), 2)
        return tos_waiver_percentage
    tos_waiver_percentage.short_description = 'Waiver % on TOS'

    def payment(self, obj):
        p = PaymentInfo.objects.filter(application_id=obj.application_id).values('amount')
        amount = 0
        for i in p:
            amount += float(i.get('amount', 0))
        return amount
    payment.short_description = 'Payment'

    def send_email_to_manager(self, request, queryset):
        try:
            data = queryset.select_related()
            already_email_sent = []
            manager_email_not_mapped = []
            email_sent = []
            settlement_id = []
            email_data = defaultdict(list)
            cc_email = {}
            for i in data:
                customer = to_dict(i.application_id)
                manager_email = customer.get('collection_manager_email', None)
                cluster_manager_email = customer.get('cluster_manager_email', None)
                application_id = str(i.application_id)
                settlement_amount = i.amount
                is_email_sent = i.is_email_sent
                bucket = customer.get('bucket', None)

                emi = EMI.objects.get(application_id_id=application_id)
                pos = float(emi.pos)
                tos = float(emi.tos)
                tos_percentage = round(float(settlement_amount / tos * 100), 2)
                tos_waiver_percentage = round(float(100 - tos_percentage), 2)

                if manager_email:
                    if not is_email_sent:
                        email_data[manager_email].append({
                                    "application_id": application_id,
                                    "branch": customer.get('branch'),
                                    "settlement_amount": settlement_amount,
                                    "loan_agreement_number": customer.get('loan_agreement_number', None),
                                    "customer_name": customer.get('name', None),
                                    "tos": tos,
                                    "pos": pos,
                                    "tos_percentage": tos_percentage,
                                    "tos_waiver_percentage": tos_waiver_percentage,
                                    "is_email_sent": is_email_sent,
                                    "bucket": bucket
                        })
                        email_sent.append(application_id)
                        settlement_id.append(i.id)
                        cc_email.update({manager_email: cluster_manager_email})
                    else:
                        already_email_sent.append(application_id)
                else:
                    manager_email_not_mapped.append(application_id)

            for email_id, value in email_data.items():
                if email_id:
                    settlement_mail({"to_email": email_id, "cc_email": cc_email.get(email_id), "value": value})

            if email_sent:
                queryset.filter(id__in=settlement_id).update(is_email_sent=True, updated_at=datetime.now())
                msg = "Email sent: {application_id_list}".format(application_id_list=email_sent)
                messages.add_message(request, messages.INFO, msg)

            if already_email_sent:
                msg = "Email already sent: {application_id_list}".format(application_id_list=already_email_sent)
                messages.add_message(request, messages.INFO, msg)

            if manager_email_not_mapped:
                msg = "Manager email not mapped: {application_id_list}".format(application_id_list=manager_email_not_mapped)
                messages.add_message(request, messages.INFO, msg)

        except Exception as exc:
            messages.add_message(request, messages.INFO, "Error while sending email")
            log.error("[Collection-admin -> SettlementAdmin]" + str(exc))

    send_email_to_manager.short_description = "Send email to manager"

    def reset_settlement(self, request, queryset):
        try:
            success_list = []
            for i in queryset:
                payment = PaymentInfo.objects.filter(application_id=i.application_id).values('amount')
                amount = 0
                for p in payment:
                    amount += float(p.get('amount', 0))
                if amount <= 0.00:
                    queryset.update(is_email_sent=False, is_approved=False, approved_date=None,
                                    updated_at=datetime.now(), created_at=datetime.now())
                    success_list.append(str(i.application_id))

            msg = "Reset Settlement: {success_list}".format(success_list=success_list)
            messages.add_message(request, messages.INFO, msg)
        except Exception as exc:
            messages.add_message(request, messages.INFO, "Error while Reset Settlement")
            log.error("[Collection-admin -> Error while Reset settlement]" + str(exc))

    reset_settlement.short_description = "Reset Settlement"

    def send_reminder_email_to_manager(self, request, queryset):
        try:
            data = queryset.select_related()
            already_email_sent = []
            manager_email_not_mapped = []
            email_sent = []
            settlement_id = []
            email_data = defaultdict(list)
            cc_email = {}
            for i in data:
                customer = to_dict(i.application_id)
                manager_email = customer.get('collection_manager_email', None)
                cluster_manager_email = customer.get('cluster_manager_email', None)
                application_id = str(i.application_id)
                settlement_amount = i.amount
                is_email_sent = i.is_email_sent
                is_approved = i.is_approved
                bucket = customer.get('bucket', None)

                emi = EMI.objects.get(application_id_id=application_id)
                pos = float(emi.pos)
                tos = float(emi.tos)
                tos_percentage = round(float(settlement_amount / tos * 100), 2)
                tos_waiver_percentage = round(float(100 - tos_percentage), 2)

                if manager_email:
                    if not is_approved:
                        email_data[manager_email].append({
                                    "application_id": application_id,
                                    "branch": customer.get('branch'),
                                    "settlement_amount": settlement_amount,
                                    "loan_agreement_number": customer.get('loan_agreement_number', None),
                                    "customer_name": customer.get('name', None),
                                    "tos": tos,
                                    "pos": pos,
                                    "tos_percentage": tos_percentage,
                                    "tos_waiver_percentage": tos_waiver_percentage,
                                    "is_email_sent": is_email_sent,
                                    "bucket": bucket
                        })
                        email_sent.append(application_id)
                        settlement_id.append(i.id)
                        cc_email.update({manager_email: cluster_manager_email})
                    else:
                        already_email_sent.append(application_id)
                else:
                    manager_email_not_mapped.append(application_id)

            for email_id, value in email_data.items():
                if email_id:
                    settlement_reminder_mail({"to_email": email_id, "cc_email": cc_email.get(email_id), "value": value})

            if email_sent:
                queryset.filter(id__in=settlement_id).update(is_email_sent=True, updated_at=datetime.now())
                msg = "Email sent: {application_id_list}".format(application_id_list=email_sent)
                messages.add_message(request, messages.INFO, msg)

            if already_email_sent:
                msg = "Already Approved: {application_id_list}".format(application_id_list=already_email_sent)
                messages.add_message(request, messages.INFO, msg)

            if manager_email_not_mapped:
                msg = "Manager email not mapped: {application_id_list}".format(application_id_list=manager_email_not_mapped)
                messages.add_message(request, messages.INFO, msg)

        except Exception as exc:
            messages.add_message(request, messages.INFO, "Error while sending email")
            log.error("[Collection-admin -> SettlementAdmin]" + str(exc))

    send_reminder_email_to_manager.short_description = "Reminder email to manager"


class ClickToCallEventDetailInline(admin.TabularInline):
    model = ClickToCallEventDetail
    can_delete = False
    readonly_fields = ('uuid', 'event_name', 'timestamp', 'file_name', 'duration', 'ivr_name', 'call_status', 'mos', 'rtp_in_count', 'cause_code', 'hangup_cause', 'created_at',)
    max_num = 1


class ClickToCallEventAdmin(admin.ModelAdmin):
    inlines = (ClickToCallEventDetailInline,)

    list_display = ('application_id', 'phone_number', 'extension', 'email_user_name', 'updated_at', 'created_at', 'is_active', 'recording')
    readonly_fields = ('uuid', 'application_id', 'phone_number', 'extension', 'updated_at', 'created_at', 'is_active')
    search_fields = ('uuid', 'application_id__application_id', 'phone_number', 'extension')
    ordering = ('-created_at',)
    fieldsets = (
            ('CALL DETAILS', {
                'fields': ('application_id', 'uuid', 'phone_number', 'extension', 'updated_at', 'created_at', 'is_active')
            }),
    )

    list_filter = (
        ('extension', DropdownFilter),
        ('created_at', DateRangeFilter),
        ('updated_at', DateRangeFilter),
    )

    def email_user_name(self, obj):
        user = EmailUser.objects.filter(extension=obj.extension).values('first_name', 'last_name').last()
        if user:
            first_name = user.get('first_name', None)
            last_name = user.get('last_name', None)
            return first_name + ' ' + last_name
        else:
            return None
    email_user_name.short_description = 'Agent'

    def recording(self, obj):
        created_date = datetime.strftime(obj.created_at, '%Y-%m-%d')
        url = "http://61.12.85.62/recordings/{created_date}/{uuid}.wav".format(uuid=obj.uuid, created_date=created_date)
        html = format_html("<a href='{url}' target='_blank'> Recording </a>", url=url)
        return html
    recording.short_description = 'Recording'

    def has_delete_permission(self, request, obj=None):
        return None

    def has_add_permission(self, request):
        return None

    def has_change_permission(self, request, obj=None):
        return None


class CampaignSyncSettingAdmin(admin.ModelAdmin):
    list_display = ('last_sync_timestamp', 'sync_in_process')
    search_fields = ('last_sync_timestamp', )
    ordering = ('last_sync_timestamp', )
    list_filter = (
        ('last_sync_timestamp', DateRangeFilter),
    )


class AgentConfirmedPaymentInfoAdmin(admin.ModelAdmin):
    list_display = ('application_id', 'amount', 'transaction_id', 'payment_mode', 'sub_payment_mode', 'type', 'email_user_name', 'created_at', )
    search_fields = ('application_id__application_id', 'transaction_id', 'bank_branch', 'email_user__first_name', 'email_user__last_name')
    ordering = ('-created_at', )
    list_filter = (
        ('type', DropdownFilter),
        ('email_user', RelatedDropdownFilter),
        ('payment_mode', RelatedDropdownFilter),
        ('sub_payment_mode', RelatedDropdownFilter),
        ('created_at', DateRangeFilter),
    )

    def email_user_name(self, obj):
        return obj.email_user.first_name + ' ' + obj.email_user.last_name
    email_user_name.short_description = 'Agent'

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


class PaymentInfoAdmin(admin.ModelAdmin):
    list_display = ('application_id', 'amount', 'transaction_id', 'payment_mode', 'sub_payment_mode', 'payment_date', 'email_user_name', 'created_at')
    search_fields = ('application_id__application_id', 'amount', 'email_user__first_name', 'email_user__last_name', 'transaction_id', 'payment_mode', 'sub_payment_mode',)
    ordering = ('-created_at',)
    list_filter = (
        ('email_user', RelatedDropdownFilter),
        ('created_at', DateRangeFilter),
    )

    def email_user_name(self, obj):
        return obj.email_user.first_name + ' ' + obj.email_user.last_name
    email_user_name.short_description = 'Created By'

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


class PaymentLinkAdmin(admin.ModelAdmin):
    list_display = ('application_id', 'amount', 'razorpay_url', 'payment_status', 'payment_track', 'reference_id', 'email_user_name', 'updated_at', 'created_at', 'is_active')
    search_fields = ('application_id__application_id', 'amount', 'payment_id', 'payment_status', 'payment_url', 'mobile_number', 'email_user__first_name', 'email_user__last_name', 'reference_id')
    ordering = ('-created_at',)
    list_filter = (
        'payment_status',
        'is_active',
        ('email_user', RelatedDropdownFilter),
        ('created_at', DateRangeFilter),
        ('updated_at', DateRangeFilter),
    )

    def email_user_name(self, obj):
        return obj.email_user.first_name + ' ' + obj.email_user.last_name
    email_user_name.short_description = 'Created By'

    def payment_track(self, obj):
        payment_id = obj.payment_id
        url = "https://dashboard.razorpay.com/app/paymentlinks/{payment_id}".format(payment_id=payment_id)
        html = format_html("<a href='{url}' target='_blank'> Razorpay </a>", url=url)
        return html
    payment_track.short_description = 'Track'

    def razorpay_url(self, obj):
        payment_url = obj.payment_url
        url = "{payment_url}".format(payment_url=payment_url)
        html = format_html("<a href='{url}' target='_blank'> {payment_url} </a>", url=url, payment_url=payment_url)
        return html
    razorpay_url.short_description = 'Razorpay Link'

    actions = ['update_payment_link_status_by_id', 'update_all_payment_link_status']

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        if not request.user.is_superuser:
            actions = []
        return actions

    def update_payment_link_status_by_id(self, request, queryset):
        try:
            success_list = []
            error_list = []
            for i in queryset:
                payment_id = i.payment_id
                response = razorpay_api.get_payment_link_by_id(payment_id=payment_id)
                if response.status_code == 200:
                    response = response.json()
                    is_update = PaymentLink.objects.filter(
                        payment_id=response.get('id', None),
                        reference_id=response.get('reference_id', None)).update(
                        payment_status=response.get('status', None), updated_at=datetime.now())
                    if is_update:
                        success_list.append(str(i.application_id))
                    else:
                        error_list.append(str(i.application_id))
                else:
                    error_list.append(str(i.application_id))

            if success_list:
                msg = "Payment Status Updated: {success_list}".format(success_list=success_list)
                messages.add_message(request, messages.INFO, msg)
            if error_list:
                msg = "Error While Payment Status Updated: {error_list}".format(error_list=error_list)
                messages.add_message(request, messages.ERROR, msg)
        except Exception as exc:
            messages.add_message(request, messages.ERROR, "Error while Payment Status Update!")
            log.error("[Collection-admin -> Error while Payment Status Update!]" + str(exc))

    update_payment_link_status_by_id.short_description = "Payment Status Update"

    def update_all_payment_link_status(self, request, queryset):
        try:
            success_list = []
            error_list = []
            queryset = PaymentLink.objects.filter(is_active=True, payment_status__in=['created', 'partially_paid'])
            for i in queryset:
                payment_id = i.payment_id
                response = razorpay_api.get_payment_link_by_id(payment_id=payment_id)
                if response.status_code == 200:
                    response = response.json()
                    is_update = PaymentLink.objects.filter(
                        payment_id=response.get('id', None),
                        reference_id=response.get('reference_id', None)).update(
                        payment_status=response.get('status', None), updated_at=datetime.now())
                    if is_update:
                        success_list.append(str(i.application_id))
                    else:
                        error_list.append(str(i.application_id))
                else:
                    error_list.append(str(i.application_id))

            if success_list:
                msg = "Payment Status Updated: {success_list}".format(success_list=success_list)
                messages.add_message(request, messages.INFO, msg)
            if error_list:
                msg = "Error While Payment Status Updated: {error_list}".format(error_list=error_list)
                messages.add_message(request, messages.ERROR, msg)
        except Exception as exc:
            messages.add_message(request, messages.ERROR, "Error while Payment Status Update!")
            log.error("[Collection-admin -> Error while Payment Status Update!]" + str(exc))

    update_all_payment_link_status.short_description = "All Payment Status Update"


admin.site.register(Disposition, DispositionAdmin)
admin.site.register(SubDisposition, SubDispositionAdmin)
admin.site.register(PaymentMode, PaymentModeAdmin)
admin.site.register(SubPaymentMode, SubPaymentModeAdmin)
admin.site.register(PTPType, PTPTypeAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(List, ListAdmin)
admin.site.register(LeadRecycle, LeadRecycleAdmin)
admin.site.register(Settlement, SettlementAdmin)
admin.site.register(ClickToCallEvent, ClickToCallEventAdmin)
admin.site.register(CampaignSyncSetting, CampaignSyncSettingAdmin)
admin.site.register(AgentConfirmedPaymentInfo, AgentConfirmedPaymentInfoAdmin)
admin.site.register(PaymentInfo, PaymentInfoAdmin)
admin.site.register(PaymentLink, PaymentLinkAdmin)
