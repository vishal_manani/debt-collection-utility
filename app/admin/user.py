from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from .forms import EmailUserChangeForm, EmailUserCreationForm
from app.models.user import *


class EmailUserAdmin(UserAdmin):

    form = EmailUserChangeForm
    add_form = EmailUserCreationForm

    """EmailUser Admin model."""
    fieldsets = (
        (None, {'fields': ('first_name', 'last_name', 'email', 'password', 'extension', 'gateway')}),
        (_('Lead Assign'), {'fields': ('team_lead', 'states', 'dispositions')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = ((
                         None, {
                             'classes': ('wide',),
                             'fields': ('first_name', 'last_name', 'email', 'password1', 'password2')
                         }
                     ),
    )

    # form = EmailUserChangeForm
    # add_form = EmailUserCreationForm

    list_display = ('email', 'first_name', 'last_name', 'extension', 'gateway', 'is_active')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'states', 'groups', 'team_lead', 'gateway')
    search_fields = ('email', 'first_name', 'last_name', 'extension', 'gateway')
    ordering = ('email',)
    filter_horizontal = ('states', 'dispositions', 'groups', 'user_permissions')


class StateAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name',)
    ordering = ('name',)


class GatewayAdmin(admin.ModelAdmin):
    list_display = ('name', 'prefix')
    search_fields = ('name', 'prefix')
    ordering = ('name',)


admin.site.register(EmailUser, EmailUserAdmin)
admin.site.register(State, StateAdmin)
admin.site.register(Gateway, GatewayAdmin)
