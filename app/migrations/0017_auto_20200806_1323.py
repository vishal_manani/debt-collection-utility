# Generated by Django 3.0.7 on 2020-08-06 13:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20200806_1239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clicktocallevent',
            name='application_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='click_to_call', to='app.Customer'),
        ),
        migrations.AlterField(
            model_name='clicktocalleventdetail',
            name='uuid',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='click_to_call_event', to='app.ClickToCallEvent'),
        ),
    ]
