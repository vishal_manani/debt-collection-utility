# Generated by Django 3.0.7 on 2021-02-04 12:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0037_auto_20210130_2211'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymentlink',
            name='reference_id',
            field=models.CharField(blank=True, max_length=250, null=True),
        ),
    ]
