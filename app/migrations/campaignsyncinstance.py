from django.db import migrations

def forwards_func(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    CampaignSyncSetting = apps.get_model("app", "CampaignSyncSetting")
    db_alias = schema_editor.connection.alias
    CampaignSyncSetting.objects.using(db_alias).create()

class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_campaignsyncsetting'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]
