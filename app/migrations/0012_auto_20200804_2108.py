# Generated by Django 3.0.7 on 2020-08-04 21:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_customer_manager_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='followup',
            name='ptp_amount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True),
        ),
    ]
