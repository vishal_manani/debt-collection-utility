# Generated by Django 3.0.7 on 2020-07-20 12:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_paymentinfo_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='last_status',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='last_disposition_status', to='app.Disposition'),
        ),
        migrations.AddField(
            model_name='customer',
            name='lead_status',
            field=models.BooleanField(default=True),
        ),
    ]
