# Generated by Django 3.0.7 on 2020-07-17 13:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_remove_paymentinfo_loan_agreement_no'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymentinfo',
            name='type',
            field=models.CharField(max_length=250, null=True),
        ),
    ]
