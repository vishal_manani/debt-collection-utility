# Generated by Django 3.0.7 on 2020-08-19 13:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0025_auto_20200817_1405'),
    ]

    operations = [
        migrations.AlterField(
            model_name='followup',
            name='ptp_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
