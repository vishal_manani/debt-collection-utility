# Generated by Django 3.0.7 on 2020-07-04 17:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=30, verbose_name='last name')),
                ('email', models.EmailField(db_index=True, max_length=255, unique=True, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
                'swappable': 'AUTH_USER_MODEL',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('application_id', models.CharField(max_length=250, primary_key=True, serialize=False)),
                ('loan_agreement_number', models.CharField(blank=True, max_length=250, null=True)),
                ('name', models.CharField(blank=True, max_length=250, null=True)),
                ('address', models.TextField(blank=True, max_length=250, null=True)),
                ('alternate_address', models.TextField(blank=True, max_length=250, null=True)),
                ('contact_number_1', models.CharField(blank=True, max_length=15, null=True)),
                ('contact_number_2', models.CharField(blank=True, max_length=15, null=True)),
                ('contact_number_3', models.CharField(blank=True, max_length=15, null=True)),
                ('branch', models.CharField(blank=True, max_length=250, null=True)),
                ('product', models.CharField(blank=True, max_length=250, null=True)),
                ('asset_make', models.CharField(blank=True, max_length=250, null=True)),
                ('registration_number', models.CharField(blank=True, max_length=250, null=True)),
                ('loan_amount', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('emi_amount', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('reference_1_name', models.CharField(blank=True, max_length=250, null=True)),
                ('reference_1_contact_number', models.CharField(blank=True, max_length=15, null=True)),
                ('reference_2_name', models.CharField(blank=True, max_length=250, null=True)),
                ('reference_2_contact_number', models.CharField(blank=True, max_length=15, null=True)),
                ('email_user_assign_timestamp', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('email_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='user_lead', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Disposition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='PaymentMode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='PTPType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='SubDisposition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('disposition', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='disposition', to='app.Disposition')),
            ],
        ),
        migrations.CreateModel(
            name='LeadRecycle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('application_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='lead_recycle', to='app.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='IVR',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(blank=True, max_length=250, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('application_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ivr', to='app.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='Followup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('previous_contact_date', models.DateField(blank=True, null=True)),
                ('ptp_amount', models.DecimalField(decimal_places=2, max_digits=10, null=True)),
                ('ptp_date', models.DateField(blank=True, null=True)),
                ('settlement_percent', models.IntegerField(blank=True, null=True)),
                ('settlement_amount', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('remarks', models.TextField(blank=True, max_length=250, null=True)),
                ('callback_date_time', models.DateTimeField(blank=True, null=True)),
                ('callback_status', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('application_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='followup', to='app.Customer')),
                ('disposition', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='disposition_for_followup', to='app.Disposition')),
                ('email_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='user_for_followup', to=settings.AUTH_USER_MODEL)),
                ('payment_mode', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='payment_mode_for_followup', to='app.PaymentMode')),
                ('ptp_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='ptp_type_for_followup', to='app.PTPType')),
                ('sub_disposition', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='sub_disposition_for_followup', to='app.SubDisposition')),
            ],
        ),
        migrations.CreateModel(
            name='EMI',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('actual_bucket', models.CharField(blank=True, max_length=20, null=True)),
                ('emi_due', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('bounce_charges', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('late_payment_charges', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('tos', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('pos', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_active', models.BooleanField(default=True)),
                ('application_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='emi_detail', to='app.Customer')),
            ],
        ),
    ]
