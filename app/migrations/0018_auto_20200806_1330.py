# Generated by Django 3.0.7 on 2020-08-06 13:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0017_auto_20200806_1323'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clicktocalleventdetail',
            name='file_name',
            field=models.CharField(max_length=250, null=True),
        ),
    ]
