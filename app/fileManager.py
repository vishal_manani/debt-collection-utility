"""
Created:          25/06/2020
Post-History:     07/07/2020
Title:            This module is intended to provide handler for FileManager
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import ray
import time
import logging
import datetime
import itertools
import pandas as pd
from django.conf import settings
from core.utils import str_to_list, Dict2Obj
from service.ray import synchronous_processing
from .models import List
from .helper import FILE_DICT, SET_LIST_FUNCTION, update_list_count

log = logging.getLogger("FileManager")


class FileManager:
    _instance = None

    def __init__(self, request, rayFunction, processFunc: str):
        log.info(f"[fileManager -> __init__] File manager Instance Created at:{time.time()}")
        self.fields = str_to_list(FILE_DICT[processFunc], settings.DELIMITER) if processFunc != 'generic_file_update_info' else []
        self.chunk_size = 10 ** 3
        self.request = request
        self.rayFunction = rayFunction
        self.processFunc = processFunc
        log.debug(f"[fileManager -> __init__] Ray Function:{self.rayFunction} and Process Function:{self.processFunc}")

    def __enter__(self):
        log.info(f"[fileManager -> __enter__] Ray Initialize at:{time.time()}")
        ray.init(configure_logging=False, object_store_memory=100000000)
        return self

    def __exit__(self, *args):
        log.info(f"[fileManager -> __exit__] Ray Shutdown at:{time.time()}")
        ray.shutdown(exiting_interpreter=True)

    def file_process(self, file, list_id=None):
        """
        Process files in Chunk
        :return:
        """
        self._set_list_object_id(file, list_id)
        log.info(f"[fileManager -> file_process] Data Process Start at:{time.time()}")
        if self.processFunc == 'generic_file_update_info':
            df = pd.read_csv(file, skipinitialspace=True, dtype="str")
        else:
            df = pd.read_csv(file,  skipinitialspace=True, usecols=self.fields, dtype="str")
        df = df.fillna(' ')
        data = df.to_dict(orient='records')
        process_data = self._data_process(data)
        count, error_data = map(list, zip(*process_data))
        error_data = list(itertools.chain(*error_data))
        self._update_list_object(count)
        log.info(f"[fileManager -> file_process] Data Process end at:{time.time()}")
        df = pd.DataFrame(error_data, columns=["Application Id"])
        if len(error_data) > 0: df.to_csv(settings.MEDIA_ROOT+ 'error_data.csv')
        return error_data

    def _data_process(self, data: list):
        """
        Initiate Synchronous Processing.
        :param data: dict
        :return: None
        """
        return synchronous_processing(user=self.request.user.id, rayFun=self.rayFunction, data=data, processFunc=self.processFunc, obj=self.list_obj_id)

    def _set_list_object_id(self, file, list_id):
        self.list_obj_id, self.list_obj = None,  None
        if self.processFunc in SET_LIST_FUNCTION:
            log.info(f"[fileManager -> _set_list_object_id] Get List Instance ID")
            if list_id:
                self.list_obj_id = list_id
                self.list_obj = List.objects.get(id=list_id)
            if not list_id:
                file_name = f"{datetime.datetime.now():%Y%m%d%H%m%S}"
                self.list_obj = List.objects.create(name=file.name, file_name=str(file_name),
                                                    email_user_id=self.request.user.id)
                self.list_obj_id = self.list_obj.id

    def _update_list_object(self, list_count):
        if self.processFunc in SET_LIST_FUNCTION:
            log.info(f"[fileManager -> _update_list_object] Set List against Customer")
            t_count, c_count, u_count = 0, 0, 0
            for obj in list_count:
                t_count += obj.t_count
                c_count += obj.c_count
                u_count += obj.u_count
            update_list_count(self.list_obj, data=Dict2Obj({"t_count": t_count, "c_count": c_count, "u_count": u_count}))
