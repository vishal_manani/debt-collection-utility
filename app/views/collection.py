"""
Created:          24/06/2020
Post-History:     25/06/2020
Title:            This module is intended to provide Collection APIs
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import csv
import itertools
import time
import json
import requests
import logging
import pandas as pd
from django.forms import model_to_dict
from django.http import HttpResponse
from app.serializers import *
from app.fileManager import FileManager
from core.viewsets import VerboseViewSet
from core.response import success_response, error_response
from service.ray import file_data_store_in_db
from rest_framework import viewsets
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from pandas.errors import EmptyDataError
from core.search import get_search_results
from django.db.models import DateTimeField
from django.db.models import Count, Sum
from datetime import datetime
from core.utils import custom_exception_handler
from rest_framework.response import Response
from core.viewsets import get_json
from django.conf import settings
from app.helper import get_payment_info, get_agent_confirmed_payment_info, get_click_to_call_event_details, user_group_list, update_ivr_status, \
    change_campaign_sync_status, get_campaign_sync_status, get_settlement, get_lead_recycle, get_click_to_call_event,\
    user_state_list, user_dispositions_list, dashboard_date_wise
from core.api_client import CampaignApiManager
from django.db.models import Q
from django.core.files.storage import FileSystemStorage
from core.utils import to_dict
from collections import defaultdict
from service.razorpay import RazorpayApiManager


log = logging.getLogger("Collection")
cp_api = CampaignApiManager.get_instance()

slack_logger = logging.getLogger('django.request')

razorpay_api = RazorpayApiManager.get_instance()


class DispositionViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used for all Operations over Dispositions.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'post', 'put', 'patch', 'delete', 'options']
    serializer_class = DispositionSerializer
    queryset = Disposition.objects.filter(is_active=True).all().order_by('name')


class SubDispositionViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used for all Operations over Sub Dispositions.
    """
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = SubDispositionSerializer

    def get_queryset(self):
        disposition_id = self.request.query_params.get('q', None)
        queryset = SubDisposition.objects.filter(is_active=True, disposition=disposition_id).order_by('name')
        return queryset


class GetAllSubDispositionViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to List out all Sub-dispositions.
    """
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = SubDispositionSerializer
    queryset = SubDisposition.objects.filter(is_active=True).all().order_by('name')


class PaymentModeViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to All operations over Payment Mode.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'post', 'put', 'patch', 'delete', 'options']
    serializer_class = PaymentModeSerializer
    queryset = PaymentMode.objects.filter(is_active=True).all().order_by('name')


class SubPaymentModeViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to All operations over Payment Mode.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'post', 'put', 'patch', 'delete', 'options']
    serializer_class = PaymentModeSerializer

    def get_queryset(self):
        payment_mode_id = self.request.query_params.get('q', None)
        queryset = SubPaymentMode.objects.filter(is_active=True, payment_mode=payment_mode_id).order_by('name')
        return queryset


class PTPTypeViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to All operations over PTPTypes.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'post', 'put', 'patch', 'delete', 'options']
    serializer_class = PTPTypeSerializer
    queryset = PTPType.objects.filter(is_active=True).all().order_by('name')


class DispositionForFilterViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used for all Operations over Dispositions For Filter.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'post', 'put', 'patch', 'delete', 'options']
    serializer_class = DispositionSerializer
    queryset = Disposition.objects.filter().all().order_by('name')


class StateViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used for all Operations over State.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'post', 'put', 'patch', 'delete', 'options']
    serializer_class = StateSerializer
    queryset = State.objects.all().order_by('name')


class CustomerView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to All operations over Customers.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'put', 'patch', 'options']
    serializer_class = CustomerSerializer
    queryset = Customer.objects.filter(is_active=True).all().order_by('last_status_date_time')

    def get_queryset(self):
        user_id = self.request.user.id
        group = user_group_list(request=self.request)
        if 'admin' in group.get('group_list') and self.request.user.is_superuser:
            queryset = Customer.objects.filter(is_active=True).all().order_by('last_status_date_time')
        elif 'team lead' in group.get('group_list'):
            team_agent = list(EmailUser.objects.filter(team_lead=user_id).values_list('id', flat=True))
            queryset = Customer.objects.filter(is_active=True, email_user_id__in=team_agent).all().order_by('last_status_date_time')
        else:
            state = user_state_list(request=self.request)
            user_dispositions = user_dispositions_list(request=self.request)
            filter_string = {"is_active": True, "calling_status": True, "email_user": user_id}
            is_data_list = self.request.query_params.get('page', None)
            if state and is_data_list:
                filter_string.update({"state_name__in": state})
            if user_dispositions and is_data_list:
                filter_string.update({"last_status__in": user_dispositions})
            queryset = Customer.objects.filter(**filter_string).all().order_by('last_status_date_time')
        return queryset


class CustomerSearchView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to list out Customers.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = CustomerSerializer

    def get_queryset(self):
        search_term = self.request.query_params.get('q', None)
        user_id = self.request.user.id
        group = user_group_list(request=self.request)
        if 'admin' in group.get('group_list') and self.request.user.is_superuser:
            queryset = Customer.objects.filter(is_active=True).order_by('last_status_date_time')
        elif 'team lead' in group.get('group_list'):
            team_agent = list(EmailUser.objects.filter(team_lead=user_id).values_list('id', flat=True))
            queryset = Customer.objects.filter(is_active=True, email_user_id__in=team_agent).all().order_by('last_status_date_time')
        else:
            state = user_state_list(request=self.request)
            user_dispositions = user_dispositions_list(request=self.request)
            filter_string = {"is_active": True, "calling_status": True, "email_user": user_id}
            if state:
                filter_string.update({"state_name__in": state})
            if user_dispositions:
                filter_string.update({"last_status__in": user_dispositions})
            queryset = Customer.objects.filter(**filter_string).order_by('last_status_date_time')
        fields = [f.name for f in Customer._meta.fields if not isinstance(f, DateTimeField)]
        fields.remove('email_user')
        fields.remove('last_status')
        queryset = get_search_results(queryset=queryset, search_fields=fields, search_term=search_term)
        return queryset


class CustomerFilterView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to return Followups for selected User.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = CustomerSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        filter_by = self.request.query_params.get('filter_by', None)
        group = user_group_list(request=self.request)
        filter_by = None if filter_by == '7' else filter_by
        lead_status = False if filter_by == '6' else True
        if 'admin' in group.get('group_list') and self.request.user.is_superuser:
            queryset = Customer.objects.filter(is_active=True, last_status=filter_by, lead_status=lead_status).order_by('last_status_date_time')
        elif 'team lead' in group.get('group_list'):
            team_agent = list(EmailUser.objects.filter(team_lead=user_id).values_list('id', flat=True))
            queryset = Customer.objects.filter(is_active=True, email_user_id__in=team_agent, last_status=filter_by, lead_status=lead_status).all().order_by('last_status_date_time')
        else:
            state = user_state_list(request=self.request)
            user_dispositions = user_dispositions_list(request=self.request)
            filter_string = {"is_active": True, "calling_status": True, "email_user": user_id, "last_status": filter_by, "lead_status": lead_status}
            if state:
                filter_string.update({"state_name__in": state})
            if user_dispositions:
                filter_string.update({"last_status__in": user_dispositions})
            queryset = Customer.objects.filter(**filter_string).order_by('last_status_date_time')
        return queryset


class FollowupView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to All operations over Followup.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'post', 'options']
    serializer_class = FollowupSerializer
    queryset = Followup.objects.filter(is_active=True).all().order_by('id')

    def create(self, request, *args, **kwargs):
        try:
            serializer = super().create(request, *args, **kwargs)
            followup_id = serializer.data.get('data', {}).get('id', None)
            application_id = serializer.data.get('data', {}).get('application_id', None)
            callback_status = serializer.data.get('data', {}).get('callback_status', None)
            disposition = serializer.data.get('data', {}).get('disposition', None)
            sub_disposition = serializer.data.get('data', {}).get('sub_disposition', None)
            if callback_status:
                followup = Followup.objects.filter(application_id=application_id).exclude(id=followup_id).update(callback_status=False)
            else:
                followup = Followup.objects.filter(application_id=application_id).update(callback_status=False)

            if disposition == 1:
                ptp_status = Followup.objects.filter(application_id=application_id).exclude(id=followup_id).update(ptp_status=False)
            elif disposition == 4 or (disposition == 2 and sub_disposition == 5):
                ptp_status = Followup.objects.filter(application_id=application_id).update(ptp_status=False)

            followup_count = Followup.objects.filter(application_id=application_id).count()
            Customer.objects.filter(application_id=application_id).update(last_status=disposition, last_status_date_time=datetime.now(), total_followup=followup_count)
            response_data = get_json(200, '+OK', 'Success', 'Created Successfully', serializer)
            return Response(response_data)
        except Exception as exc:
            log.error("[collection -> FollowupView]" + str(exc))
            raise custom_exception_handler(exc, request)


class UploadFileView(APIView):
    """
    Used to Upload File.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            file = request.FILES.get('file')
            list_id = request.FILES.get('list_id', None)
            start_time = time.time()
            log.debug(f'File Processing Start at:{start_time}')
            with FileManager(request, file_data_store_in_db, 'create_update_customer_obj') as fm:
                fm.file_process(file, list_id)
            log.debug(f'File Processing End at:{time.time()}')
            log.debug(f'File Processing Time at:{time.time()-start_time}')
        except EmptyDataError as exc:
            log.error("[collection -> UploadFileView]" + str(exc))
            slack_logger.error("Error while Customer File Upload", exc_info=True)
            raise error_response('Blank Data File is not Allowed.')
        except Exception as exc:
            log.error("[collection -> UploadFileView]" + str(exc))
            slack_logger.error("Error while Customer File Upload", exc_info=True)
            return error_response('File Missing')
        return success_response("File Processing Completed Successfully")


class AdminDashboardView(APIView):
    """
    Used to display Admin Dashboard
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']

    def get(self, request):
        try:
            state_name = request.GET.get('state_name', None)
            if state_name and not state_name == 'null' and not state_name == 'undefined':
                customer_filter = {"is_active": True, "state_name": state_name}
            else:
                customer_filter = {"is_active": True}

            group = user_group_list(request=self.request)
            if 'team lead' in group.get('group_list'):
                user_id = self.request.user.id
                team_agent = list(EmailUser.objects.filter(team_lead=user_id).values_list('id', flat=True))
                customer_filter.update({"email_user_id__in": team_agent})

            data = {}

            month = request.GET.get('month', None)
            current_month = datetime.now().strftime("%m")

            if month and month != current_month:
                converted_month = datetime.strptime(month, '%m').strftime('%b')
                application_list = list(LeadRecycle.objects.filter(month=converted_month).values_list('application_id_id', flat=True))
                customer_filter.pop('is_active')
                customer_filter.update({"application_id__in": application_list})
                customer = Customer.objects.filter(**customer_filter).values_list('application_id', flat=True)
                current_month = month
            else:
                customer = Customer.objects.filter(**customer_filter).values_list('application_id', flat=True)

            lead_assign_count = len(customer)
            data.update({'lead_assign_count': lead_assign_count})

            emi = EMI.objects.filter(application_id_id__in=customer).aggregate(total_pos=Sum('pos'), total_tos=Sum('tos'))
            total_tos = round(emi.get('total_tos', 0))
            total_pos = round(emi.get('total_pos', 0))
            data.update({"tos": total_tos, "pos": total_pos})

            paid_unconfirmed = Customer.objects.filter(application_id__in=customer, is_active=1, last_status_id=8).values_list('application_id', flat=True)

            ptp_queryset = Followup.objects.filter(application_id__in=customer, ptp_status=True, is_active=True).filter(~Q(application_id__in=paid_unconfirmed)).values('application_id', 'ptp_amount', 'settlement_amount', 'sub_disposition')
            ptp = defaultdict(dict)
            for i in ptp_queryset:
                ptp_amount = i.get('ptp_amount', None)
                settlement_amount = i.get('settlement_amount', None)
                sub_disposition = i.get('sub_disposition', None)
                if sub_disposition == 1:
                    ptp_amount = settlement_amount
                if ptp_amount is not None:
                    ptp[i.get('application_id')].update({'amount': ptp_amount})

            ptp_amount_value = 0
            for key, value in ptp.items():
                amount = float(value.get('amount', 0))
                ptp_amount_value += amount

            ptp_count = len(ptp)
            ptp_percentage = round((ptp_count / lead_assign_count) * 100, 2)

            ptp_application_id_list = []
            for app_id in ptp:
                ptp_application_id_list.append(app_id)
            emi = EMI.objects.filter(application_id_id__in=ptp_application_id_list).aggregate(total_pos=Sum('pos'), total_tos=Sum('tos'))
            total_ptp_pos = round(emi.get('total_pos', 0))

            data.update({'ptp_count': ptp_count, 'ptp_amount_value': round(ptp_amount_value),
                         'ptp_percentage': ptp_percentage, "total_ptp_pos": total_ptp_pos})

            payment = PaymentInfo.objects.filter(application_id__in=customer).values('application_id', 'amount', 'payment_date')
            payment_amount, payment_count = 0, []
            for p in payment:
                payment_date = p.get('payment_date', None)
                if payment_date:
                    payment_date_month = datetime.strptime(payment_date, "%Y-%m-%d").strftime("%m")
                    if current_month == payment_date_month:
                        payment_count.append(p.get('application_id'))
                        p_amount = p.get('amount', 0)
                        payment_amount += p_amount

            payment_amount = round(payment_amount)
            payment_count = len(set(payment_count))

            resolution_payment = round((payment_count / lead_assign_count) * 100, 2)
            resolution_tos = round((payment_amount / total_tos) * 100, 2)
            resolution_pos = round((payment_amount / total_pos) * 100, 2)

            data.update({"payment_amount": payment_amount, "payment_count": payment_count,
                         "resolution_payment": resolution_payment, "resolution_tos": resolution_tos,
                         "resolution_pos": resolution_pos})

            settlement = Settlement.objects.filter(application_id__in=customer, created_at__month=current_month,
                                                   is_approved=True, is_active=True).values('application_id', 'amount')
            settlement_data = defaultdict(int)
            for s in settlement:
                application_id = s.get('application_id', None)
                settlement_payment = PaymentInfo.objects.filter(application_id=application_id, created_at__month=current_month).values('amount')
                p_amount = 0
                for p in settlement_payment:
                    p_amount += p.get('amount', 0)
                s_amount = s.get('amount', 0)
                if s_amount <= p_amount:
                    settlement_data[application_id] += s_amount

            settlement_amount = round(sum(settlement_data.values()))
            settlement_count = len(settlement_data)

            emi_payment_count = payment_count - settlement_count
            emi_payment_amount = payment_amount - settlement_amount

            if settlement_data:
                settlement_application_id_list = []
                for app_id in settlement_data:
                    settlement_application_id_list.append(app_id)
                settlement_emi = EMI.objects.filter(application_id_id__in=settlement_application_id_list).aggregate(total_pos=Sum('pos'), total_tos=Sum('tos'))
                total_settlement_pos = round(settlement_emi.get('total_pos', 0)) if settlement_emi else 0
            else:
                total_settlement_pos = 0

            settlement_ratio = round((settlement_amount / total_settlement_pos) * 100, 2) if total_settlement_pos else 0

            data.update({"settlement_count": settlement_count, 'settlement_amount': settlement_amount,
                         'emi_payment_count': emi_payment_count, 'emi_payment_amount': emi_payment_amount,
                         "total_settlement_pos": total_settlement_pos, "settlement_ratio": settlement_ratio})

        except Exception as exc:
            log.error("[collection -> AdminDashboardView]" + str(exc))
            slack_logger.error("Error while AdminDashboardView", exc_info=True)
            return error_response('Error while dashboard data get')
        return success_response("Data get Successfully", data=data)


class UserDashboardView(APIView):
    """
    Used to display Dashboard
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']

    def get(self, request):
        try:
            selected_user = request.GET.get('user', None)
            from_date = self.request.query_params.get('from_date', None)
            to_date = self.request.query_params.get('to_date', None)
            if from_date is None or to_date is None:
                from_date = to_date = datetime.today().strftime("%Y-%m-%d")

            if selected_user and not selected_user == 'null':
                customer_filter = {"email_user": selected_user, "is_active": True}
            else:
                customer_filter = {"email_user": request.user.id, "is_active": True}

            data = {}
            customer = Customer.objects.filter(**customer_filter).values_list('application_id', flat=True)
            lead_assign_count = len(customer)
            data.update({'lead_assign_count': lead_assign_count})

            emi = EMI.objects.filter(application_id_id__in=customer).aggregate(total_pos=Sum('pos'), total_tos=Sum('tos'))
            total_tos = round(emi.get('total_tos', 0))
            total_pos = round(emi.get('total_pos', 0))
            data.update({"tos": total_tos, "pos": total_pos})

            total_attempt = Followup.objects.filter(is_active=1, application_id__in=customer).count()
            data.update({'total_attempt': total_attempt})

            unique_attempt_count, attempt_pending_count = 0, 0
            for i in customer:
                attempt = Followup.objects.filter(is_active=1, application_id_id=i).exists()
                if attempt:
                    unique_attempt_count += 1
                else:
                    attempt_pending_count += 1
            data.update({'unique_attempt_count': unique_attempt_count, 'attempt_pending_count': attempt_pending_count})

            connect_count = 0
            for i in customer:
                connect = Followup.objects.filter(is_active=1, application_id_id=i).filter(Q(application_id_id__last_status=1) | Q(application_id_id__last_status=2) | Q(application_id_id__last_status=4) | Q(application_id_id__last_status=14) | Q(sub_disposition_id=15)).exists()
                if connect:
                    connect_count += 1
            data.update({'connect_count': connect_count})

            connect_pending_count = lead_assign_count - connect_count
            data.update({'connect_pending_count': connect_pending_count})

            avg_connect_per_case = round((connect_count / lead_assign_count) * 100, 2)
            data.update({'avg_connect_per_case': avg_connect_per_case})

            paid_unconfirmed = Customer.objects.filter(application_id__in=customer, is_active=1, last_status_id=8).values_list('application_id', flat=True)

            ptp_queryset = Followup.objects.filter(application_id__in=customer, ptp_status=True, is_active=True).filter(~Q(application_id__in=paid_unconfirmed)).values('application_id', 'ptp_amount', 'settlement_amount', 'sub_disposition')
            ptp = defaultdict(dict)
            for i in ptp_queryset:
                ptp_amount = i.get('ptp_amount', None)
                settlement_amount = i.get('settlement_amount', None)
                sub_disposition = i.get('sub_disposition', None)
                if sub_disposition == 1:
                    ptp_amount = settlement_amount
                if ptp_amount is not None:
                    ptp[i.get('application_id')].update({'amount': ptp_amount})

            ptp_amount_value = 0
            for key, value in ptp.items():
                amount = float(value.get('amount', 0))
                ptp_amount_value += amount

            ptp_count = len(ptp)
            ptp_percentage = round((ptp_count / lead_assign_count) * 100, 2)
            data.update({'ptp_count': ptp_count, 'ptp_amount_value': round(ptp_amount_value), 'ptp_percentage': ptp_percentage})

            current_month = datetime.now().strftime("%m")
            payment = PaymentInfo.objects.filter(application_id__in=customer).values('application_id', 'amount', 'payment_date')
            payment_amount, payment_count = 0, []
            for p in payment:
                payment_date = p.get('payment_date', None)
                if payment_date:
                    payment_date_month = datetime.strptime(payment_date, "%Y-%m-%d").strftime("%m")
                    if current_month == payment_date_month:
                        payment_count.append(p.get('application_id'))
                        p_amount = p.get('amount', 0)
                        payment_amount += p_amount

            payment_amount = round(payment_amount)
            payment_count = len(set(payment_count))

            resolution_payment = round((payment_count / lead_assign_count) * 100, 2)
            resolution_tos = round((payment_amount / total_tos) * 100, 2)
            resolution_pos = round((payment_amount / total_pos) * 100, 2)

            data.update({"payment_amount": payment_amount, "payment_count": payment_count,
                         "resolution_payment": resolution_payment, "resolution_tos": resolution_tos, "resolution_pos": resolution_pos})

            settlement = Settlement.objects.filter(application_id__in=customer, created_at__month=current_month, is_approved=True, is_active=True).values('application_id', 'amount')
            settlement_data = defaultdict(int)
            for s in settlement:
                application_id = s.get('application_id', None)
                settlement_payment = PaymentInfo.objects.filter(application_id=application_id, created_at__month=current_month).values('amount')
                p_amount = 0
                for p in settlement_payment:
                    p_amount += p.get('amount', 0)
                s_amount = s.get('amount', 0)
                if s_amount <= p_amount:
                    settlement_data[application_id] += s_amount

            settlement_amount = round(sum(settlement_data.values()))
            settlement_count = len(settlement_data)

            emi_payment_count = payment_count - settlement_count
            emi_payment_amount = payment_amount - settlement_amount

            if settlement_data:
                settlement_application_id_list = []
                for app_id in settlement_data:
                    settlement_application_id_list.append(app_id)
                settlement_emi = EMI.objects.filter(application_id_id__in=settlement_application_id_list).aggregate(total_pos=Sum('pos'), total_tos=Sum('tos'))
                total_settlement_pos = round(settlement_emi.get('total_pos', 0)) if settlement_emi else 0
                total_settlement_tos = round(settlement_emi.get('total_tos', 0)) if settlement_emi else 0
            else:
                total_settlement_pos = 0
                total_settlement_tos = 0

            settlement_ratio_pos = round((settlement_amount / total_settlement_pos) * 100, 2) if total_settlement_pos else 0
            settlement_ratio_tos = round((settlement_amount / total_settlement_tos) * 100, 2) if total_settlement_tos else 0

            data.update({"settlement_count": settlement_count, 'settlement_amount': settlement_amount,
                         "settlement_ratio_pos": settlement_ratio_pos, "settlement_ratio_tos": settlement_ratio_tos,
                         'emi_payment_count': emi_payment_count, 'emi_payment_amount': emi_payment_amount})

            attempt_ratio = round((unique_attempt_count / lead_assign_count) * 100, 2) if lead_assign_count else 0
            connect_ratio = round((connect_count / lead_assign_count) * 100, 2) if lead_assign_count else 0
            ptp_ratio_allocation = round((ptp_count / lead_assign_count) * 100, 2) if lead_assign_count else 0
            ptp_ratio_connect = round((ptp_count / connect_count) * 100, 2) if connect_count else 0

            data.update({"attempt_ratio": attempt_ratio, "connect_ratio": connect_ratio,
                         "ptp_ratio_allocation": ptp_ratio_allocation, "ptp_ratio_connect": ptp_ratio_connect})

            today_data = dashboard_date_wise(customer=customer, paid_unconfirmed=paid_unconfirmed, from_date=from_date, to_date=to_date)
            data.update({"today_data": today_data})

        except Exception as exc:
            log.error("[collection -> UserDashboardView]" + str(exc))
            slack_logger.error("Error while User Dashboard", exc_info=True)
            return error_response('Error while dashboard data get')
        return success_response("Data get Successfully", data=data)


class CallbackView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to return Followups for selected User.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = CallbackSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        from_date = self.request.query_params.get('from_date', None)
        to_date = self.request.query_params.get('to_date', None)
        if from_date is None or to_date is None:
            from_date = to_date = datetime.today().strftime("%Y-%m-%d")

        customer = Customer.objects.filter(email_user=user_id, is_active=1, calling_status=True).values_list('application_id', flat=True)

        state = user_state_list(request=self.request)
        filter_string = {"is_active": True, "application_id__in": customer, "callback_status": True, "callback_date_time__range": [from_date + ' 00:00:00', to_date + ' 23:59:59']}
        if state:
            filter_string.update({"application_id__state_name__in": state})

        queryset = Followup.objects.filter(**filter_string).order_by('callback_date_time')
        return queryset


class CallbackSearchView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to list out Customers.
    """
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = CallbackSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        search_term = self.request.query_params.get('q', None)
        from_date = self.request.query_params.get('from_date', None)
        to_date = self.request.query_params.get('to_date', None)
        if from_date is None or to_date is None:
            from_date = to_date = datetime.today().strftime("%Y-%m-%d")

        customer = Customer.objects.filter(email_user=user_id, is_active=1, calling_status=True).values_list('application_id', flat=True)

        state = user_state_list(request=self.request)
        filter_string = {"is_active": True, "application_id__in": customer, "callback_status": True, "callback_date_time__range": [from_date + ' 00:00:00', to_date + ' 23:59:59']}
        if state:
            filter_string.update({"application_id__state_name__in": state})

        queryset = Followup.objects.filter(**filter_string).order_by('callback_date_time')
        fields = ['application_id__application_id', 'application_id__name', 'application_id__loan_agreement_number',
                  'application_id__contact_number_1', 'application_id__contact_number_2', 'application_id__contact_number_3']
        queryset = get_search_results(queryset=queryset, search_fields=fields, search_term=search_term)
        return queryset


class PTPView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to return Followups for selected User.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = PTPSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        from_date = self.request.query_params.get('from_date', None)
        to_date = self.request.query_params.get('to_date', None)

        customer = Customer.objects.filter(email_user=user_id, is_active=1).values_list('application_id', flat=True)

        if from_date is None or to_date is None:
            from_date = to_date = datetime.today().strftime("%Y-%m-%d")
            queryset = Followup.objects.filter(application_id__in=customer, is_active=True, ptp_status=True, ptp_date__lte=to_date + ' 23:59:59').order_by('ptp_date', 'application_id')
        else:
            queryset = Followup.objects.filter(application_id__in=customer, is_active=True, ptp_status=True, ptp_date__range=[from_date + ' 00:00:00', to_date + ' 23:59:59']).order_by('ptp_date', 'application_id')
        return queryset

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.get_queryset()
            data = defaultdict(dict)
            for i in queryset:
                emi_detail = i.application_id.emi_detail.values('pos', 'tos')
                pos, tos = 0, 0
                for e in emi_detail:
                    pos = e.get('pos')
                    tos = e.get('tos')

                if i.settlement_amount and not i.ptp_amount:
                    ptp_amount = i.settlement_amount
                else:
                    ptp_amount = i.ptp_amount

                data[i.application_id_id].update(
                    {
                        "application_id": i.application_id_id,
                        "name": i.application_id.name,
                        "ptp_amount": ptp_amount,
                        "pos": pos,
                        "tos": tos,
                        "ptp_date": i.ptp_date,
                        "state_name": i.application_id.state_name,
                        "is_notice_issued": i.application_id.is_notice_issued,
                        "is_post_card_issued": i.application_id.is_post_card_issued,
                        "total_followup": i.application_id.total_followup,
                        "last_status_date_time": i.application_id.last_status_date_time,
                    }
                )
            data = [v for k, v in data.items()]
            return success_response("Listed Successfully", data=data)
        except Exception as exc:
            log.error("[PTPView -> Error while grt ptp]" + str(exc))
            slack_logger.error("Error while PTP List Records", exc_info=True)
            raise custom_exception_handler(exc, request)


class PTPSearchView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to list out Customers.
    """
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = PTPSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        search_term = self.request.query_params.get('q', None)
        from_date = self.request.query_params.get('from_date', None)
        to_date = self.request.query_params.get('to_date', None)

        customer = Customer.objects.filter(email_user=user_id, is_active=1).values_list('application_id', flat=True)

        if from_date is None or to_date is None:
            from_date = to_date = datetime.today().strftime("%Y-%m-%d")
            queryset = Followup.objects.filter(application_id__in=customer, is_active=True, ptp_status=True, ptp_date__lte=to_date + ' 23:59:59').order_by('ptp_date', 'application_id')
        else:
            queryset = Followup.objects.filter(application_id__in=customer, is_active=True, ptp_status=True, ptp_date__range=[from_date + ' 00:00:00', to_date + ' 23:59:59']).order_by('ptp_date', 'application_id')

        fields = ['application_id__application_id', 'application_id__name', 'application_id__loan_agreement_number',
                  'application_id__contact_number_1', 'application_id__contact_number_2', 'application_id__contact_number_3']
        queryset = get_search_results(queryset=queryset, search_fields=fields, search_term=search_term)
        return queryset

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.get_queryset()
            data = defaultdict(dict)
            for i in queryset:
                emi_detail = i.application_id.emi_detail.values('pos', 'tos')
                pos, tos = 0, 0
                for e in emi_detail:
                    pos = e.get('pos')
                    tos = e.get('tos')

                if i.settlement_amount and not i.ptp_amount:
                    ptp_amount = i.settlement_amount
                else:
                    ptp_amount = i.ptp_amount

                data[i.application_id_id].update(
                    {
                        "application_id": i.application_id_id,
                        "name": i.application_id.name,
                        "ptp_amount": ptp_amount,
                        "pos": pos,
                        "tos": tos,
                        "ptp_date": i.ptp_date,
                        "state_name": i.application_id.state_name,
                        "is_notice_issued": i.application_id.is_notice_issued,
                        "is_post_card_issued": i.application_id.is_post_card_issued,
                        "total_followup": i.application_id.total_followup,
                        "last_status_date_time": i.application_id.last_status_date_time,
                    }
                )
            data = [v for k, v in data.items()]
            return success_response("Listed Successfully", data=data)
        except Exception as exc:
            log.error("[PTPSearchView -> Error which ptp search]" + str(exc))
            slack_logger.error("Error while PTP Search", exc_info=True)
            raise custom_exception_handler(exc, request)


class CheckPTPExists(APIView):
    """
       Used to check ptp is exists or not
    """
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            application_id = request.data.get('application_id', None)
            ptp = Followup.objects.filter(application_id=application_id, disposition_id=1, ptp_status=True, is_active=True).exists()
            response = {"ptp_exists": ptp}
        except Exception as exc:
            response = {"ptp_exists": None}
            log.error("[collection -> CheckPTPExists]" + str(exc))
            slack_logger.error("Error while CheckPTPExists", exc_info=True)
        return success_response("Successfully get ptp status", data=response)


class FilterByNoticeOrPostCard(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to All operations over Customers.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'put', 'patch', 'options']
    serializer_class = CustomerSerializer
    queryset = Customer.objects.filter(is_active=True).all().order_by('application_id')

    def get_queryset(self):
        user_id = self.request.user.id
        group = user_group_list(request=self.request)
        filter_by = self.request.query_params.get('filter_by', None)
        search_term = self.request.query_params.get('search', None)
        disposition = self.request.query_params.get('disposition', None)

        if filter_by == "is_notice_issued":
            filter_string = {"is_notice_issued": True}
        elif filter_by == "is_post_card_issued":
            filter_string = {"is_post_card_issued": True}
        else:
            filter_string = None

        if disposition:
            disposition = None if disposition == '7' else disposition
            filter_string.update({"last_status": disposition})

        if 'admin' in group.get('group_list') and self.request.user.is_superuser:
            queryset = Customer.objects.filter(is_active=True).filter(**filter_string).all().order_by('application_id')
        else:
            state = user_state_list(request=self.request)
            user_dispositions = user_dispositions_list(request=self.request)
            if state:
                filter_string.update({"state_name__in": state})
            if user_dispositions:
                filter_string.update({"last_status__in": user_dispositions})
            queryset = Customer.objects.filter(is_active=True, email_user=user_id).filter(**filter_string).all().order_by('application_id')

        if search_term:
            fields = [f.name for f in Customer._meta.fields if not isinstance(f, DateTimeField)]
            fields.remove('email_user')
            fields.remove('last_status')
            queryset = get_search_results(queryset=queryset, search_fields=fields, search_term=search_term)

        return queryset


class ManageLeadView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to return All Active Customers.
    """
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'put', 'patch', 'options']
    serializer_class = ManageLeadSerializer

    def get_queryset(self):
        search_term = self.request.query_params.get('q', None)
        if search_term:
            queryset = Customer.objects.filter(is_active=True).all().order_by('-updated_at')
            fields = ['application_id', 'email_user__first_name', 'email_user__last_name', 'email_user__email']
            queryset = get_search_results(queryset=queryset, search_fields=fields, search_term=search_term)
        else:
            queryset = Customer.objects.filter(is_active=True).all().order_by('-updated_at')
        return queryset


class AssignLead(APIView):
    """
    Used to Assign Lead to Customer.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            data = json.loads(request.body.decode("UTF-8"))
            application_id_list = data.get('application_id_list')
            user_id = data.get('user_id')
            Customer.objects.filter(application_id__in=application_id_list).update(email_user=user_id, email_user_assign_timestamp=datetime.now())
        except Exception as exc:
            log.error("[collection -> AssignLead]" + str(exc))
            slack_logger.error("Error while Assign Lead", exc_info=True)
            return error_response('Error While assign lead')
        return success_response("Assign Lead Completed Successfully")


class FileAssignLead(APIView):
    """
    Used to Assign Lead in Bulk to User
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            file = request.FILES.get('file')
            start_time = time.time()
            log.debug(f'File Processing Start at:{start_time}')
            with FileManager(request, file_data_store_in_db, 'update_agent_lead') as fm:
                fm.file_process(file)
            log.debug(f'File Processing End at:{time.time()}')
            log.debug(f'File Processing Time at:{time.time() - start_time}')
        except EmptyDataError as exc:
            log.error("[collection -> FileAssignLead]" + str(exc))
            slack_logger.error("Error while File Assign Lead", exc_info=True)
            raise error_response('Blank Data File is not Allowed.')
        except Exception as exc:
            log.error("[collection -> FileAssignLead]" + str(exc))
            slack_logger.error("Error while File Assign Lead", exc_info=True)
            return error_response('Error While assign lead')
        return success_response("Assign Lead Completed Successfully")


class ClickToCall(APIView):
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            data = json.loads(request.body.decode("UTF-8"))
            contact_number = data.get('contact_number', None)
            application_id = data.get('application_id', None)
            extension_number = request.user.extension
            if contact_number and extension_number:
                if 1031 <= int(extension_number) <= 1050:
                    c2c_host = settings.OTHER_C2C_HOST
                else:
                    c2c_host = settings.C2C_HOST

                url = "{c2c_host}/agent-dialout".format(c2c_host=c2c_host)
                if request.user.gateway:
                    gateway = request.user.gateway.name
                    prefix = request.user.gateway.prefix
                else:
                    gateway = 'Airtel'
                    prefix = '444'
                data = {
                    "phone_number": contact_number,
                    "agent_extension": extension_number,
                    "webhook_url": settings.WEB_HOOK_URL,
                    "gateway": gateway,
                    "caller_id": "7968352500",
                    "prefix": prefix
                }
                response = requests.post(url, json=data, verify=False)
                response = response.json()
                uuid = response.get('call_uuid', None)
                if response.get('code') == 200 and uuid:
                    ClickToCallEvent.objects.create(
                        uuid=uuid,
                        application_id_id=application_id,
                        phone_number=contact_number,
                        extension=extension_number
                    )
                else:
                    return error_response('Call not connecting!')
            elif not extension_number:
                return error_response('Extension not valid!')
            else:
                return error_response('Mobile Number or Extension not valid!')
        except Exception as exc:
            log.error("[collection -> ClickToCall]" + str(exc))
            slack_logger.error("Error while Click to Call", exc_info=True)
            return error_response('Call not connecting!')
        return success_response("C2C Completed Successfully")


class ClickToCallLogByApplicationId(APIView):
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            data = json.loads(request.body.decode("UTF-8"))
            application_id = data.get('application_id')
            queryset = ClickToCallEvent.objects.filter(application_id=application_id).prefetch_related().order_by('-created_at')
            log_data = []
            call_log = {}
            for i in queryset:
                call_event = to_dict(i)
                uuid = call_event.get('uuid', None)
                phone_number = call_event.get('phone_number', None)
                call_detail = list()
                for py in i.click_to_call_event.all():
                    event = to_dict(py)
                    ts = int(event.get('timestamp', 0))
                    timestamp = datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                    event['timestamp'] = timestamp
                    event['phone_number'] = phone_number
                    call_detail.append(event)
                call_log.update({uuid: call_detail})
            log_data.append(call_log)
        except Exception as exc:
            log.error("[collection -> ClickToCallLogByApplicationId]" + str(exc))
            slack_logger.error("Error while Click to Call Lob By Application ID", exc_info=True)
            return error_response('Error while get call log!')
        return success_response("Successfully get call log data!", data=log_data)


class CampaignStartApiView(APIView):
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            log.info(f"[collection -> CampaignStartApiView] Campaign Sync settings Updated")
            change_campaign_sync_status(True)
            customers_phone = list(Customer.objects.filter(ivr_status=False).all())
            start_item = 0
            skip_items = 10
            while True:
                if not get_campaign_sync_status():
                    break
                start_time = time.time()
                log.debug(f"[collection -> CampaignStartApiView] Campaign Sync start at:{time.time()}")
                stop_item= start_item + skip_items
                customers = customers_phone[start_item:stop_item]
                start_item = stop_item
                if len(customers) == 0:
                    break
                else:
                    for customer in customers:
                        if customer.contact_number_1 == '' or not customer.contact_number_1:
                            pass
                        else:
                            response = cp_api.campaign(customer.contact_number_1)
                            if response.status_code == status.HTTP_200_OK:
                                update_ivr_status(customer, response.json())
                            else:
                                error_response('API Issue')
                    time.sleep(10)
                log.debug(f"[collection -> CampaignStartApiView] Campaign end at:{start_time - time.time()}")

        except Exception as exc:
            log.error("[collection -> CampaignStartApiView]" + str(exc))
            slack_logger.error("Error while Campaign Start", exc_info=True)
            return error_response('Call is not connecting!')
        return success_response("Campaign Completed Successfully")


class CampaignStopApiView(APIView):
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            log.info(f"[collection -> CampaignStopApiView] Campaign Stop settings Updated")
            change_campaign_sync_status(False)
        except Exception as exc:
            log.error("[collection -> CampaignStopApiView]" + str(exc))
            slack_logger.error("Error while Campaign Stop", exc_info=True)
            return error_response('Campaign is not stop due to some issue!')
        return success_response("Campaign Stopped!!")


class CampaignViewSet(VerboseViewSet, viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    serializer_class = ListSerializer
    http_method_names = ['get', 'options']
    queryset = List.objects.all().order_by('-created_at')


class EMIFollowupView(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to return Followups for customer who pay in EMI.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = PTPSerializer

    def get_queryset(self):
        user_id = self.request.user.id
        customer = Customer.objects.filter(email_user=user_id, is_active=1).values_list('application_id', flat=True)
        queryset = Followup.objects.filter(application_id__in=customer, is_active=True, disposition__name='Promise to Pay',
                                           sub_disposition__name='Ready to Pay EMI').order_by('ptp_date', 'application_id')
        return queryset

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.get_queryset()
            data = defaultdict(dict)
            for i in queryset:
                emi_detail = i.application_id.emi_detail.values('pos', 'tos')
                pos, tos = 0, 0
                for e in emi_detail:
                    pos = e.get('pos')
                    tos = e.get('tos')

                if i.settlement_amount and not i.ptp_amount:
                    ptp_amount = i.settlement_amount
                else:
                    ptp_amount = i.ptp_amount

                data[i.application_id_id].update(
                    {
                        "application_id": i.application_id_id,
                        "name": i.application_id.name,
                        "ptp_amount": ptp_amount,
                        "pos": pos,
                        "tos": tos,
                        "ptp_date": i.ptp_date,
                        "state_name": i.application_id.state_name,
                        "is_notice_issued": i.application_id.is_notice_issued,
                        "is_post_card_issued": i.application_id.is_post_card_issued,
                    }
                )
            data = [v for k, v in data.items()]
            return success_response("Listed Successfully", data=data)
        except Exception as exc:
            log.error("[PTPSearchView -> Error While EMIFollowupView]" + str(exc))
            slack_logger.error("Error while EMIFollowupView", exc_info=True)
            raise custom_exception_handler(exc, request)


class PaymentInfoViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to record Payment Information against Customer.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'patch', 'options']
    serializer_class = PaymentInfoSerializer
    queryset = PaymentInfo.objects.all()

    def create(self, request, *args, **kwargs):
        try:
            request.data['email_user'] = request.user.id
            request.data['application_id_id'] = request.data['application_id']
            serializer = super().create(request, *args, **kwargs)
            if request.data.get('type', None) == 'Settle':
                disposition = Disposition.objects.get(name='Paid')
                Customer.objects.filter(application_id=request.data.get('application_id', None)).update(lead_status=0, last_status=disposition)
            response_data = get_json(200, '+OK', 'Success', 'Created Successfully', serializer)
            return Response(response_data)
        except Exception as exc:
            log.error("[collection -> PaymentInfoViewSet]" + str(exc))
            slack_logger.error("Error while Payment Info", exc_info=True)
            return error_response('Error While Payment Information Update')


class FilePaymentInfo(APIView):
    """
    Used to record payment Information in Bulk
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request, *args, **kwargs):
        try:
            file = request.FILES.get('file')
            type = kwargs['type']
            start_time = time.time()
            log.debug(f'[collection -> FilePaymentInfo] File Processing Start at:{start_time}')
            if type == 1:
                with FileManager(request, file_data_store_in_db, 'update_payment_info1') as fm:
                    fm.file_process(file)
            else:
                with FileManager(request, file_data_store_in_db, 'update_payment_info2') as fm:
                    fm.file_process(file)
            log.debug(f'[collection -> FilePaymentInfo] File Processing End at:{time.time()}')
            log.debug(f'[collection -> FilePaymentInfo] File Processing Time at:{time.time() - start_time}')
        except EmptyDataError as exc:
            log.error("[collection -> FilePaymentInfo]" + str(exc))
            slack_logger.error("Error while File Payment Info Upload", exc_info=True)
            raise error_response('Blank Data File is not Allowed.')
        except Exception as exc:
            log.error("[collection -> FilePaymentInfo]" + str(exc))
            slack_logger.error("Error while File Payment Info Upload", exc_info=True)
            return error_response('Error While assign lead')
        return success_response("Bulk Payment Information Recorded Successfully")


class SearchByApplication(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to list out Customers.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'options']
    serializer_class = CustomerSerializer

    def get_queryset(self):
        search_term = self.request.query_params.get('search_term', None)
        queryset = Customer.objects.filter(Q(application_id=search_term) | Q(loan_agreement_number=search_term))
        return queryset


class AgentConfirmedPaymentInfoViewSet(APIView):
    """
    Used to record agent confirmed payment Information in Bulk
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            file = request.FILES.get('file', None)
            application_id = request.POST.get('application_id', None)
            amount = request.POST.get('amount', None)
            payment_mode = request.POST.get('payment_mode', None)
            sub_payment_mode = request.POST.get('sub_payment_mode', None)
            bank_branch = request.POST.get('bank_branch', None)
            transaction_id = request.POST.get('transaction_id', None)
            payment_type = request.POST.get('type', None)

            if file:
                file_ext = file.name.split(".")[-1]
                timestamp = str(int(time.time()))
                file_name = "{application_id}_{timestamp}.{file_ext}".format(
                    application_id=application_id, timestamp=timestamp, file_ext=file_ext)
            else:
                file_name = None

            create = AgentConfirmedPaymentInfo.objects.create(
                application_id_id=application_id,
                amount=amount,
                payment_mode_id=payment_mode,
                sub_payment_mode_id=sub_payment_mode,
                bank_branch=bank_branch,
                transaction_id=transaction_id,
                type=payment_type,
                receipt=file_name,
                email_user_id=request.user.id,
            )
            if create and file and file_name:
                fs = FileSystemStorage()
                fs.save(file_name, file)
            if create:
                paid_unconfirmed = Disposition.objects.get(name='Paid Unconfirmed')
                Customer.objects.filter(application_id=application_id).update(last_status=paid_unconfirmed)

        except EmptyDataError as exc:
            log.error("[collection -> AgentConfirmedPaymentInfoViewSet]" + str(exc))
            slack_logger.error("Error while Agent Confirmed Payment Info", exc_info=True)
            raise error_response('Blank File is not Allowed.')
        except Exception as exc:
            log.error("[collection -> AgentConfirmedPaymentInfoViewSet]" + str(exc))
            slack_logger.error("Error while Agent Confirmed Payment Info", exc_info=True)
            return error_response('Error Payment Information Recorded')
        return success_response("Payment Information Recorded Successfully")


class SettlementViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to record Payment Information against Customer.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'post', 'patch', 'options']
    serializer_class = SettlementSerializer

    def get_queryset(self):
        search_term = self.request.query_params.get('q', None)
        filter_term = self.request.query_params.get('filter', None)
        user_id = self.request.user.id
        customer = Customer.objects.filter(email_user=user_id, is_active=1).values_list('application_id', flat=True)
        if search_term:
            queryset = Settlement.objects.filter(is_active=True,  application_id__in=customer).all().order_by('-updated_at')
            fields = ['application_id__application_id', 'amount']
            queryset = get_search_results(queryset=queryset, search_fields=fields, search_term=search_term)
        elif filter_term:
            queryset = Settlement.objects.filter(is_active=True, is_approved=filter_term, application_id__in=customer).all().order_by('-updated_at')
        else:
            queryset = Settlement.objects.filter(is_active=True, application_id__in=customer).all().order_by('-updated_at')
        return queryset

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.get_queryset()
            data = []
            for i in queryset:
                payment = PaymentInfo.objects.filter(application_id=i.application_id.application_id).values('amount')
                amount = 0
                for p in payment:
                    amount += float(p.get('amount', 0))

                if not i.amount <= amount and i.created_at < datetime.today():
                    data.append(
                        {
                            "application_id": i.application_id.application_id,
                            "amount": i.amount,
                            "is_email_sent": i.is_email_sent,
                            "is_approved": i.is_approved,
                            "approved_date": i.approved_date,
                            "created_at": i.created_at,
                            "payment": amount if amount != 0 else ""
                        }
                    )
            return success_response("Listed Successfully", data=data)
        except Exception as exc:
            log.error("[PTPView -> Error while grt Settlement]" + str(exc))
            slack_logger.error("Error while get Settlement List", exc_info=True)
            raise custom_exception_handler(exc, request)

    def create(self, request, *args, **kwargs):
        try:
            Settlement.objects.update_or_create(
                application_id_id=request.data['application_id'],
                is_email_sent=False,
                defaults={
                    "amount": request.data['amount'],
                    "email_user_id": request.user.id
                }
            )
            response_data = get_json(200, '+OK', 'Success', 'Created Successfully')
            return Response(response_data)
        except Exception as exc:
            log.error("[collection -> SettlementViewSet]" + str(exc))
            slack_logger.error("Error while Settement", exc_info=True)
            return error_response('Error While Settlement Information Save')


class ReportByDateRange(APIView):
    """
       Used to download PTP, callback report based on date range
       """
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            report_type = request.data.get('report_date', None)
            from_date = request.data.get('from_date', None)
            to_date = request.data.get('to_date', None)

            timestamp = f"{datetime.now(): %Y_%m_%d_%H_%M}"
            file_name = "{type}_{timestamp}.csv".format(type=report_type, timestamp=timestamp)
            data = []
            if from_date is None or to_date is None or from_date == '' or to_date == '':
                from_date = to_date = datetime.today().strftime("%Y-%m-%d")
            if report_type == 'PTP':
                queryset = Followup.objects.filter(is_active=True, ptp_status=True, ptp_date__range=[from_date, to_date]).select_related().order_by('ptp_date', 'application_id')
                ptp = defaultdict(dict)
                for i in queryset:

                    if i.settlement_amount and not i.ptp_amount:
                        ptp_amount = i.settlement_amount
                    else:
                        ptp_amount = i.ptp_amount

                    ptp[i.application_id_id].update(
                        {
                            "id": i.id,
                            "application_id": i.application_id_id,
                            "disposition": i.sub_disposition.name if i.sub_disposition else '',
                            "sub_disposition": i.sub_disposition.name if i.sub_disposition else '',
                            "ptp_amount": ptp_amount,
                            "ptp_date": i.ptp_date,
                            "ptp_status": i.ptp_status,
                            "payment_mode": i.payment_mode.name if i.payment_mode else '',
                            "ptp_type": i.ptp_type.name if i.ptp_type else '',
                            "settlement_percent": i.settlement_percent,
                            # "callback_date_time": i.callback_date_time,
                            # "callback_status": i.callback_status,
                            "agent": str(i.email_user.first_name) + ' ' + str(i.email_user.last_name),
                            "created_at": i.created_at,
                            "updated_at": i.updated_at,
                            "is_active": i.is_active,
                            "remarks": i.remarks,
                        }
                    )

                data = [v for k, v in ptp.items()]

            elif report_type == 'PAYMENT_INFO':
                data = get_payment_info(from_date, to_date)

            elif report_type == 'AGENT_CONFIRMED_PAYMENT_INFO':
                data = get_agent_confirmed_payment_info(from_date, to_date)

            elif report_type == 'SETTLEMENT':
                data = get_settlement(from_date, to_date)

            elif report_type == 'LEAD_RECYCLE':
                data = get_lead_recycle(from_date, to_date)

            elif report_type == 'CLICK_TO_CALL_EVENT':
                data = get_click_to_call_event(from_date, to_date)

            elif report_type == 'CLICK_TO_CALL_EVENT_DETAILS':
                data = get_click_to_call_event_details(from_date, to_date)

            records = data
            keys = records[0].keys() if records else {}
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = f'attachment; filename="{file_name}"'
            dict_writer = csv.DictWriter(response, keys)
            dict_writer.writeheader()
            dict_writer.writerows(records)
            return response
        except Exception as exc:
            log.error("[collection -> DownloadCsv]" + str(exc))
            slack_logger.error("Error while Report by Date Range", exc_info=True)
            raise


# class DownloadCsvApiView(APIView):
#     """
#        Used to record agent confirmed payment Information in Bulk
#        """
#     permission_classes = [AllowAny, ]
#     authentication_classes = [TokenAuthentication]
#     http_method_names = ['post', 'options']
#
#     def get_application_ids(self):
#         return LeadRecycle.objects.filter(month=self.request.data['month']).values_list('application_id',
#                                                                                                    flat=True)
#
#     def get_customer_queryset(self):
#         return Customer.objects.filter(application_id__in=self.get_application_ids(), is_active=True).select_related().order_by('application_id', 'created_at')
#
#     def get_followup_queryset(self):
#         return Followup.objects.filter(application_id__in=self.get_application_ids(), is_active=True).select_related().order_by('application_id_id', 'created_at')
#
#     def post(self, request):
#         try:
#             type = request.data.get('type', None)
#             month = request.data.get('month', None)
#             timestamp = f"{datetime.now(): %Y_%m_%d_%H_%M}"
#             file_name = "{type}_{month}_{timestamp}.csv".format(type=type, month=month, timestamp=timestamp)
#             serializer = ReportSerializer(self.get_customer_queryset(), many=True)
#             if type == "Followup":
#                 serializer = ReportFollowupSerializer(self.get_followup_queryset(), many=True)
#             records = serializer.data
#             keys = records[0].keys() if records else {}
#             response = HttpResponse(content_type='text/csv')
#             response['Content-Disposition'] = f'attachment; filename="{file_name}"'
#             dict_writer = csv.DictWriter(response, keys)
#             dict_writer.writeheader()
#             dict_writer.writerows(records)
#             return response
#         except Exception as exc:
#             log.error("[collection -> DownloadCsv]" + str(exc))
#             raise


class DownloadCsvApiView(APIView):
    """
    Used to record agent confirmed payment Information in Bulk
    """
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def get_application_ids(self):
        return LeadRecycle.objects.filter(month=self.request.data['month']).values_list('application_id',
                                                                                                   flat=True)

    def get_customer_queryset(self):
        data = []
        cs = Customer.objects.filter(application_id__in=self.get_application_ids(), is_active=True).select_related().order_by('application_id', 'created_at')
        for i in cs:
            pd = model_to_dict(i)
            pd['email_user'] = i.email_user.first_name if i.email_user else ''
            pd['last_status'] = i.last_status.name if i.last_status else ''
            pd.update(model_to_dict(i.emi_detail.first()))
            pd.update({'paid_unconfirmed_transaction_id': '', 'paid_unconfirmed_amount': '',
                       'paid_unconfirmed_payment_mode': '', 'paid_unconfirmed_sub_payment_mode': '',
                       'paid_unconfirmed_bank_branch': '', 'paid_unconfirmed_type': '',
                       'payment_transaction_id': '', 'payment_amount': '', 'payment_mode': '',
                       'sub_payment_mode': '', 'payment_date': ''
                       })

            if i.agent_confirmed_payment_info.last():
                last_obj = i.agent_confirmed_payment_info.last()
                # pd.update(model_to_dict(last_obj))
                pd['paid_unconfirmed_transaction_id'] = last_obj.transaction_id
                pd['paid_unconfirmed_payment_mode'] = last_obj.payment_mode.name if last_obj.payment_mode else ''
                pd['paid_unconfirmed_sub_payment_mode'] = last_obj.sub_payment_mode.name if last_obj.sub_payment_mode else ''
                pd['paid_unconfirmed_bank_branch'] = last_obj.bank_branch
                pd['paid_unconfirmed_type'] = last_obj.type
                amount = i.agent_confirmed_payment_info.aggregate(amount=Sum('amount')).get('amount')
                pd['paid_unconfirmed_amount'] = amount

            if i.payment_info.last():
                last_obj = i.payment_info.last()
                pd['payment_transaction_id'] = last_obj.transaction_id
                pd['payment_mode'] = last_obj.payment_mode
                pd['sub_payment_mode'] = last_obj.sub_payment_mode
                pd['payment_date'] = last_obj.payment_date
                amount = i.payment_info.aggregate(amount=Sum('amount')).get('amount')
                pd['payment_amount'] = amount
            data.append(pd)
        return data

    def get_followup_queryset(self):
        fp = Followup.objects.filter(application_id__in=self.get_application_ids(), is_active=True).values('application_id', 'disposition__name', 'sub_disposition__name', 'ptp_status',
                                                                                                           'ptp_amount', 'ptp_date', 'payment_mode__name', 'ptp_type__name',  'settlement_percent',
                                                                                                           'settlement_amount', 'remarks', 'email_user__first_name', 'callback_date_time', 'callback_status',
                                                                                                           'created_at', 'updated_at', 'is_active', ).order_by('application_id_id', 'created_at')
        return list(fp)

    def post(self, request):
        try:
            type = request.data.get('type', None)
            month = request.data.get('month', None)
            timestamp = f"{datetime.now(): %Y_%m_%d_%H_%M}"
            file_name = "{type}_{month}_{timestamp}.csv".format(type=type, month=month, timestamp=timestamp)
            records = self.get_customer_queryset()
            if type == "Followup":
                records = self.get_followup_queryset()
            keys = records[0].keys() if records else {}
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = f'attachment; filename="{file_name}"'
            dict_writer = csv.DictWriter(response, keys)
            dict_writer.writeheader()
            dict_writer.writerows(records)
            return response
        except Exception as exc:
            log.error("[collection -> DownloadCsv]" + str(exc))
            slack_logger.error("Error while Download CSV for Customer and Followup", exc_info=True)
            raise


class UpdateCustomerStatus(APIView):
    """
    Used to Deactivate Customers in Bulk
    """
    permission_classes = [AllowAny]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            file = request.FILES.get('file')
            df = pd.read_csv(file, skipinitialspace=True, dtype="str")
            application_id = df['APPLICATION_ID']
            try:
                Customer.objects.filter(application_id__in=application_id).update(is_active=0)
                EMI.objects.filter(application_id__in=application_id).update(is_active=0)
                Followup.objects.filter(application_id__in=application_id).update(is_active=0)
                IVR.objects.filter(application_id__in=application_id).update(is_active=0)
                ClickToCallEvent.objects.filter(application_id__in=application_id).update(is_active=0)
                return success_response("Leads deactivate successfully ")
            except Exception as exc:
                log.error("[collection -> customer status update error]" + str(exc))
                return error_response('Error While update customer')
        except EmptyDataError as exc:
            log.error("[collection -> Empty file data]" + str(exc))
            slack_logger.error("Error while Customer Status Update", exc_info=True)
            return error_response('Blank Data File is not Allowed.')
        except Exception as exc:
            log.error("[collection -> file input error]" + str(exc))
            slack_logger.error("Error while Customer Status Update", exc_info=True)
            return error_response('Error While assign lead')


class FileSettlementApiView(APIView):
    """
    Used to update Settlement Information.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            file = request.FILES.get('file')
            start_time = time.time()
            log.debug(f'File Processing Start at:{start_time}')
            with FileManager(request, file_data_store_in_db, 'update_settlement_info') as fm:
                fm.file_process(file)
            log.debug(f'File Processing End at:{time.time()}')
            log.debug(f'File Processing Time at:{time.time() - start_time}')
        except EmptyDataError as exc:
            log.error("[collection -> FileSettlementApiView]" + str(exc))
            slack_logger.error("Error while File Settlement", exc_info=True)
            raise error_response('Blank Data File is not Allowed.')
        except Exception as exc:
            log.error("[collection -> FileSettlementApiView]" + str(exc))
            slack_logger.error("Error while File Settlement", exc_info=True)
            return error_response('Error While Settlement Info Update')
        return success_response("Settlement info updated Successfully")


class FileUploadGenericApiView(APIView):
    """
    Used to update Settlement Information.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            file = request.FILES.get('file')
            start_time = time.time()
            log.debug(f'File Processing Start at:{start_time}')
            with FileManager(request, file_data_store_in_db, 'generic_file_update_info') as fm:
                fm.file_process(file)
            log.debug(f'File Processing End at:{time.time()}')
            log.debug(f'File Processing Time at:{time.time() - start_time}')
        except EmptyDataError as exc:
            log.error("[collection -> FileUploadGenericApiView]" + str(exc))
            slack_logger.error("Error while Generic File Upload", exc_info=True)
            raise error_response('Blank Data File is not Allowed.')
        except Exception as exc:
            log.error("[collection -> FileUploadGenericApiView]" + str(exc))
            slack_logger.error("Error while Generic File Upload", exc_info=True)
            return error_response('Error While Settlement Info Update')
        return success_response("File upload Successfully")


class PaymentLinkViewSet(VerboseViewSet, viewsets.ModelViewSet):
    """
    Used to record Payment Information against Customer.
    """
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'post', 'patch', 'options']
    serializer_class = PaymentLinkSerializer

    def get_queryset(self):
        search_term = self.request.query_params.get('q', None)
        filter_term = self.request.query_params.get('filter', None)
        user_id = self.request.user.id
        customer = Customer.objects.filter(email_user=user_id, is_active=1).values_list('application_id', flat=True)
        if search_term:
            queryset = PaymentLink.objects.filter(is_active=True,  application_id__in=customer).all().order_by('-updated_at')
            fields = ['application_id__application_id', 'amount']
            queryset = get_search_results(queryset=queryset, search_fields=fields, search_term=search_term)
        elif filter_term:
            queryset = PaymentLink.objects.filter(is_active=True, is_approved=filter_term, application_id__in=customer).all().order_by('-updated_at')
        else:
            queryset = PaymentLink.objects.filter(is_active=True, application_id__in=customer).all().order_by('-updated_at')
        return queryset

    # def list(self, request, *args, **kwargs):
    #     try:
    #         queryset = self.get_queryset()
    #         data = []
    #         for i in queryset:
    #             payment = PaymentLink.objects.filter(application_id=i.application_id.application_id).values('amount')
    #             amount = 0
    #             for p in payment:
    #                 amount += float(p.get('amount', 0))
    #
    #             if not i.amount <= amount and i.created_at < datetime.today():
    #                 data.append(
    #                     {
    #                         "application_id": i.application_id.application_id,
    #                         "amount": i.amount,
    #                         "is_email_sent": i.is_email_sent,
    #                         "is_approved": i.is_approved,
    #                         "approved_date": i.approved_date,
    #                         "created_at": i.created_at,
    #                         "payment": amount if amount != 0 else ""
    #                     }
    #                 )
    #         return success_response("Listed Successfully", data=data)
    #     except Exception as exc:
    #         log.error("[PTPView -> Error while grt Settlement]" + str(exc))
    #         slack_logger.error("Error while get Settlement List", exc_info=True)
    #         raise custom_exception_handler(exc, request)

    def create(self, request, *args, **kwargs):
        try:
            application_id = request.data.get('application_id', None)
            amount = request.data.get('amount', None)
            amount = amount + '00' if amount else None
            amount = int(amount)
            mobile_number = request.data.get('mobile_number', None)
            timestamp = datetime.strftime(datetime.now(), '%Y%m%d%H%M%S')
            reference_id = "{application_id}#{timestamp}".format(application_id=application_id, timestamp=timestamp)
            description = "Hero FinCorp Limited Loan number {application_id}".format(application_id=application_id)
            name = request.data.get('name', None)
            email = '{name}@gmail.com'.format(name=''.join(name.split()).lower())

            response = razorpay_api.payment_links(
                amount=amount, reference_id=reference_id, description=description,
                name=name, contact=mobile_number, email=email
            )

            if response.status_code == 200:
                response = response.json()
                payment_id = response.get('id', None)
                reference_id = response.get('reference_id', None)
                payment_url = response.get('short_url', None)
                payment_status = response.get('status', None)
                mobile_number = response.get('customer', {}).get('contact', None)
                amount = int(response.get('amount', 0)) // 100

                PaymentLink.objects.update_or_create(
                    application_id_id=application_id,
                    defaults={
                        "amount": amount,
                        "mobile_number": mobile_number,
                        "email_user_id": request.user.id,
                        "payment_id": payment_id,
                        "reference_id": reference_id,
                        "payment_url": payment_url,
                        "payment_status": payment_status
                    }
                )
                response_data = get_json(200, '+OK', 'Success', 'Created Successfully')
            else:
                response_data = get_json(500, '-ERR', 'Error', 'Payment Link not created.')
            return Response(response_data)
        except Exception as exc:
            log.error("[collection -> PaymentLinkViewSet]" + str(exc))
            slack_logger.error("Error While Create payment link", exc_info=True)
            return error_response('Error While Create payment link')
