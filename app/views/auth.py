"""
Created:          19/06/2020
Post-History:
Title:            This module is intended to provide Views for Email USer
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import time
import logging
from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.auth.models import update_last_login
from django.contrib.auth import authenticate, login, logout
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from app.models import EmailUser
from app.serializers.user import UserProfileSerializer
from core.response import success_response, error_response
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from core.viewsets import VerboseViewSet
from app.serializers.user import EmailUserSerializer
from app.helper import user_group_list


log = logging.getLogger("Auth")


class SignUpViewSet(viewsets.ModelViewSet):
    """
        User Signup Functionality : Render Sign UP page and User create
        :param value: first_name , last_name,  email, password
        :return:
    """
    permission_classes = [AllowAny]
    http_method_names = ['post', 'get']
    serializer_class = UserProfileSerializer
    queryset = EmailUser.objects.all()
    template = "signup.html"

    def list(self, request, *args, **kwargs):
        if request.user and request.user.is_authenticated:
            return redirect(settings.DASHBOARD_URL)
        return render(request, self.template, locals())

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        login(request, serializer.instance)
        update_last_login(None, serializer.instance)
        log.debug(f"[auth -> SignUpViewSet] User {serializer.instance.email} Signup at:{time.time()}")
        return success_response('User Sign-up Success: User registered Successfully')


class LoginApiView(APIView):
    """
        User Login :  render login Page and Login user
        :param value: username, password -- Username will be email
        :return:
    """
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'get', 'options']

    def get(self, request):
        if request.user and request.user.is_authenticated:
            token, _ = Token.objects.get_or_create(user=request.user)
            group_list = list(request.user.groups.values_list('name', flat=True))
            if 'admin' in group_list:
                dashboard_url = settings.DASHBOARD_URL
            else:
                dashboard_url = settings.USER_DASHBOARD_URL
            return success_response('User logged in Successfully',
                                    data={'email': request.user.email, 'access_token': token.key, 'url': dashboard_url})

    def post(self, request):
        email = request.data.get("email", None)
        password = request.data.get("password", None)

        if email is None or password is None:
            return error_response('Please provide both username and password.')

        user = authenticate(username=email, password=password)

        if not user:
            return error_response('The username/password is not a valid.')

        if not user.is_active:
            return error_response('Please contact an administrator regarding this account.')

        login(request, user)
        log.debug(f"[auth -> LoginApiView] User {user.email} Login at:{time.time()}")
        token, _ = Token.objects.get_or_create(user=user)
        group_list = list(request.user.groups.values_list('name', flat=True))
        if 'admin' in group_list:
            dashboard_url = settings.DASHBOARD_URL
        elif 'team lead' in group_list:
            dashboard_url = settings.DASHBOARD_URL
        else:
            dashboard_url = settings.USER_DASHBOARD_URL
        return success_response('Login Success: User logged in Successfully',
                                data={'email': user.email, 'access_token': token.key, 'url': dashboard_url})


class LogoutView(APIView):
    """
         User Logout :  Render Logout Page and User logout
        :param value: username, password -- Username will be email
        :return:
    """
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['post', 'options']

    def post(self, request):
        request.user.auth_token.delete()
        log.debug(f"[auth -> LogoutView] User {request.user.email} Logout at:{time.time()}")
        logout(request)
        return success_response("Logout successfully", data={'url': settings.LOGIN_URL})


class UserViewSet(VerboseViewSet, viewsets.ModelViewSet):
    permission_classes = [AllowAny, ]
    authentication_classes = [TokenAuthentication]
    http_method_names = ['get', 'put', 'patch', 'options']
    serializer_class = EmailUserSerializer

    def get_queryset(self):
        user_filter = {"is_active": True, "groups__name": "agent"}
        group = user_group_list(request=self.request)
        if 'team lead' in group.get('group_list'):
            user_id = self.request.user.id
            team_agent = list(EmailUser.objects.filter(team_lead=user_id).values_list('id', flat=True))
            user_filter.update({"id__in": team_agent})
        queryset = EmailUser.objects.filter(**user_filter).all().order_by('first_name')
        return queryset
