"""
Created:          10/07/2020
Post-History:
Title:            This module is intended to provide Webhook Notifications
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import json
import logging
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from core.response import error_response, success_response
from django.db import IntegrityError
from app.models import ClickToCallEventDetail, Customer, PaymentLink
from django.db.models import Q
from rest_framework.response import Response
from django.conf import settings
from datetime import datetime
import hmac
import hashlib


log = logging.getLogger("WebhookViews")
slack_logger = logging.getLogger('django.request')


class C2CEventWebhookView(APIView):
    permission_classes = [AllowAny]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            event = json.loads(request.body.decode("UTF-8"))
            uuid = event.get('UUID', None)
            event_name = event.get('Event-Name', None)
            timestamp = event.get('Timestamp', None)
            file_name = event.get('file_name', None)
            duration = event.get('duration', 0)
            ivr_name = event.get('ivr_name', None)
            call_status = event.get('call_status', None)
            mos = event.get('mos', 0)
            rtp_in_count = event.get('rtp_in_count', 0)
            cause_code = event.get('cause_code', 0)
            hangup_cause = event.get('hangup_cause', None)
            length = event.get('length', 0)
            recording_file = event.get('recording_file', None)

            ClickToCallEventDetail.objects.create(
                uuid_id=uuid,
                event_name=event_name,
                timestamp=timestamp,
                file_name=file_name,
                duration=duration,
                ivr_name=ivr_name,
                call_status=call_status,
                mos=mos,
                rtp_in_count=rtp_in_count,
                cause_code=cause_code,
                hangup_cause=hangup_cause,
                length=length,
                recording_file=recording_file
            )
            return success_response('Successfully Received Event!')
        except IntegrityError as exc:
            log.error("[WebhookViews -> UUID not found ]" + str(exc))
            return error_response('UUID not found!', errors=str(exc), status_code=404)
        except Exception as exc:
            log.error("[WebhookViews -> C2CEventApiView ]" + str(exc))
            return error_response('Error while processed event!', errors=str(exc), status_code=500)


class C2CCallerInfoView(APIView):
    """
        Used to redirecting incoming calls from lenders to agent.
    """
    permission_classes = [AllowAny]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            data = json.loads(request.body.decode("UTF-8"))
            source = data.get('source', None)
            customer = Customer.objects.get(
                Q(contact_number_1=source) | Q(contact_number_2=source) | Q(contact_number_3=source))
            extension = customer.email_user.extension if customer.email_user else settings.C2C_DEFAULT_EXTENSION
            customer_name = customer.name if customer else None
            application_id = customer.application_id if customer else None
            email_user = str(customer.email_user.id) if customer.email_user else None
            response = {
                "msg": "+OK",
                "status_code": "200",
                "agent_extension": extension,
                "caller-name": customer_name,
                "agent-id": email_user,
                "application_id": application_id
            }
        except Customer.DoesNotExist:
            default_extension = settings.C2C_DEFAULT_EXTENSION
            response = {
                "msg": "Not Found",
                "status_code": "404",
                "agent_extension": default_extension,
                "caller-name": "",
                "agent-id": "",
                "application_id": ""
            }
        except Exception as exc:
            log.error("[WebhookViews -> C2CEventApiView ]" + str(exc))
            response = {
                "msg": "-ERR",
                "status_code": "500",
                "agent_extension": "",
                "caller-name": "",
                "agent-id": "",
                "application_id": "",
                "error": str(exc)
            }
        return Response(response)


class RazorpayWebhookView(APIView):
    permission_classes = [AllowAny]
    http_method_names = ['post', 'options']

    def post(self, request):
        try:
            secret_key = settings.RAZORPAY_WEBHOOK_SECRET_KEY
            message = request.body
            received_signature = request.headers.get('X-Razorpay-Signature')
            expected_signature = hmac.new(secret_key.encode('utf-8'), msg=message, digestmod=hashlib.sha256).hexdigest()

            if expected_signature == received_signature:
                event = json.loads(message.decode("UTF-8"))
                data = event.get('payload', {}).get('payment_link', {}).get('entity', {})
                if data:
                    payment_id = data.get('id', None)
                    reference_id = data.get('reference_id', None)
                    payment_status = data.get('status', None)

                    is_update = PaymentLink.objects.filter(payment_id=payment_id, reference_id=reference_id).update(
                        payment_status=payment_status, updated_at=datetime.now())
                    if is_update:
                        response = {
                            'status_code': 200,
                            'message': 'Payment Details Received successfully!',
                        }
                    else:
                        response = {
                            'status_code': 400,
                            'message': 'Error While Update PaymentLink!',
                        }
                else:
                    response = {
                        'status_code': 400,
                        'message': 'Event Data Not Found!',
                    }
            else:
                response = {
                    'status_code': 400,
                    'message': 'Message Authentication Failed!',
                }
                slack_logger.error("Razorpay Webhook Message Authentication Failed!")
        except Exception as exc:
            log.error("[WebhookViews -> RazorpayWebhookView]" + str(exc))
            slack_logger.error("Error While Razorpay Webhook Received!", exc_info=True)
            response = {
                'status_code': 500,
                'message': 'Error While Razorpay Webhook Received!',
            }
        return Response(response, status=response.get('status_code', 200))
