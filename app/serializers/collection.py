"""
Created:          23/06/2020
Post-History:     25/06/2020
Title:            This module is intended to provide Serializers for Collection
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

from rest_framework import serializers
from app.models import *
from app.serializers.user import EmailUserSerializer


class FilteredListSerializer(serializers.ListSerializer):

    def to_representation(self, data):
        data = data.filter(is_active=True)
        return super(FilteredListSerializer, self).to_representation(data)


class DispositionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Disposition
        fields = "__all__"


class SubDispositionSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubDisposition
        fields = "__all__"


class PaymentModeSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentMode
        fields = "__all__"


class SubPaymentModeSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubPaymentMode
        fields = "__all__"


class StateSerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = "__all__"


class PTPTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = PTPType
        fields = "__all__"


class EmiSerializer(serializers.ModelSerializer):

    class Meta:
        model = EMI
        fields = "__all__"


class FollowupSerializer(serializers.ModelSerializer):
    user_for_followup = EmailUserSerializer(many=True, read_only=True)

    class Meta:
        list_serializer_class = FilteredListSerializer
        model = Followup
        fields = "__all__"


class IVRSerializer(serializers.ModelSerializer):

    class Meta:
        model = IVR
        fields = "__all__"


class LeadRecycleSerializer(serializers.ModelSerializer):

    class Meta:
        model = LeadRecycle
        fields = ['month']


class PaymentInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentInfo
        fields = "__all__"


class AgentConfirmedPaymentInfoSerializer(serializers.ModelSerializer):
    payment_mode = PaymentModeSerializer()
    sub_payment_mode = SubPaymentModeSerializer()
    email_user = EmailUserSerializer()

    class Meta:
        model = AgentConfirmedPaymentInfo
        fields = "__all__"


class CustomerSerializer(serializers.ModelSerializer):
    emi_detail = EmiSerializer(many=True, read_only=True)
    followup = FollowupSerializer(many=True, read_only=True)
    ivr = IVRSerializer(many=True, read_only=True)
    lead_recycle = LeadRecycleSerializer(many=True, read_only=True)
    last_status = DispositionSerializer()
    payment_info = PaymentInfoSerializer(many=True, read_only=True)
    agent_confirmed_payment_info = AgentConfirmedPaymentInfoSerializer(many=True, read_only=True)
    email_user = EmailUserSerializer()

    class Meta:
        model = Customer
        fields = "__all__"


class CustomerFollowup(serializers.ModelSerializer):
    emi_detail = EmiSerializer(many=True, read_only=True)

    class Meta:
        model = Customer
        fields = ['application_id', 'name', 'contact_number_1', 'contact_number_2', 'loan_amount', 'emi_amount', 'emi_detail', 'state_name', 'is_notice_issued', 'is_post_card_issued', 'total_followup', 'last_status_date_time']


class CallbackSerializer(serializers.ModelSerializer):
    application_id = CustomerFollowup()

    class Meta:
        model = Followup
        fields = ['callback_date_time', 'application_id']


class ManageLeadSerializer(serializers.ModelSerializer):
    email_user = EmailUserSerializer()

    class Meta:
        model = Customer
        fields = ['application_id', 'email_user', 'email_user_assign_timestamp']


class ListSerializer(serializers.ModelSerializer):
    class Meta:
        model = List
        fields = "__all__"


class FollowupFilterSerializer(serializers.ModelSerializer):
    application_id = CustomerFollowup()

    class Meta:
        model = Followup
        fields = ['disposition', 'application_id']


class PTPSerializer(serializers.ModelSerializer):
    application_id = CustomerFollowup()

    class Meta:
        model = Followup
        fields = ['ptp_date', 'application_id', 'ptp_amount', 'settlement_amount']


class ReportSerializer(serializers.ModelSerializer):
    emi_due = serializers.SerializerMethodField()
    bounce_charges = serializers.SerializerMethodField()
    late_payment_charges = serializers.SerializerMethodField()
    tos = serializers.SerializerMethodField()
    pos = serializers.SerializerMethodField()
    customer_name = serializers.SerializerMethodField()
    email_user = serializers.CharField(source='email_user.first_name', read_only=True)
    last_status = serializers.CharField(source='last_status.name', read_only=True)

    def get_emi_due(self, obj):
        emi = obj.emi_detail.first()
        return emi.emi_due  if emi else ''

    def get_bounce_charges(self, obj):
        emi = obj.emi_detail.first()
        return emi.bounce_charges  if emi else ''

    def get_late_payment_charges(self, obj):
        emi = obj.emi_detail.first()
        return emi.late_payment_charges  if emi else ''

    def get_tos(self, obj):
        emi = obj.emi_detail.first()
        return emi.tos  if emi else ''

    def get_pos(self, obj):
        emi = obj.emi_detail.first()
        return emi.pos  if emi else ''

    def get_customer_name(self, obj):
        return obj.name + ' ' + obj.last_name if obj.last_name != None or obj.last_name != ' ' else obj.name

    class Meta:
        model = Customer
        fields = ['application_id', 'loan_agreement_number','customer_name',
                  'address','alternate_address', 'permanent_address', 'contact_number_1',
                  'contact_number_2','contact_number_3','branch','product',
                  'asset_make','registration_number','loan_amount', 'loan_booking_date', 'loan_maturity_date',
                  'disbursal_date' ,'allocation_date', 'emi_amount',
                  'reference_1_name','reference_1_contact_number', 'reference_1_address', 'reference_2_name',
                  'reference_2_contact_number', 'reference_2_address', 'email_user','email_user_assign_timestamp',
                  'lead_status','last_status', 'branch_id', 'state_name', 'lead_month', 'collection_manager',
                  'collection_manager_contact_number', 'collection_manager_email', 'cluster_manager',
                  'cluster_manager_email', 'state_manager', 'zonal_manager', 'territory_name', 'territory_code', 'bucket',
                  'actual_bucket', 'ncm', 'repayment_mode', 'contact_person', 'agency_name', 'reason_of_bounce',
                  'standard_reason', 'scheme', 'supplier', 'tenure', 'dealer_code', 'gross_loan_to_value', 'net_loan_to_value',
                  'dme_name', 'sales_executive_name', 'sales_executive_code', 'created_at','updated_at','is_active','ivr_status',
                  'emi_due', 'bounce_charges', 'late_payment_charges', 'tos', 'pos']


class SettlementSerializer(serializers.ModelSerializer):

    class Meta:
        model = Settlement
        fields = '__all__'


class ReportFollowupSerializer(serializers.ModelSerializer):
    disposition = serializers.SerializerMethodField()
    sub_disposition = serializers.SerializerMethodField()
    payment_mode = serializers.SerializerMethodField()
    ptp_type = serializers.SerializerMethodField()
    email_user = serializers.SerializerMethodField()

    def get_disposition(self, obj): return obj.disposition.name if obj.disposition else ''
    def get_sub_disposition(self, obj): return obj.sub_disposition.name if obj.sub_disposition else ''
    def get_payment_mode(self, obj): return obj.payment_mode.name if obj.payment_mode else ''
    def get_ptp_type(self, obj): return obj.ptp_type.name if obj.ptp_type else ''
    def get_email_user(self, obj): return obj.email_user.first_name if obj.email_user else ''

    class Meta:
        model = Followup
        fields = [
            'application_id', 'disposition','sub_disposition', 'ptp_amount',
            'ptp_date', 'payment_mode', 'ptp_type', 'settlement_percent', 'settlement_amount', 'remarks',
            'callback_date_time', 'callback_status', 'created_at', 'updated_at', 'is_active', 'email_user'
            ]


class PaymentLinkSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentLink
        fields = '__all__'
