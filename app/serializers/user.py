"""
Created:          25/06/2020
Post-History:
Title:            This module is intended to provide Serializers for User
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import re
from app.models import EmailUser
from rest_framework import serializers
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


def password_validator(password):
    reg = "^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{8,}$"
    pattern = re.compile(reg)
    if not re.search(pattern, password):
        raise ValidationError(
            _("Please enter a password with a minimum 8 digits with 1 Symbol, 1 Capital Letter, and 1 Number."),
            code='password_no_number',
        )
    return password


class UserProfileSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True,  validators=[password_validator])

    class Meta:
        model = EmailUser
        fields = '__all__'

    def create(self, validated_data):
        user = EmailUser.objects.create_user(**validated_data)
        return user


class PasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, validators=[password_validator])

    class Meta:
        model = EmailUser
        fields = ('password',)

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return instance


class EmailUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = EmailUser
        fields = "__all__"
