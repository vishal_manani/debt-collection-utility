"""
Created:          25/06/2020
Post-History:     07/07/2020
Title:            This module is intended to provide helper functions for Instances
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import logging
from collections import defaultdict

from django.db.models import Q
from django.forms import model_to_dict

from app.models import Customer, EMI, LeadRecycle, EmailUser, CampaignSyncSetting, ClickToCallEvent, PaymentInfo, \
    AgentConfirmedPaymentInfo, Settlement, Followup, ClickToCallEventDetail
from core.utils import Dict2Obj, convert_string_to_date
from datetime import datetime, timedelta
from django.conf import settings

log = logging.getLogger("Collection")

FILE_DICT= {"create_update_customer_obj": settings.FILE_FIELDS, "update_agent_lead": settings.ASSIGN_LEAD_FILE_FIELDS,
            "update_payment_info1": settings.PAYMENT_INFO_FILE1_FIELDS,
            "update_payment_info2": settings.PAYMENT_INFO_FILE2_FIELDS,
            "update_settlement_info": settings.SETTLEMENT_FIELDS}
SET_LIST_FUNCTION = ['create_update_customer_obj']


def update_list_count(obj, data):
    """
    Function is used to update File Count
    :param obj:
    :param status:
    :return:
    """
    try:
        obj.t_count = data.t_count
        obj.c_count = data.c_count
        obj.u_count = data.u_count
        obj.save()
    except Exception as exc:
        log.error("[helper -> update_list_count ]" + str(exc))
        raise


def count_obj(status: bool):
    """
    Bifurcate File Count
    :param status:
    :return: Count Object
    """
    c_count, u_count, t_count = 0, 0, 0
    if status:
        c_count = 1
    if not status:
        u_count = 1
    t_count = 1
    return Dict2Obj({"t_count": t_count, "c_count": c_count, "u_count": u_count})


def create_update_customer_obj(user: int, data: dict, error_data, obj=None):
    """
    Create/Update  Customer
    :param data: dict
    :return:
    """
    try:
        applicationId = data.pop('APPLICATION_ID')
        try:
            cs, is_created = Customer.objects.update_or_create(
                            application_id=applicationId,
                            defaults={"loan_agreement_number": data['LOAN_AGREEMENT_NO'],
                                      "name": data['CUSTOMER_NAME'],
                                      "last_name": data['PRIMARY_CUSTOMER_FATHER_NAME'],
                                      "contact_number_1": str(data['CONTACT_NUMBER_1']),
                                      "contact_number_2": str(data['CONTACT_NUMBER_2']),
                                      "address": data['CUSTOMER_ADDDRESS'],
                                      "permanent_address": data['PERMANENT_ADDRESS'],
                                      "branch": data['BRANCH'],
                                      "branch_id": data['BRANCH_ID'],
                                      "product": data['PROD'],
                                      "asset_make": data['ASSET_MAKE'],
                                      "registration_number": data['REGISTRATION_NO'],
                                      "loan_amount": data['LOAN_AMOUNT'],
                                      "emi_amount": data['EMI_AMOUNT'],
                                      "reference_1_name": data['REFERENCE_1_NAME'],
                                      "reference_1_contact_number": str(data['REFERENCE_1_CONTACT_NUMBER']),
                                      "reference_1_address": str(data['REFERENCE_1_ADDRESS']),
                                      "reference_2_name": data['REFERENCE_2_NAME'],
                                      "reference_2_contact_number": str(data['REFERENCE_2_CONTACT_NUMBER']),
                                      "reference_2_address": str(data['REFERENCE_2_ADDRESS']),
                                      "state_name": data['STATE_NAME'],
                                      "lead_month": data['MONTH'],
                                      "collection_manager": data['COLLECTION_MANAGER'],
                                      "collection_manager_contact_number": data['COLLECTION_MANAGER_CONTACT_NUMBER'],
                                      "collection_manager_email": data['COLLECTION_MANAGER_EMAIL'],
                                      "cluster_manager": data['CLUSTER_MANAGER'],
                                      "cluster_manager_email": data['CLUSTER_MANAGER_EMAIL'],
                                      "state_manager": data['STATE_MANAGER'],
                                      "zonal_manager": data['ZONAL_MANAGER'],
                                      "territory_name": data['TERRITORY_NAME'],
                                      "territory_code": data['TERRITORY_CODE'],
                                      "contact_person": data['CONTACT_PERSON'],
                                      "bucket": data['BKT'],
                                      "actual_bucket": data['ACTUAL_BKT'],
                                      "loan_booking_date": data['LOAN_BOOKING_DATE'],
                                      "loan_maturity_date": data['LOAN_MATURITY_DATE'],
                                      "disbursal_date": data['DISBURSAL_DATE'],
                                      "allocation_date": data['ALLOCATION_DATE'],
                                      "ncm": data['NCM'],
                                      "repayment_mode": data['REPAYMENT_MODE'],
                                      "agency_name": data['AGENCY_NAME'],
                                      "reason_of_bounce": data['REASON_OF_BOUNCE'],
                                      "standard_reason": data['STANDARD_REASON'],
                                      "scheme": data['SCHEME'],
                                      "supplier": data['SUPPLIER'],
                                      "tenure": data['TENURE'],
                                      "dealer_code": data['DEALER_CODE'],
                                      "gross_loan_to_value": data['GROSS_LOAN_TO_VALUE'],
                                      "net_loan_to_value": data['NET_LOAN_TO_VALUE'],
                                      "dme_name": data['DME_NAME'],
                                      "sales_executive_name": data['SALES_EXECUTIVE_NAME'],
                                      "sales_executive_code": data['SALES_EXECUTIVE_CODE'],
                                      }
                        )
            EMI.objects.update_or_create(
                application_id_id=applicationId,
                defaults={
                    "due_date": data['DUE_DATE'],
                    "first_emi_due_date": data['FIRST_EMI_DUE_DATE'],
                    "emi_due": data['EMI_DUE'],
                    "bounce_charges": data['BOUNCE_CHARGE_DUE'],
                    "late_payment_charges": data['LPP_DUE'],
                    "tos": data['TOS'],
                    "pos": data['POS'],
                }
            )
            month = datetime.now().strftime('%b')
            LeadRecycle.objects.update_or_create(application_id_id=applicationId, month=month, defaults={"list_id_id": obj})
            obj = count_obj(is_created)
            if not is_created:
                lead_status_change_paid_unconfirmed_to_ptp(application_id=applicationId)
        except:
            obj = Dict2Obj({"t_count": 0, "c_count": 0, "u_count": 0})
            error_data.append(applicationId)
        return obj, error_data
    except Exception as exc:
        log.error("[helper -> create_update_customer_obj ]" + str(exc))
        raise


def update_agent_lead(user: int, data: dict, error_data, obj=None):
    """
    Used to Update Agent Lead
    :param data:
    :return:
    """
    try:
        application_id = data.pop('APPLICATION_ID')
        try:
            user_email = data.pop('AGENT_EMAIL')
            user = EmailUser.objects.get(email=user_email)
            if user:
                Customer.objects.filter(application_id=application_id).update(email_user=user.id,
                                                                              email_user_assign_timestamp=datetime.now())
        except:
            if len(data) > 0:
                error_data.append(application_id)
        return obj, error_data
    except Exception as exc:
        log.error("[helper -> updateAgentLead ]" + str(exc))
        raise


def user_group_list(request):
    group_list = list(request.user.groups.values_list('name', flat=True))
    return {'group_list': group_list}


def user_state_list(request):
    state_list = list(request.user.states.values_list('name', flat=True))
    return state_list


def user_dispositions_list(request):
    dispositions_list = list(request.user.dispositions.values_list('id', flat=True))
    return dispositions_list


def update_ivr_status(obj, data):
    """
    Used to Update IVR Instance with response data
    :param data:
    :return:
    """
    try:
        ClickToCallEvent.objects.create(application_id_id=obj.application_id, phone_number=obj.contact_number_1, uuid=data['UUID'])
        obj.ivr_status = True
        obj.save()
    except Exception as exc:
        log.error("[helper -> update_ivr ]" + str(exc))
        raise


def change_campaign_sync_status(arg: bool):
    """
    Used to sync Campaign.
    :return:
    """
    try:
        cs = CampaignSyncSetting.objects.latest("id")
        cs.sync_in_process = arg
        cs.save()
    except Exception as exc:
        log.error("[helper -> change_campaign_sync_status ]" + str(exc))
        raise


def get_campaign_sync_settings_obj():
    """
    Return Campaign Sync Settings latest Instance
    :return: Instance
    """
    try:
        return CampaignSyncSetting.objects.latest("id")
    except Exception as exc:
        log.error("[helper -> get_campaign_sync_settings_obj ]" + str(exc))
        raise


def get_campaign_sync_status():
    """
    Used to get status of Campaign
    :return:
    """
    try:
        cs = get_campaign_sync_settings_obj()
        return cs.sync_in_process
    except Exception as exc:
        log.error("[helper -> get_campaign_sync_status ]" + str(exc))
        raise


def update_payment_info1(user: int, data: dict, error_data, obj=None):
    """
        Used to Update Payment Info
        :param data:
        :return:
        """
    try:
        application_id = data.pop('App ID / Lon Lan', None)
        user = EmailUser.objects.get(id=user)
        if application_id and application_id != ' ':
            try:
                cs = Customer.objects.get(Q(application_id=application_id) | Q(loan_agreement_number=application_id))
                application_id = cs.application_id
                payment_date = data["Money sent Date"]
                try:
                    payment_date_object = datetime.strptime(payment_date, "%d-%b-%y").strftime("%Y-%m-%d")
                except Exception as err:
                    try:
                        payment_date_object = datetime.strptime(payment_date, "%d %b %y").strftime("%Y-%m-%d")
                    except Exception as err:
                        try:
                            payment_date_object = datetime.strptime(payment_date, "%d.%m.%Y").strftime("%Y-%m-%d")
                        except Exception as err:
                            payment_date_object = payment_date

                payment_obj, created = PaymentInfo.objects.update_or_create(
                    application_id_id=application_id,
                    transaction_id=data["TXN ID"],
                    defaults={
                        "amount": data["Amount"],
                        "payment_mode": data["Mode"],
                        "sub_payment_mode": data["Source"],
                        "payment_date": payment_date_object,
                        "email_user": user
                    }
                )
                if created:
                    lead_status_change_after_payment(application_id=application_id, amount=data["Amount"])
            except:
                if len(data) > 0:
                    error_data.append(application_id)
        return obj, error_data
    except Exception as exc:
        log.error("[helper -> update_payment_info]" + str(exc))
        raise


def update_payment_info2(user: int, data: dict, error_data, obj=None):
    """
        Used to Update Payment Info
        :param data:
        :return:
        """
    try:
        loan_agreement_number = data.pop('Agreement No', None)
        user = EmailUser.objects.get(id=user)

        if len(data) == 0 or ((loan_agreement_number == ' ') or (not loan_agreement_number)) or (loan_agreement_number == ' ') or (not loan_agreement_number):
            return obj, error_data

        if loan_agreement_number and loan_agreement_number != '':
            try:
                cs = Customer.objects.get(loan_agreement_number=loan_agreement_number)
                application_id = cs.application_id

                payment_date = data["Transaction Date"]
                try:
                    payment_date_object = datetime.strptime(payment_date, "%d-%b-%y").strftime("%Y-%m-%d")
                except Exception as err:
                    try:
                        payment_date_object = datetime.strptime(payment_date, "%d %b %y").strftime("%Y-%m-%d")
                    except Exception as err:
                        try:
                            payment_date_object = datetime.strptime(payment_date, "%d.%m.%Y").strftime("%Y-%m-%d")
                        except Exception as err:
                            payment_date_object = payment_date

                payment_obj, created = PaymentInfo.objects.update_or_create(
                    application_id_id=application_id,
                    transaction_id=data["Receipt No."],
                    defaults={
                        "amount": data["Amount"],
                        "payment_mode": data["Instrument Type"],
                        "sub_payment_mode": None,
                        "payment_date": payment_date_object,
                        "email_user": user
                    }
                )
                if created:
                    lead_status_change_after_payment(application_id=application_id, amount=data["Amount"])
            except:
                error_data.append(loan_agreement_number)
        return obj, error_data
    except Exception as exc:
        log.error("[helper -> update_payment_info]" + str(exc))
        raise


def get_payment_info(from_date, to_date):
    pi = PaymentInfo.objects.filter(created_at__range=[from_date, to_date]
                                    ).values('application_id_id', 'amount',
                                             'transaction_id', 'payment_mode',
                                             'sub_payment_mode', 'payment_date',
                                             'email_user__first_name', 'created_at').order_by('created_at', 'application_id')
    return list(pi)


def get_agent_confirmed_payment_info(from_date, to_date):
    ai = AgentConfirmedPaymentInfo.objects.filter(created_at__range=[from_date, to_date]
                                    ).values('application_id_id', 'amount',
                                             'transaction_id', 'payment_mode__name',
                                             'sub_payment_mode__name', 'bank_branch',
                                             'type', 'receipt',
                                             'email_user__first_name', 'created_at').order_by('created_at', 'application_id')
    return list(ai)


def get_settlement(from_date, to_date):
    st = Settlement.objects.filter(created_at__range=[from_date, to_date]
                                    ).values('application_id_id', 'amount',
                                             'is_email_sent', 'is_approved', 'email_user__first_name',
                                             'payment_info__amount', 'payment_info__transaction_id',
                                             'payment_info__payment_mode', 'payment_info__sub_payment_mode',
                                             'payment_info__payment_date',
                                             'is_active', 'updated_at','created_at').order_by('created_at', 'application_id')
    return list(st)


def get_lead_recycle(from_date, to_date):
    lr = LeadRecycle.objects.filter(created_at__range=[from_date, to_date]
                                    ).values('application_id_id', 'month',
                                             'updated_at', 'created_at',
                                             'list_id__name', 'list_id__file_name',
                                             'list_id__created_at', 'list_id__t_count',
                                             'list_id__c_count', 'list_id__u_count').order_by('created_at', 'application_id')
    return list(lr)


def get_click_to_call_event(from_date, to_date):
    ce = ClickToCallEvent.objects.filter(created_at__range=[from_date, to_date]
                                    ).values('uuid', 'application_id_id', 'phone_number',
                                             'extension', 'is_active',
                                             'updated_at', 'created_at',).order_by('created_at', 'application_id', 'uuid')
    return list(ce)


def get_click_to_call_event_details(from_date, to_date):
    data = list()
    ce = ClickToCallEventDetail.objects.filter(created_at__range=[from_date, to_date]).all().order_by('created_at', 'uuid')
    for i in ce:
        pd = model_to_dict(i)
        dt_object = datetime.fromtimestamp(int(i.timestamp))
        pd['created_at'] = i.created_at
        pd['call_timestamp'] = dt_object
        data.append(pd)
    return data


def update_settlement_info(user: int, data: dict, error_data, obj=None):
    """
        Used to Update Settlement Info
        :param data:
        :return:
        """
    try:
        application_id = data.pop('APPLICATION_ID', None)
        approved_date = data.pop('APPROVED_DATE', None)
        if approved_date:
            approved_date = convert_string_to_date(approved_date)
        try:
            Settlement.objects.filter(application_id_id=application_id).update(is_approved=True, approved_date=approved_date, updated_at=datetime.now())
        except:
            if len(data) > 0:
                error_data.append(application_id)
        return obj, error_data
    except Exception as exc:
        log.error("[helper -> update_settlement_info]" + str(exc))
        raise


def generic_file_update_info(user: int, data: dict, error_data, obj=None):
    """
        Used to Upload Generic Info
        :param data:
        :return:
        """
    try:
        application_id = data.pop('APPLICATION_ID', None)
        fields = Customer._meta.fields
        fields_list = [f.name for f in fields]
        data_dict = {}

        for i in data.keys():
            if i in fields_list:
                data_dict.update({i: data[i]})

        if len(data_dict) > 0:
            Customer.objects.filter(application_id=application_id).update(**data_dict)
        else:
            if len(data) > 0:
                error_data.append(application_id)
        return obj, error_data
    except Exception as exc:
        log.error("[helper -> update_settlement_info]" + str(exc))
        raise


def lead_status_change_after_payment(application_id, amount):
    try:
        settlement = Settlement.objects.filter(application_id=application_id).values('amount').last()
        if settlement:
            settlement_amount = int(float(settlement.get('amount', 0)))
            payment_amount = int(float(amount))
            if settlement_amount and settlement_amount == payment_amount:
                c = Customer.objects.filter(application_id=application_id).update(lead_status=0, last_status_id=6)
                ptp_status = Followup.objects.filter(application_id=application_id).update(ptp_status=False, callback_status=False)
    except Exception as exc:
        log.error("[helper -> lead_status_change_after_payment]" + str(exc))


def lead_status_change_paid_unconfirmed_to_ptp(application_id):
    try:
        paid_unconfirmed = Customer.objects.filter(application_id=application_id, last_status=8).exists()
        if paid_unconfirmed:
            ptp_date = datetime.today() + timedelta(days=4)
            week_day = ptp_date.weekday()
            if week_day == 6:
                ptp_date = datetime.today() + timedelta(days=5)

            ptp = Followup.objects.filter(application_id=application_id, disposition_id=1, is_active=True).values().last()
            if ptp:
                followup = Followup.objects.create(
                    application_id_id=ptp.get('application_id_id', None),
                    disposition_id=ptp.get('disposition_id', None),
                    sub_disposition_id=ptp.get('sub_disposition_id', None),
                    ptp_amount=ptp.get('ptp_amount', 0),
                    ptp_date=ptp_date,
                    ptp_status=True,
                    payment_mode_id=ptp.get('payment_mode_id', None),
                    ptp_type_id=ptp.get('ptp_type_id', None),
                    settlement_percent=ptp.get('settlement_percent', None),
                    settlement_amount=ptp.get('settlement_amount', None),
                    remarks=ptp.get('remarks', None),
                    callback_date_time=ptp.get('callback_date_time', None),
                    callback_status=ptp.get('callback_status', None),
                    is_active=True,
                    email_user_id=ptp.get('email_user_id', None),
                )
            else:
                user = Customer.objects.filter(application_id=application_id).values('email_user').last()
                followup = Followup.objects.create(
                    application_id_id=application_id,
                    disposition_id=1,
                    sub_disposition_id=2,
                    ptp_amount=0,
                    ptp_date=ptp_date,
                    ptp_status=True,
                    payment_mode_id=6,
                    ptp_type_id=None,
                    settlement_percent=None,
                    settlement_amount=None,
                    remarks='',
                    callback_date_time=None,
                    callback_status=False,
                    is_active=True,
                    email_user_id=user.get('email_user', None),
                )
            if followup:
                ptp_status = Followup.objects.filter(application_id=application_id).exclude(id=followup.id).update(ptp_status=False)
                customer = Customer.objects.filter(application_id=application_id, last_status=8).update(last_status=1)
    except Exception as exc:
        log.error("[helper -> lead_status_change_paid_unconfirmed_to_ptp]" + str(exc))


def dashboard_date_wise(customer, paid_unconfirmed, from_date, to_date):
    today_data = {}
    total_attempt = Followup.objects.filter(created_at__range=[from_date + ' 00:00:00', to_date + ' 23:59:59'], is_active=1, application_id__in=customer).count()
    today_data.update({'total_attempt': total_attempt})

    unique_attempt_count = 0
    for i in customer:
        attempt = Followup.objects.filter(created_at__range=[from_date + ' 00:00:00', to_date + ' 23:59:59'], is_active=1, application_id_id=i).exists()
        if attempt:
            unique_attempt_count += 1

    today_data.update({'unique_attempt_count': unique_attempt_count})

    connect_count = 0
    for i in customer:
        connect = Followup.objects.filter(created_at__range=[from_date + ' 00:00:00', to_date + ' 23:59:59'],is_active=1, application_id_id=i).filter(
            Q(application_id_id__last_status=1) | Q(application_id_id__last_status=2) | Q(
                application_id_id__last_status=4) | Q(application_id_id__last_status=14) | Q(
                sub_disposition_id=15)).exists()
        if connect:
            connect_count += 1
    today_data.update({'connect_count': connect_count})

    ptp_queryset = Followup.objects.filter(created_at__range=[from_date + ' 00:00:00', to_date + ' 23:59:59'],application_id__in=customer, ptp_status=True, is_active=True).filter(
        ~Q(application_id__in=paid_unconfirmed)).values('application_id', 'ptp_amount', 'settlement_amount',
                                                        'sub_disposition')
    ptp = defaultdict(dict)
    for i in ptp_queryset:
        ptp_amount = i.get('ptp_amount', None)
        settlement_amount = i.get('settlement_amount', None)
        sub_disposition = i.get('sub_disposition', None)
        if sub_disposition == 1:
            ptp_amount = settlement_amount
        if ptp_amount is not None:
            ptp[i.get('application_id')].update({'amount': ptp_amount})

    ptp_amount_value = 0
    for key, value in ptp.items():
        amount = float(value.get('amount', 0))
        ptp_amount_value += amount

    ptp_count = len(ptp)
    today_data.update({'ptp_count': ptp_count, 'ptp_amount_value': round(ptp_amount_value)})
    return today_data
