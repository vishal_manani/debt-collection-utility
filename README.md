# Debt Collection Utility

Debt Collection Utility is crm track followup of customer.

### Getting Started

Python Version: python 3.8.2 

Setup project environment with [virtualenv](https://virtualenv.pypa.io) and [pip](https://pip.pypa.io).


##### *Setup service on local server*

```bash
# Create project directory
$ mkdir /opt/astute

# Change dir to /opt/astute
$ cd /opt/astute

# Create Virtual environment for python 3.8.2 
$ virtualenv venv

# Activate Virtual environment
$ source venv/bin/activate

# Clone Repository
$ git clone https://gitahd.lintelindia.com/jay.d/debt-collection-utility.git

# Change dir to debt-collection-utility
$ cd debt-collection-utility

# Install Requirements
$ pip install -r requirements.txt

# Copy etc/config.yaml to /etc/debt-collection-utility/config.yaml
$ cp etc/config.yaml /etc/debt-collection-utility/config.yaml

# Edit value in config.yaml and save changes, if required
$ nano /etc/debt-collection-utility/config.yaml

# Create database
$ Create database collection, you can use mysql, postgresql, RDS etc

# Django database Migrate
$ python manage.py migrate

# Django Create Superuser
$ python manage.py createsuperuser

# Django runserver on local server
$ python manage.py runserver
```

##### *Setup gunicorn service on production server*

```bash

# Edit collection.py and save, if required (bind = '127.0.0.1:9000') 
$ nano collection.py

# Copy collection.service to /etc/systemd/system/collection.service
$ cp collection.service /etc/systemd/system/collection.service

# Edit collection.service and save, if required
$ nano /etc/systemd/system/collection.service

# systemd reload
$ systemctl daemon-reload

# To start application
$ systemctl start collection

# To stop application
$ systemctl stop collection

# To restart application
$ systemctl restart collection

# Log check
$ tail -f application log file path [/var/log/debt-collection-utility/collection.log]
```