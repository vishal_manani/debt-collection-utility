import operator
from functools import reduce
from django.db.models import Q


def get_search_results(queryset, search_fields, search_term):

    def construct_search(field_name):
        return "%s__icontains" % field_name

    if search_fields and search_term:
        orm_lookups = [construct_search(str(search_field))for search_field in search_fields]
        for bit in search_term.split():
            or_queries = [Q(**{orm_lookup: bit})for orm_lookup in orm_lookups]
            queryset = queryset.filter(reduce(operator.or_, or_queries))

    return queryset
