"""
Created:          23/06/2020
Post-History:     25/06/2020
Title:            This module is intended to provide customize Success/Error Responses
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

from rest_framework import status
from rest_framework.response import Response


def success_response(message, data=None, url=None):
    """
    Return Success Response
    :param message:
    :param data:
    :param url:
    :return:
    """
    response = {"message": message, "type": "+OK", "status_code": status.HTTP_200_OK, "headers": "Success"}
    if data:
        response["data"] = data
    if url:
        response["url"] = url
    return Response(response, status=status.HTTP_200_OK)


def error_response(message, errors=None, status_code=status.HTTP_202_ACCEPTED):
    """
    Return Error Response
    :param message:
    :param data:
    :param url:
    :return:
    """
    response = {"message": message, "status_code": status_code, "type": "-ERR"}
    if errors:
        response["errors"] = errors
    return Response(response, status=status_code)
