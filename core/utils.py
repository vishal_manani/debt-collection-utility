"""
Created:          24/06/2020
Post-History:     24/06/2020
Title:            This module is intended to provide functionality for scientific calculations.
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import logging
from datetime import datetime
from itertools import chain
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework.views import exception_handler
from rest_framework import exceptions, status

log = logging.getLogger("Utils")


class Dict2Obj(object):
    """
    Turns a dictionary into a class
    """
    def __init__(self, dictionary):
        """Constructor"""
        for key in dictionary:
            setattr(self, key, dictionary[key])


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if isinstance(exc, (exceptions.AuthenticationFailed, exceptions.NotAuthenticated)):
        response.status_code = status.HTTP_401_UNAUTHORIZED

    # Now add the HTTP status code to the response.
    if response is not None:
        data = response.data
        errors = {}
        for field, value in data.items():
            if isinstance(value, dict):
                for k, v in value.items():
                    errors[k] = v[0] if isinstance(v, list) else v
                continue
            errors[field] = value[0] if isinstance(value, list) else value
        response.data = {'errors': errors, "type": '-ERR', 'status_code': response.status_code}
    return response


def server_error(request, *args, **kwargs):
    """
    If the DEBUG is True, this method will handle the internal server error,
    And response the json id the request if ajax request else render the page with error msg.
    """
    log.error("Server Error", exc_info=True)
    if request.is_ajax():
        data = {'errors': {'detail': 'Internal Server Error, Please try again later'}}
        return JsonResponse(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    else:
        return render(request, "error500.html", status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def str_to_list(value: str, delimiter: str):
    """
    Converts the given delimiter separated string into list
    :param value: delimiter separated string
    :param delimiter: delimiter char
    :return: List of the delimited values
    """
    return list(map(str.strip, value.split(delimiter)))


def calculate_divisor(data:list):
    """
    Calculate Divisor
    :param data:
    :return:
    """
    divisor = 1
    for i in range(0, int(len(str(len(data))) / 2)):
        divisor *= 10
    return  divisor


def to_dict(instance):
    """
    Used to convert Object To Dictionary
    :param: instance-
    :return: data- dict
    """
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields):
        data[f.name] = f.value_from_object(instance)
    for f in opts.many_to_many:
        data[f.name] = [i.id for i in f.value_from_object(instance)]
    return data


def convert_string_to_date(date:str):
    datetime_object = datetime.strptime(date, '%d-%m-%Y')
    return datetime.strftime(datetime_object, "%Y-%m-%d")


