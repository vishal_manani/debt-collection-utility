import json
import requests
from django.conf import settings


class CampaignApiManager:

    _instance = None

    def __init__(self, base_url: str, action_args: str, caller_id: str, action: str, web_hook_url: str,
                 amd_playback: str, amd: str, vars: str):
        self.base_url = base_url
        self.action_args = action_args
        self.caller_id = caller_id
        self.action = action
        self.web_hook_url = web_hook_url
        self.amd_playback = amd_playback
        self.amd = amd
        self.vars = vars

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = cls(base_url=settings.BASE_URL, action_args=settings.ACTION_ARGS,
                                caller_id=settings.CALLER_ID, action=settings.ACTION,
                                web_hook_url=settings.WEB_HOOK_URL, amd_playback=settings.AMD_PLAYBACK,
                                amd=settings.AMD, vars=settings.VARS)
        return cls._instance

    def _send(self, method, path, content=None, query_params={}, headers={}, cookies=None):

        method = method.upper()

        if 'Content-Type' not in headers:
            headers['Content-Type'] = 'application/json'

        if 'application/json' == headers['Content-Type'] and content is not None:
            content = json.dumps(content).encode('utf-8')

        endpoint = self.base_url + path

        response = requests.request(method, endpoint, params=query_params, data=content, headers=headers)
        return response

    def campaign(self, ph):
        content = {
            "action_args": [self.action_args],
            "caller_id": self.caller_id,
            "ph": ph,
            "action": self.action,
            "web_hook_url": self.web_hook_url,
            "amd_playback": self.amd_playback,
            "amd": self.amd,
            "vars": {"VAR1": [self.vars]}
        }
        return self._send("POST", "/dial", content)
