"""
Created:          19/06/2020
Post-History:
Title:            This module is intended to Custom Viewset.
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import logging
from .utils import custom_exception_handler
from rest_framework.status import  HTTP_200_OK
from rest_framework.response import Response
from django.db import IntegrityError
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

log = logging.getLogger("FileManager")


def get_json(status, response_type, headers, message, serializer=None):
    if serializer is None:
        response_data = {'status_code': status, 'type': response_type, 'headers': headers, 'message': message}
    else:
        response_data = {'status_code': status, 'type': response_type, 'data': serializer.data, 'headers': headers,
                         'message': message}
    return response_data


class VerboseViewSet(object):
    def create(self, request, *args, **kwargs):
        try:
            serializer = super().create(request, *args, **kwargs)
            response_data = get_json(HTTP_200_OK, '+OK', 'Success', 'Created Successfully', serializer)
            return Response(response_data)
        except IntegrityError as exc:
            log.error("[viewsets -> VerboseViewSet]" + str(exc))
            raise custom_exception_handler(exc, request)

    def list(self, request, *args, **kwargs):
        try:
            serializer = super().list(request, *args, **kwargs)
            response_data = get_json(HTTP_200_OK, '+OK', 'Success', 'Listed Successfully', serializer)
            return Response(response_data)
        except IntegrityError as exc:
            log.error("[viewsets -> VerboseViewSet]" + str(exc))
            raise custom_exception_handler(exc, request)

    def retrieve(self, request, *args, **kwargs):
        try:
            serializer = super().retrieve(request, *args, **kwargs)
            response_data = get_json(HTTP_200_OK, '+OK', 'Success', 'Retrieved Successfully', serializer)
            return Response(response_data)
        except IntegrityError as exc:
            log.error("[viewsets -> VerboseViewSet]" + str(exc))
            raise custom_exception_handler(exc, request)

    def update(self, request, *args, **kwargs):
        try:
            serializer = super().update(request, *args, **kwargs)
            response_data = get_json(HTTP_200_OK, '+OK', 'Success', 'Updated Successfully', serializer)
            return Response(response_data)
        except IntegrityError as exc:
            log.error("[viewsets -> VerboseViewSet]" + str(exc))
            raise custom_exception_handler(exc, request)

    def destroy(self, request, *args, **kwargs):
        try:
            serializer = super().destroy(request, *args, **kwargs)
            response_data = get_json(HTTP_200_OK, '+OK', 'Success', 'Destroyed Successfully', serializer)
            return Response(response_data)
        except IntegrityError as exc:
            log.error("[viewsets -> VerboseViewSet]" + str(exc))
            raise custom_exception_handler(exc, request)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


@method_decorator(login_required, name="dispatch")
class ProtectedTemplateView(TemplateView):
    pass
