import json
import requests
from django.conf import settings
from requests.auth import HTTPBasicAuth


class RazorpayApiManager:

    _instance = None

    def __init__(self, base_url, key_id, key_secret):
        self.base_url = base_url
        self.key_id = key_id
        self.key_secret = key_secret

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = cls(base_url=settings.RAZORPAY_BASE_URL, key_id=settings.RAZORPAY_KEY_ID,
                                key_secret=settings.RAZORPAY_KEY_SECRET)
        return cls._instance

    def _send(self, method, path, content=None, query_params={}, headers={}, cookies=None):

        method = method.upper()

        if 'Content-Type' not in headers:
            headers['Content-Type'] = 'application/json'

        if 'application/json' == headers['Content-Type'] and content is not None:
            content = json.dumps(content).encode('utf-8')

        endpoint = self.base_url + path

        response = requests.request(method, endpoint, params=query_params, data=content, headers=headers,
                                    auth=HTTPBasicAuth(self.key_id, self.key_secret))
        return response

    def payment_links(self, amount, reference_id, description, name, contact, email):
        content = {
            "amount": amount,
            "currency": "INR",
            "reference_id": reference_id,
            "description": description,
            "customer": {
                "name": name,
                "contact": contact,
                "email": email
            },
            "notify": {
                "sms": True,
                "email": False
            },
            "reminder_enable": True,
        }
        return self._send("POST", "/payment_links/", content)

    def get_all_payments_link(self):
        return self._send("GET", "/payment_links/")

    def get_payment_link_by_id(self, payment_id):
        url = '/payment_links/{payment_id}'.format(payment_id=payment_id)
        return self._send("GET", url)
