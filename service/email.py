from pynliner import fromString
import threading
import logging
from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
from core.utils import str_to_list

slack_logger = logging.getLogger('django.request')


class EmailThread(threading.Thread):
    def __init__(self, subject, html_content, recipient_list, cc_list):
        self.subject = subject
        self.recipient_list = recipient_list
        self.html_content = html_content
        self.cc = cc_list
        threading.Thread.__init__(self)

    def run(self):
        sender = settings.EMAIL_HOST_USER
        msg = EmailMessage(self.subject, self.html_content, sender, self.recipient_list, cc=self.cc)
        msg.content_subtype = "html"
        msg.send()


def send_mail(subject, to, template_name, context_dict, cc=[]):
    try:
        message = render_to_string(template_name, context_dict)
        body = fromString(message)
        if not isinstance(to, list):
            to = [to, ]
        if not isinstance(cc, list):
            to = [cc, ]
        EmailThread(subject, body, to, cc).start()
    except Exception as error:
        slack_logger.error('Error while Sending email', exc_info=True)


def settlement_mail(data):
    templete = "email/settlement.html"
    cc_email = []
    to_email = data.get('to_email', None)
    cc_email.append(data.get('cc_email', None))
    cc_email_id = str_to_list(settings.SETTLEMENT_CC_EMAIL, settings.DELIMITER)
    cc_email.extend(cc_email_id)
    send_mail("Settlement Approval", to_email, templete, data, cc=cc_email)


def settlement_reminder_mail(data):
    templete = "email/settlement_reminder.html"
    cc_email = []
    to_email = data.get('to_email', None)
    cc_email.append(data.get('cc_email', None))
    cc_email_id = str_to_list(settings.SETTLEMENT_CC_EMAIL, settings.DELIMITER)
    cc_email.extend(cc_email_id)
    send_mail("Status of settlement Approval", to_email, templete, data, cc=cc_email)
