"""
Created:          25/06/2020
Post-History:     07/07/2020
Title:            This module is intended to provide functions for  Synchronous Data Processing.
"""

__version__ = '0.0.1'
__author__ = 'Jeenal Suthar'

import ray
import math
import logging
from core.utils import Dict2Obj
from core.utils import calculate_divisor

log = logging.getLogger("ray")


def synchronous_processing(user, rayFun, data: list, processFunc:str, obj=None):
    """
    Execute ray_fun in parallel
    :param ray_fun:
    :param: data-
    :return:
    """
    start_row = 0
    chunks = list()
    divisor = calculate_divisor(data)

    if divisor > 0:
        skip_record = int(math.ceil(len(data) / divisor))
    else:
        skip_record = 1

    while True:
        stop_row = start_row + skip_record
        user_chunk = data[start_row:stop_row]
        start_row = stop_row

        if len(user_chunk) == 0:
            break
        else:
            chunks.append(rayFun.remote(user, user_chunk, processFunc, obj))
    return ray.get(chunks)


@ray.remote
def file_data_store_in_db(user, chunk:list, func:str, obj=None):
    """
    Create Users and Send Email in Bulk
    :param: chunk-
    :return:
    """
    import django
    django.setup()
    from app.helper import create_update_customer_obj, update_agent_lead, update_payment_info1, update_payment_info2, update_settlement_info, generic_file_update_info
    functions = {"create_update_customer_obj": create_update_customer_obj, "update_agent_lead": update_agent_lead,
                 "update_payment_info1": update_payment_info1, "update_payment_info2": update_payment_info2,
                 "update_settlement_info": update_settlement_info, "generic_file_update_info": generic_file_update_info}
    t_count, c_count, u_count = 0, 0, 0
    error_data = []
    for i in chunk:
        function = functions[func]
        data, error_data = function(user, i, error_data, obj)
        if data:
            t_count += data.t_count
            c_count += data.c_count
            u_count += data.u_count
    return Dict2Obj({"t_count": t_count, "c_count": c_count, "u_count":u_count}), error_data