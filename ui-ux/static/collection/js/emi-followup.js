var app = angular.module('myApp', []);
app.controller('customerCtl', function ($scope, $http, $timeout) {
    $scope.showLoader = false
    $scope.customer = []
    $scope.next_url = null;
    $scope.previous_url = null;
    $scope.search_keyword = null;

    $scope.getCustomer = function(url="/api/emi-followup/?page=1") {
        $scope.showLoader = true;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.customer = [];
                var json_data = angular.fromJson(response.data.data);
                $scope.customer = json_data;
                var next_url = response.data.data.next;
                $scope.next_url = next_url.replace(/^.*\/\/[^\/]+/, '');
                var previous_url = response.data.data.previous;
                $scope.previous_url = previous_url.replace(/^.*\/\/[^\/]+/, '');
                $scope.showLoader = false;
                $.apply();
            })
            setTimeout(function() {
                $scope.showLoader = false;
                $scope.$apply();
            }, 1000);
    }

});