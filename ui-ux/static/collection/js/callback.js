var app = angular.module('myApp', []);
app.controller('customerCtl', function ($scope, $http, $timeout, $filter) {
    $scope.showLoader = false
    $scope.customer = []
    $scope.next_url = null;
    $scope.previous_url = null;
    $scope.search_keyword = null;

    $scope.getCustomer = function(url="/api/callback/?page=1") {
        $scope.showLoader = true;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.customer = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.customer = json_data;
                var next_url = response.data.data.next;
                $scope.next_url = next_url.replace(/^.*\/\/[^\/]+/, '');
                var previous_url = response.data.data.previous;
                $scope.previous_url = previous_url.replace(/^.*\/\/[^\/]+/, '');
                $scope.showLoader = false;
                $.apply();
            })
            setTimeout(function() {
                $scope.showLoader = false;
                $scope.$apply();
            }, 1000);
    }

    var inputChangedPromise;
    $scope.search = function(){
        if(inputChangedPromise){
            $timeout.cancel(inputChangedPromise);
        }
        inputChangedPromise = $timeout($scope.doSearch,1000);
    }

    $scope.doSearch = function(){
        if ($scope.from_date && $scope.to_date){
            var from_dt =  $filter('date')($scope.from_date, "y-MM-dd")
            var to_dt =  $filter('date')($scope.to_date, "y-MM-dd")
            url = "/api/search/callback/?q=" + $scope.search_keyword + "&from_date=" + from_dt + "&to_date=" + to_dt;
        }else{
            url = "/api/search/callback/?q=" + $scope.search_keyword;
        }
        $scope.getCustomer(url);
    }

    $scope.dateRange = function(){
        $scope.search_keyword = '';
        if($scope.from_date && $scope.to_date){
            var from_dt =  $filter('date')($scope.from_date, "y-MM-dd")
            var to_dt =  $filter('date')($scope.to_date, "y-MM-dd")
            url = "/api/callback/?from_date=" + from_dt + "&to_date=" + to_dt;
            $scope.getCustomer(url);
        }else{
            swal("Error", "Please select valid date!", "error");
        }
    }

    $scope.resetDateFilter = function(){
        window.location = '/user/callback/'
    }
});