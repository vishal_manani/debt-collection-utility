var app = angular.module('myApp', []);
app.controller('fileCtl', function ($scope, $http, $filter) {
    $scope.save_message = {"text": ""};
    $scope.showLoader = false
    $scope.files = []

    $scope.report_month = $filter('date')(new Date(), 'MMM');
    $scope.month_list = [
        {'month': 'Jan', 'name': 'January'},
        {'month': 'Feb', 'name': 'February'},
        {'month': 'Mar', 'name': 'March'},
        {'month': 'Apr', 'name': 'April'},
        {'month': 'May', 'name': 'May'},
        {'month': 'Jun', 'name': 'June'},
        {'month': 'Jul', 'name': 'July'},
        {'month': 'Aug', 'name': 'August'},
        {'month': 'Sep', 'name': 'September'},
        {'month': 'Oct', 'name': 'October'},
        {'month': 'Nov', 'name': 'November'},
        {'month': 'Dec', 'name': 'December'},
    ]

	$scope.validateInput = function (val_obj) {
		if (val_obj.$valid) {
			val_obj.$$element.next().hide(100);
		} else {
			if (val_obj.$error.required) {
				val_obj.$$element.next().text("You can't leave this field empty.").show(100);
			} else {
				val_obj.$$element.next().text("Please enter valid input").show(100);
			}
		}
	}

	$scope.doSaveFile = function() {
	        $scope.showLoader = true;
	        var header_cfg = angular.extend({
                'Content-Type': undefined
            }, header_config)
            var file = $scope.files[0]
            var post_data = new FormData();
            post_data.append("file", file);
            $http({
                url: "/api/file-upload/",
                method: "POST",
                data: post_data,
                transformRequest: angular.identity,
                headers: header_cfg,
            }).then(function(result) {
                if (result.data.status_code == 200) {
                    $scope.showLoader = false
					$scope.save_message.text = result.data.message;
					swal({
                        title: "Success!",
                        text: result.data.message,
                        type: "success",
                    }, function() {
                        window.location = '/manage-lead/';
                    });
                } else {
                    $scope.showLoader = false
					$scope.save_message.text = result.data.message;
					swal({
                        title: "Error!",
                        text: result.data.message,
                        type: "error",
                    }, function() {
                        window.location = '/import-export/';
                    });
                }
            }, function (result) {
                $scope.showLoader = false
                $scope.save_message.text = result.data.message;
                swal({
                    title: "Error!",
                    text: result.data.message,
                    type: "error",
                }, function() {
                    window.location = '/import-export/';
                });
			}).then(function() {
               $scope.$apply();
               $scope.showLoader = false;
               $scope.files = [];
            });
    }

    $scope.filechange = function (files) {
        $scope.files.push(files[0]);
    }

    $scope.download_data_dump = function() {
        $("#type").val($scope.report_type);
        $("#month").val($scope.report_month);
        $("#data_dump_csv").submit();
    }

    $scope.report_date_dump = function() {
        if($scope.from_date && $scope.to_date){
            var from_dt =  $filter('date')($scope.from_date, "y-MM-dd")
            var to_dt =  $filter('date')($scope.to_date, "y-MM-dd")
        }else{
            var from_dt =  null
            var to_dt =  null
        }
        $("#report_date").val($scope.report_date_type);
        $("#from_date").val(from_dt);
        $("#to_date").val(to_dt);
        $("#report_date_csv").submit();
    }

    $scope.doSaveCustomerStatusFile = function() {
        $scope.showLoader = true;
        var header_cfg = angular.extend({
            'Content-Type': undefined
        }, header_config)
        var file = $scope.files[0]
        var post_data = new FormData();
        post_data.append("file", file);
        $http({
            url: "/api/update-customer-status/",
            method: "POST",
            data: post_data,
            transformRequest: angular.identity,
            headers: header_cfg,
        }).then(function(result) {
            if (result.data.status_code == 200) {
                $scope.showLoader = false
                $scope.save_message.text = result.data.message;
                swal({
                    title: "Success!",
                    text: result.data.message,
                    type: "success",
                }, function() {
                    window.location = '/import-export/';
                });
            } else {
                $scope.showLoader = false
                $scope.save_message.text = result.data.message;
                swal({
                    title: "Error!",
                    text: result.data.message,
                    type: "error",
                }, function() {
                    window.location = '/import-export/';
                });
            }
        }, function (result) {
            $scope.showLoader = false
            $scope.save_message.text = result.data.message;
            swal({
                title: "Error!",
                text: result.data.message,
                type: "error",
            }, function() {
                window.location = '/import-export/';
            });
        }).then(function() {
           $scope.$apply();
           $scope.showLoader = false;
           $scope.files = [];
        });
}

     $scope.doSaveSettlementFile = function() {
         $scope.showLoader = true;
         var header_cfg = angular.extend({
             'Content-Type': undefined
         }, header_config)
         var file = $scope.files[0]
         var post_data = new FormData();
         post_data.append("file", file);
         $http({
             url: "/api/file-settlement/",
             method: "POST",
             data: post_data,
             transformRequest: angular.identity,
             headers: header_cfg,
         }).then(function(result) {
             if (result.data.status_code == 200) {
                 $scope.showLoader = false
                 $scope.save_message.text = result.data.message;
                 swal({
                     title: "Success!",
                     text: result.data.message,
                     type: "success",
                 }, function() {
                     window.location = '/import-export/';
                 });
             } else {
                 $scope.showLoader = false
                 $scope.save_message.text = result.data.message;
                 swal({
                     title: "Error!",
                     text: result.data.message,
                     type: "error",
                 }, function() {
                     window.location = '/import-export/';
                 });
             }
         }, function (result) {
             $scope.showLoader = false
             $scope.save_message.text = result.data.message;
             swal({
                 title: "Error!",
                 text: result.data.message,
                 type: "error",
             }, function() {
                 window.location = '/import-export/';
             });
         }).then(function() {
            $scope.$apply();
            $scope.showLoader = false;
            $scope.files = [];
         });
     }

     $scope.doSaveGenericFile = function() {
         $scope.showLoader = true;
         var header_cfg = angular.extend({
             'Content-Type': undefined
         }, header_config)
         var file = $scope.files[0]
         var post_data = new FormData();
         post_data.append("file", file);
         $http({
             url: "/api/file-generic/",
             method: "POST",
             data: post_data,
             transformRequest: angular.identity,
             headers: header_cfg,
         }).then(function(result) {
             if (result.data.status_code == 200) {
                 $scope.showLoader = false
                 $scope.save_message.text = result.data.message;
                 swal({
                     title: "Success!",
                     text: result.data.message,
                     type: "success",
                 }, function() {
                     window.location = '/import-export/';
                 });
             } else {
                 $scope.showLoader = false
                 $scope.save_message.text = result.data.message;
                 swal({
                     title: "Error!",
                     text: result.data.message,
                     type: "error",
                 }, function() {
                     window.location = '/import-export/';
                 });
             }
         }, function (result) {
             $scope.showLoader = false
             $scope.save_message.text = result.data.message;
             swal({
                 title: "Error!",
                 text: result.data.message,
                 type: "error",
             }, function() {
                 window.location = '/import-export/';
             });
         }).then(function() {
            $scope.$apply();
            $scope.showLoader = false;
            $scope.files = [];
         });
     }
});