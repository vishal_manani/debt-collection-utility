var app = angular.module('myApp', []);
app.controller('customerCtl', function ($scope, $http, $timeout, $filter) {
    $scope.showLoader = false
    $scope.customer = []
    $scope.files = []

    $scope.payment_type_list = [
        {'id': 'EMI', 'name': 'EMI'},
        {'id': 'Settle', 'name': 'Settle'},
    ]
    $scope.getCustomer = function() {
        $scope.showLoader = true;
        url = "/api/search-application/?search_term=" + $scope.search_term;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.customer = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.customer = json_data;
                $scope.showLoader = false;
                $.apply();
            })
            setTimeout(function() {
                $scope.showLoader = false;
                $scope.$apply();
            }, 1000);
    }

    $scope.savePayment = function(){
        if (!$scope.form.$valid) {
            $.each($scope.form.$error.required, function(k, v) {
                v.$$element.next().text("You can't leave this field empty.").show(100);
            });
            return false;
        } else {
            $scope.showLoader = true;
            if($scope.customer[0].payment_date){
                var payment_dt =  $filter('date')($scope.customer[0].payment_date, "y-MM-dd")
            }else{
                var payment_dt = null
            }
            var post_data = JSON.stringify({
                "application_id": $scope.customer[0].application_id,
                "amount": $scope.customer[0].payment_amount,
                "payment_mode": $scope.customer[0].payment_mode,
                "transaction_id": $scope.customer[0].transaction_id,
                "sub_payment_mode": $scope.customer[0].sub_payment_mode,
                "payment_date": payment_dt,
            });
            $http({
                url: "/api/payment-info/",
                method: "POST",
                data: post_data,
                headers: header_config,
            }).then(function(result) {
                if (result.data.status_code == 200) {
                    $scope.showLoader = false
                    swal({
                        title: "Success!",
                        text: result.data.message,
                        type: "success",
                    }, function() {
                        window.location = "/payment/";
                    });
                } else {
                    $scope.showLoader = false
                    swal("Error", result.data.message, "error")
                }
            }, function (result) {
                $scope.showLoader = false
                swal("Error", result.data.message, "error")
            }).then(function() {
               $scope.showLoader = false;
               $scope.$apply();
            });
        }
    }

    $scope.filechange = function (files) {
        $scope.files.push(files[0]);
    }

    $scope.doSaveFile = function() {
	        $scope.showLoader = true;
	        var header_cfg = angular.extend({
                'Content-Type': undefined
            }, header_config)
            var file = $scope.files[0]
            var post_data = new FormData();
            var file_type=$scope.file_type;
            var url=""
            if (file_type == "file1")
            {
              url="/api/file-payment-info/1/"
            }
            if (file_type == "file2")
            {
              url="/api/file-payment-info/2/"
            }
            post_data.append("file", file);
            $http({
                url: url,
                method: "POST",
                data: post_data,
                transformRequest: angular.identity,
                headers: header_cfg,
            }).then(function(result) {
                if (result.data.status_code == 200) {
                    $scope.showLoader = false
					swal({
                    title: "Success!",
                    text: result.data.message,
                    type: "success",
                    }, function() {
                        window.location = "/payment/";
                    });
                } else {
                    $scope.showLoader = false
					swal({
                        title: "Error!",
                        text: result.data.message,
                        type: "error",
                    }, function() {
                        window.location = '/payment/';
                    });
                }
            }, function (result) {
                $scope.showLoader = false
                swal({
                    title: "Error!",
                    text: result.data.message,
                    type: "error",
                }, function() {
                    window.location = '/payment/';
                });
			}).then(function() {
               $scope.$apply();
               $scope.showLoader = false;
               $scope.files = [];
            });
    }

    $scope.isDecimal = function(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 46 || charCode > 57)) {
            evt.preventDefault();
            return false;
        }
        return true;
    }

    $scope.validateInput = function(val_obj) {
        if (val_obj.$valid) {
            val_obj.$$element.next().hide(100);
        } else {
            if (val_obj.$error.required) {
                val_obj.$$element.next().text("You can't leave this field empty.").show(100);
            } else {
                val_obj.$$element.next().text("Please enter valid input").show(100);
            }
        }
    }

});