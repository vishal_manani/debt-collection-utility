var app = angular.module('myApp', []);
app.controller('loginCtl', function ($scope, $http) {

    $scope.regex = {
		email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	};

	$scope.login_error_message = "";
	$scope.is_login_error = false;

	$scope.doLogin = function () {

		if (!$scope.form.$valid) {
			$.each($scope.form.$error.required, function (k, v) {
				v.$$element.next().text("You can't leave this field empty.").show(100);
			});
			$.each($scope.form.$error.pattern, function (k, v) {
				v.$$element.next().text("Please enter valid input").show(100);
			});
			return false;
		} else {

			var post_data = JSON.stringify({
				"email": $scope.username,
				"password": $scope.password,
			});
			$http({
				url: "/api/login/",
				method: "POST",
				data: post_data,
//				headers: header_config,
			}).then(function (result) {
				if (result.data.status_code == 200) {
				    $scope.login_error_message = ""
					$scope.is_login_error = false;
                    $scope.setCookie('access_token',result.data.data.access_token,1);
					location.href = result.data.data.url;
				} else {
					$scope.login_error_message = result.data.message;
					$scope.is_login_error = true;
					swal("Error", result.data.message, "error")
				}
			}, function (error) {
                $scope.login_error_message = error.data.message;
				$scope.is_login_error = true;
				swal("Error", error.data.message, "error")
			});
		}
	}

	$scope.validateInput = function (val_obj) {
		if (val_obj.$valid) {
			val_obj.$$element.next().hide(100);
		} else {
			if (val_obj.$error.required) {
				val_obj.$$element.next().text("You can't leave this field empty.").show(100);
			} else {
				val_obj.$$element.next().text("Please enter valid input").show(100);
			}
		}
	}

	$scope.password_validation = function(value, val_obj) {
        if(value.length < 8){
            val_obj.$$element.next().text("Please enter minimum 8 character password").show(100);
        }else{
            val_obj.$$element.next().text("").hide(100);
        }
	}

    $scope.setCookie = function (name,value) {
        var expires = "";
        var date = new Date();
        date.setTime(date.getTime() + (3600000*24*14));
        expires = "; expires=" + date.toUTCString();
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }

});