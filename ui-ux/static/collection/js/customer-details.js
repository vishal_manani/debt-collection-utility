var app = angular.module('myApp', []);
app.controller('detailCtl', function ($scope, $http, $location,$filter ) {
    $scope.showLoader = false
    $scope.customer = []
    $scope.deposition_list = []
    $scope.deposition_history_list = []
    $scope.sub_deposition_list = []
    $scope.all_sub_deposition_list = []
    $scope.payment_mode_list = []
    $scope.sub_payment_mode_list = []
    $scope.ptp_type_list = []
    $scope.followup_list = []
    $scope.payment_info_list = []
    $scope.agent_confirmed_payment_info_list = []
    $scope.files = []
    $scope.call_log = []
    $scope.location = $location.absUrl();
    $scope.application_id = getUrlParameter('id');
    $scope.callback_param = getUrlParameter('callback');
    $scope.emi_param = getUrlParameter('emi');
    $scope.user_id = null;
    $scope.is_callback = false;
    $scope.disposition_name = null;
    $scope.sub_disposition_name = null;
    $scope.is_settlement_amount = false;
    $scope.is_ptp_amount = false;
    $scope.show_sub_payment_mode = false;
    $scope.show_branch = false;

    var now_time = $filter('date')(new Date(), 'HH:mm');

    $scope.add_followup = {
            id: null,
            application_id: null,
            disposition: null,
            sub_disposition: null,
            ptp_amount: null,
            ptp_date: null,
            payment_mode: null,
            ptp_type: null,
            settlement_percent: null,
            settlement_amount: null,
            remarks: null,
            add_callback_date: null,
            add_callback_time: now_time,
            ptp_time: now_time,
    }

    $scope.payment_type_list = [
        {'id': 'EMI', 'name': 'EMI'},
        {'id': 'Settle', 'name': 'Settle'},
    ]

    $scope.vehicle_status_list = [
        {'id': 'YES', 'name': 'YES'},
        {'id': 'NO', 'name': 'NO'},
    ]

    $scope.getCustomer = function() {
        $scope.showLoader = true;
        var url = "/api/customer/" + $scope.application_id;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.customer = [];
                var json_data = angular.fromJson(response.data.data);
                $scope.customer = json_data;
                angular.forEach($scope.customer.followup, function(value, key) {
                    $scope.followup_list.push(value)
                });

                angular.forEach($scope.customer.payment_info, function(value, key) {
                    $scope.payment_info_list.push(value)
                });

                angular.forEach($scope.customer.agent_confirmed_payment_info, function(value, key) {
                    $scope.agent_confirmed_payment_info_list.push(value)
                });

                $scope.getDisposition();
                $scope.getPaymentMode();
                $scope.getPTPType();
                $scope.getAllSubDisposition();
                $scope.check_ptp_exists();
                $scope.showLoader = false;
                $.apply();
            })
    }

    function getUrlParameter(param, dummyPath) {
        var sPageURL = dummyPath || $scope.location,
            sURLVariables = sPageURL.split(/[&||?]/),
            res;

        for (var i = 0; i < sURLVariables.length; i += 1) {
            var paramName = sURLVariables[i],
                sParameterName = (paramName || '').split('=');

            if (sParameterName[0] === param) {
                res = sParameterName[1];
            }
        }

        return res;
    }

    $scope.saveFollowup = function() {

        if (!$scope.form.$valid) {
            $.each($scope.form.$error.required, function(k, v) {
                v.$$element.next().text("You can't leave this field empty.").show(100);
            });
            return false;
        } else {
            $scope.showLoader = true;
            if ($scope.add_followup.ptp_date){
                var ptp_dt =  $filter('date')($scope.add_followup.ptp_date, "y-MM-dd")
                var ptp_dt_time =  $filter('date')(ptp_dt + ' ' + $scope.add_followup.ptp_time, "y-MM-dd HH:mm:ss")
                var ptp_status = true;
            }else{
                var ptp_dt_time = null;
                var ptp_status = false;
            }

            if ($scope.add_followup.add_callback_date){
                var callback_dt =  $filter('date')($scope.add_followup.add_callback_date, "y-MM-dd")
                var callback_dt_time =  $filter('date')(callback_dt + ' ' + $scope.add_followup.add_callback_time, "y-MM-dd HH:mm:ss")
                var callback_status = true;
            }else{
                var callback_dt_time = null;
                var callback_status = false;
            }

            var post_data = {
                "application_id": $scope.customer.application_id,
                "disposition": $scope.add_followup.disposition,
                "sub_disposition": $scope.add_followup.sub_disposition,
                "ptp_amount": $scope.add_followup.ptp_amount,
                "ptp_date": ptp_dt_time,
                "ptp_status": ptp_status,
                "payment_mode": $scope.add_followup.payment_mode,
                "ptp_type": $scope.add_followup.ptp_type,
                "settlement_percent": $scope.add_followup.settlement_percent,
                "settlement_amount": $scope.add_followup.settlement_amount,
                "remarks": $scope.add_followup.remarks,
                "email_user": $scope.user_id,
                "callback_date_time": callback_dt_time,
                "callback_status": callback_status,
            }
            $http({
                url: "/api/followup/",
                method: "POST",
                data: post_data,
                headers: header_config,
            }).then(function(result) {
                if (result.data.status_code == 200) {
                    $scope.showLoader = false;
                    if($scope.form.alternate_address.$dirty || $scope.form.contact_number_3.$dirty || $scope.form.vehicle_status.$dirty) {
                        $scope.saveCustomer();
                    }
                    swal({
                        title: "Success!",
                        text: result.data.message,
                        type: "success",
                    }, function() {
                        if($scope.callback_param === 'true'){
                            url = '/user/callback/'
                        }else if($scope.emi_param === 'true/'){
                            url = '/user/emi-followup/'
                        }else{
                            url = '/customer/'
                        }
                        window.location = url
                    });
                } else {
                    $scope.showLoader = false;
                    swal("Error", result.data.message, "error");
                }
            }, function (result) {
                $scope.showLoader = false;
                swal("Error", result.data.message, "error");
            }).then(function() {
               $scope.$apply();
               $scope.showLoader = false;
            });
        }
    }

    $scope.getDisposition = function() {
        $http.get("/api/disposition/", {headers: header_config})
            .then(function(response) {
                $scope.deposition_list = [];
                $scope.deposition_history_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.deposition_list = json_data;
                $scope.deposition_history_list = angular.copy(json_data);
            })
    }

    $scope.getSubDisposition = function() {
        url = "/api/sub-disposition/?q=" + $scope.add_followup.disposition;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.sub_deposition_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.sub_deposition_list = json_data;
            })
    }

    $scope.getAllSubDisposition = function() {
        url = "/api/all-sub-disposition/"
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.all_sub_deposition_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.all_sub_deposition_list = json_data;
            })
    }

    $scope.getPaymentMode = function() {
        $http.get("/api/payment-mode/", {headers: header_config})
            .then(function(response) {
                $scope.payment_mode_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.payment_mode_list = json_data;
            })
    }

    $scope.getSubPaymentMode = function() {
        url = "/api/sub-payment-mode/?q=" + $scope.agent_payment.pt_payment_mode;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.sub_payment_mode_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.sub_payment_mode_list = json_data;
            })
    }

    $scope.getPTPType = function() {
        $http.get("/api/ptp-type/", {headers: header_config})
            .then(function(response) {
                $scope.ptp_type_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.ptp_type_list = json_data;
            })
    }

    $scope.check_ptp_exists = function() {
        var post_data = {
                "application_id": $scope.customer.application_id,
         }

        $http({
            url: "/api/check-ptp-exists/",
            method: "POST",
            data: post_data,
            headers: header_config,
        }).then(function(result) {
            if (result.data.status_code == 200) {
                ptp_exists = result.data.data.ptp_exists
                if(ptp_exists){
                    $scope.deposition_list.splice(0, 1);
                }
            } else {}
        }, function (result) {
        }).then(function() {
           $scope.$apply();
        });
    }

    $scope.isNumber = function(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            evt.preventDefault();
            return false;
        }
        return true;
    }

    $scope.isDecimal = function(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 46 || charCode > 57)) {
            evt.preventDefault();
            return false;
        }
        return true;
    }

    $scope.validateInput = function(val_obj) {
        if (val_obj.$valid) {
            val_obj.$$element.next().hide(100);
        } else {
            if (val_obj.$error.required) {
                val_obj.$$element.next().text("You can't leave this field empty.").show(100);
            } else {
                val_obj.$$element.next().text("Please enter valid input").show(100);
            }
        }
    }

    $scope.saveCustomer = function() {
        var post_data = {
            "application_id": $scope.customer.application_id,
            "alternate_address": $scope.customer.alternate_address,
            "contact_number_3": $scope.customer.contact_number_3,
            "vehicle_status": $scope.customer.vehicle_status,
        }
        url = "/api/customer/" + $scope.customer.application_id + "/"

        $http({
            url: url,
            method: "PATCH",
            data: post_data,
            headers: header_config,
        }).then(function(result) {
            if (result.data.status_code == 200) {
            } else {}
        }, function (result) {
        }).then(function() {
           $scope.$apply();
        });
    }

    $scope.showCallback = function(){
        var keepGoing = true;
        angular.forEach($scope.deposition_list, function(value, key) {
            if(keepGoing){
                if (value.id == $scope.add_followup.disposition) {
                    $scope.disposition_name = value.name;
                    if(value.name == 'Callback'){
                        $scope.is_callback = true;
                    }else{
                        $scope.is_callback = false;
                    }
                    keepGoing = false;
                }else{
                    $scope.is_callback = false;
                    keepGoing = true;
                }
            }
        });
    }

    $scope.calculatePercentage = function(){
        if(typeof($scope.add_followup.settlement_amount) != 'undefined' && $scope.add_followup.settlement_amount != 0 && $scope.add_followup.settlement_amount != null && $scope.customer.emi_detail[0].pos !=0 && $scope.customer.emi_detail[0].tos !=0){
            var percentage = ((parseInt($scope.add_followup.settlement_amount) / parseInt($scope.customer.emi_detail[0].pos)) * 100).toFixed(0)
            $scope.add_followup.settlement_percent = percentage

            var percentage_tos = ((parseInt($scope.add_followup.settlement_amount) / parseInt($scope.customer.emi_detail[0].tos)) * 100).toFixed(0)
            $scope.add_followup.add_settlement_percent_tos = percentage_tos
        }else{
            $scope.add_followup.settlement_percent = 0
            $scope.add_followup.add_settlement_percent_tos = 0
        }
    }

    $scope.fieldShowHide = function(sub_disposition){
        sub_disposition_name = $scope.getSubDispositionName(sub_disposition)
        if(sub_disposition_name == "Ready to Settle"){
            $scope.is_settlement_amount = true;
        }else{
            $scope.is_settlement_amount = false;
        }

        if(sub_disposition_name == "Ready to Pay EMI"){
            $scope.is_ptp_amount = true;
        }else{
            $scope.is_ptp_amount = false;

        }
    }

    $scope.getSubDispositionName = function(id){
        var keepGoing = true;
        var sub_disposition_name = null;
        angular.forEach($scope.sub_deposition_list, function(value, key) {
            if(keepGoing){
                if (value.id == id) {
                    sub_disposition_name = value.name;
                    keepGoing = false;
                }else{
                    keepGoing = true;
                }
            }
        });
        return sub_disposition_name
    }

    $scope.click_to_call = function(contact_number) {
        $scope.showLoader = true;
        if (contact_number.length >=7 && contact_number.length <=12 && contact_number != null){
            var post_data = {
                "application_id": $scope.customer.application_id,
                "contact_number": contact_number,
            }
            $http({
                url: "/api/c2c/",
                method: "POST",
                data: post_data,
                headers: header_config,
            }).then(function(result) {
                if (result.data.status_code == 200) {
                    $scope.showLoader = false;
                } else {
                    $scope.showLoader = false;
                    swal("Error", result.data.message, "error");
                }
            }, function (result) {
                $scope.showLoader = false;
                swal("Error", result.data.message, "error");
            }).then(function() {
               $scope.showLoader = false;
               $scope.$apply();
            });

        }else{
            swal("Error", "Number is invalid!", "error");
        }
    }

    $scope.savePayment = function(){
        if (!$scope.agent_payment_form.$valid) {
            $.each($scope.agent_payment_form.$error.required, function(k, v) {
                v.$$element.next().text("You can't leave this field empty.").show(100);
            });
            return false;
        } else {
            $scope.showLoader = true;
	        var header_cfg = angular.extend({
                'Content-Type': undefined
            }, header_config)
            var file = $scope.files[0]
            var post_data = new FormData();
            post_data.append("file", file);
            post_data.append("application_id", $scope.customer.application_id);
            post_data.append("amount", $scope.agent_payment.pt_amount);
            post_data.append("payment_mode", $scope.agent_payment.pt_payment_mode);
            if($scope.agent_payment.pt_sub_payment_mode){
                post_data.append("sub_payment_mode", $scope.agent_payment.pt_sub_payment_mode);
            }
            if ($scope.agent_payment.pt_branch){
                post_data.append("bank_branch", $scope.agent_payment.pt_branch);
            }
            post_data.append("transaction_id", $scope.agent_payment.pt_transaction_id);
            post_data.append("type", $scope.agent_payment.pt_payment_type);
            $http({
                url: "/api/agent-confirmed-payment-info/",
                method: "POST",
                data: post_data,
                transformRequest: angular.identity,
                headers: header_cfg,
            }).then(function(result) {
                if (result.data.status_code == 200) {
                    $("#paymentModal").modal("hide")
                    $scope.showLoader = false
                    swal({
                        title: "Success!",
                        text: result.data.message,
                        type: "success",
                    }, function() {
                        window.location = "/customer/";
                    });
                } else {
                    $("#paymentModal").modal("hide")
                    $scope.showLoader = false
                    swal("Error", result.data.message, "error")
                }
            }, function (result) {
                $("#paymentModal").modal("hide")
                $scope.showLoader = false
                swal("Error", result.data.message, "error")
            }).then(function() {
               $("#paymentModal").modal("hide")
               $scope.showLoader = false;
               $scope.$apply();
            });
        }
    }

    $scope.filechange = function (files) {
        $scope.files.push(files[0]);
    }

    $scope.show_sub_payment_info = function(){
        if($scope.agent_payment.pt_payment_mode == 4 || $scope.agent_payment.pt_payment_mode == 1){
            $scope.show_sub_payment_mode = true;
        }else{
            $scope.show_sub_payment_mode = false;
        }
    }

    $scope.show_branch_info = function(){
        if($scope.agent_payment.pt_payment_mode == 4){
            $scope.show_branch = true;
        }else{
            $scope.show_branch = false;
        }
    }

    $scope.getClickToCallLog = function(){
        $scope.showLoader = true;
        var post_data = {
            "application_id": $scope.customer.application_id,
        }
        $http({
            url: "/api/c2c/log/application-id/",
            method: "POST",
            data: post_data,
            headers: header_config,
        }).then(function(result) {
            if (result.data.status_code == 200) {
                $scope.call_log = []
                var json_data = angular.fromJson(result.data.data);
                $scope.call_log = json_data;
                $("#call_log_Modal").modal("show")
                $scope.showLoader = false
            } else {
                $("#call_log_Modal").modal("hide")
                $scope.showLoader = false
                swal("Error", result.data.message, "error")
            }
        }, function (result) {
            $("#call_log_Modal").modal("hide")
            $scope.showLoader = false
            swal("Error", result.data.message, "error")
        }).then(function() {
           $scope.showLoader = false;
           $scope.$apply();
        });
    }

    $scope.is_less_than_zero = function(val_obj) {
        console.log(val_obj)
        if(val_obj.$valid){
            if (val_obj.$modelValue <= 0) {
                val_obj.$$element.next().text("Please enter valid amount").show(100);
            } else {
                val_obj.$$element.next().hide(100);
            }
            $scope.$apply();
        }
    }

});