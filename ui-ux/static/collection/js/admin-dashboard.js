var app = angular.module('myApp', []);
app.controller('admin_dashboardCtl', function ($scope, $http, $location,$filter ) {
    $scope.showLoader = false
    $scope.followup_data = []
    $scope.state_list = []
    $scope.user_list = []

    $scope.month_list = [
        {'id': '01', 'name': 'Jan'},
        {'id': '02', 'name': 'Feb'},
        {'id': '03', 'name': 'Mar'},
        {'id': '04', 'name': 'Apr'},
        {'id': '05', 'name': 'May'},
        {'id': '06', 'name': 'Jun'},
        {'id': '07', 'name': 'Jul'},
        {'id': '08', 'name': 'Aug'},
        {'id': '09', 'name': 'Sep'},
        {'id': '10', 'name': 'Oct'},
        {'id': '11', 'name': 'Nov'},
        {'id': '12', 'name': 'Dec'},
    ]
    var current_month = $filter('date')(new Date(), 'MM');
    $scope.month = current_month

    $scope.getDashboard = function(url="/api/admin-dashboard/") {
        $scope.showLoader = true;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.followup_data = [];
                var json_data = angular.fromJson(response.data.data);
                $scope.followup_data = json_data;
                $scope.showLoader = false;
                $.apply();
            })
            $scope.getState();
            $scope.getUser();
    }

    $scope.GetCount = function(){
        if($scope.from_date && $scope.to_date){
            var from_dt =  $filter('date')($scope.from_date, "y-MM-dd")
            var to_dt =  $filter('date')($scope.to_date, "y-MM-dd")
            url = "/api/admin-dashboard/?from_date=" + from_dt + "&to_date=" + to_dt;
            $scope.getDashboard(url);
        }else{
            swal("Error", "Please select valid date!", "error");
        }
    }

    $scope.resetDateFilter = function(){
        window.location = '/dashboard/'
    }

    $scope.getState = function(){
        $http.get("/api/state/", {headers: header_config})
            .then(function(response) {
                $scope.state_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.state_list = json_data;
                $.apply();
            })
    }

    $scope.getDashboardByState = function(){
        url = '/api/admin-dashboard/?state_name=' + $scope.state_name + '&month=' + $scope.month
        $scope.getDashboard(url)
    }

    $scope.getDashboardByMonth = function(){
        url = '/api/admin-dashboard/?month=' + $scope.month + '&state_name=' + $scope.state_name
        $scope.getDashboard(url)
    }

    $scope.getUser = function(){
        $http.get("/api/email-user", {headers: header_config})
            .then(function(response) {
                $scope.user_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.user_list = json_data;
                $.apply();
            })
    }

    $scope.getDashboardByResource = function() {
        $scope.showLoader = true;
        url="/api/user-dashboard/?user=" + $scope.user
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.resource_data = [];
                var json_data = angular.fromJson(response.data.data);
                $scope.resource_data = json_data;
                $scope.showLoader = false;
                $.apply();
            })
    }
});