var app = angular.module('myApp', []);
app.controller('customerCtl', function ($scope, $http, $timeout, $filter) {
    $scope.showLoader = false
    $scope.customer = []
    $scope.settlement_list = []
    $scope.files = []

    $scope.getCustomer = function() {
        $scope.showLoader = true;
        $scope.is_search_error = false;
        url = "/api/search-application/?search_term=" + $scope.c_search_term;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.customer = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.customer = json_data;
                if($scope.customer.length == 0){
                    $scope.is_search_error = true;
                }
                $scope.showLoader = false;
                $.apply();
            })
            setTimeout(function() {
                $scope.showLoader = false;
                $scope.$apply();
            }, 1000);
    }

    $scope.saveSettlement = function(){
        if (!$scope.form.$valid) {
            $.each($scope.form.$error.required, function(k, v) {
                v.$$element.next().text("You can't leave this field empty.").show(100);
            });
            return false;
        } else {
            $scope.showLoader = true;

            var post_data = JSON.stringify({
                "application_id": $scope.customer[0].application_id,
                "amount": $scope.customer[0].settlement_amount,
            });
            $http({
                url: "/api/settlement-info/",
                method: "POST",
                data: post_data,
                headers: header_config,
            }).then(function(result) {
                if (result.data.status_code == 200) {
                    $scope.showLoader = false
                    swal({
                        title: "Success!",
                        text: result.data.message,
                        type: "success",
                    }, function() {
                        window.location = "/settlement/";
                    });
                } else {
                    $scope.showLoader = false
                    swal("Error", result.data.message, "error")
                }
            }, function (result) {
                $scope.showLoader = false
                swal("Error", result.data.message, "error")
            }).then(function() {
               $scope.showLoader = false;
               $scope.$apply();
            });
        }
    }

    $scope.filechange = function (files) {
        $scope.files.push(files[0]);
    }

    $scope.doSaveFile = function() {
	        $scope.showLoader = true;
	        var header_cfg = angular.extend({
                'Content-Type': undefined
            }, header_config)
            var file = $scope.files[0]
            var post_data = new FormData();
            post_data.append("file", file);
            $http({
                url: "/api/file-payment-info/",
                method: "POST",
                data: post_data,
                transformRequest: angular.identity,
                headers: header_cfg,
            }).then(function(result) {
                if (result.data.status_code == 200) {
                    $scope.showLoader = false
					swal({
                    title: "Success!",
                    text: result.data.message,
                    type: "success",
                    }, function() {
                        window.location = "/payment/";
                    });
                } else {
                    $scope.showLoader = false
					swal({
                        title: "Error!",
                        text: result.data.message,
                        type: "error",
                    }, function() {
                        window.location = '/payment/';
                    });
                }
            }, function (result) {
                $scope.showLoader = false
                swal({
                    title: "Error!",
                    text: result.data.message,
                    type: "error",
                }, function() {
                    window.location = '/payment/';
                });
			}).then(function() {
               $scope.$apply();
               $scope.showLoader = false;
               $scope.files = [];
            });
    }

    $scope.isDecimal = function(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 46 || charCode > 57)) {
            evt.preventDefault();
            return false;
        }
        return true;
    }

    $scope.validateInput = function(val_obj) {
        if (val_obj.$valid) {
            val_obj.$$element.next().hide(100);
        } else {
            if (val_obj.$error.required) {
                val_obj.$$element.next().text("You can't leave this field empty.").show(100);
            } else {
                val_obj.$$element.next().text("Please enter valid input").show(100);
            }
        }
    }

    $scope.getSettlementList = function(url='/api/settlement-info/') {
        $scope.showLoader = true;
//        url = "/api/settlement-info/"
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.settlement_list = [];
                var json_data = angular.fromJson(response.data.data);
                $scope.settlement_list = json_data;
                $scope.showLoader = false;
                $.apply();
            })
            setTimeout(function() {
                $scope.showLoader = false;
                $scope.$apply();
            }, 1000);
    }

    $scope.doFilter = function(){
        url = "/api/settlement-info/?filter=" + $scope.filter_by
        $scope.getSettlementList(url)
    }

    var inputChangedPromise;
    $scope.search = function(){
        if(inputChangedPromise){
            $timeout.cancel(inputChangedPromise);
        }
        inputChangedPromise = $timeout($scope.doSearch,1000);
    }

    $scope.doSearch = function(){
        url = "/api/settlement-info/?q=" + $scope.search_keyword;
        $scope.getSettlementList(url);
    }
});