//var logout_app = angular.module('logoutApp', []);
//logout_app.controller('logoutCtl', function ($scope, $http) {
//
//    $scope.doLogout = function () {
//        var post_data = JSON.stringify({});
//        $http({
//            url: "/api/logout/",
//            method: "POST",
//            data: post_data,
//			headers: header_config,
//        }).then(function (result) {
//            if (result.data.status_code == 200) {
//                $scope.deleteCookie('access_token');
//                location.href = result.data.data.url;
//            } else {
//                swal("Error", result.data.message, "error")
//            }
//        }, function (error) {
//            swal("Error", error.data.message, "error")
//        });
//	}
//
//	$scope.deleteCookie = function (name,value="",days=-1) {
//        var expires = "";
//        if (days) {
//            var date = new Date();
//            date.setTime(date.getTime() + (days*24*60*60*1000));
//            expires = "; expires=" + date.toUTCString();
//        }
//        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
//    }
//
//
//});


function doLogout(){
    $.ajax({
        url : "/api/logout/",
        type: "POST",
        data : {},
        headers: header_config,
        success: function(data, textStatus, jqXHR)
        {
            deleteCookie('access_token');
            location.href = data.data.url;
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal("Error", errorThrown, "error")
        }
    });
}

function deleteCookie(name,value="",days=-1) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function startTime() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var y = today.getFullYear();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('clock').innerHTML =
  dd + "/" + mm + "/" + y + " " + h + ":" + m + ":" + s;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}