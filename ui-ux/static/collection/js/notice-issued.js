var app = angular.module('myApp', []);
app.controller('customerCtl', function ($scope, $http, $timeout) {
    $scope.showLoader = false
    $scope.customer = []
    $scope.next_url = null;
    $scope.previous_url = null;
    $scope.search_keyword = null;
    $scope.deposition_list = null;

    $scope.getCustomer = function(url="/api/filter-notice-postcard/?filter_by=is_notice_issued") {
        $scope.showLoader = true;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.customer = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.customer = json_data;
                var next_url = response.data.data.next;
                $scope.next_url = next_url.replace(/^.*\/\/[^\/]+/, '');
                var previous_url = response.data.data.previous;
                $scope.previous_url = previous_url.replace(/^.*\/\/[^\/]+/, '');
                $scope.showLoader = false;
                $.apply();
            })
            setTimeout(function() {
                $scope.showLoader = false;
                $scope.$apply();
            }, 1000);
    }


    var inputChangedPromise;
    $scope.search = function(){
        if(inputChangedPromise){
            $timeout.cancel(inputChangedPromise);
        }
        inputChangedPromise = $timeout($scope.doSearch,1000);
    }

    $scope.doSearch = function(){
        url = "/api/filter-notice-postcard/?filter_by=is_notice_issued&search=" + $scope.search_keyword;
        $scope.getCustomer(url);
    }

    $scope.getDisposition = function() {
        $http.get("/api/disposition-filter/", {headers: header_config})
            .then(function(response) {
                $scope.deposition_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.deposition_list = json_data;
            })
    }

    $scope.doFilter = function(filter_by){
        if(filter_by != null){
            url = "/api/filter-notice-postcard/?filter_by=is_notice_issued&disposition=" + filter_by;
        }else{
            url = "/api/filter-notice-postcard/?filter_by=is_notice_issued";
        }
        $scope.getCustomer(url);
    }

});