var app = angular.module('myApp', []);
app.controller('customerCtl', function ($scope, $http, $timeout) {
    $scope.showLoader = false
    $scope.customer = []
    $scope.user_list = []
    $scope.files = []
    $scope.next_url = null;
    $scope.previous_url = null;
    $scope.search_keyword = null;
    $scope.application_id_list = [];
    $scope.is_disable = true;
    $scope.check_all = false;

    $scope.getCustomer = function(url="/api/manage-lead/?page=1") {
        $scope.showLoader = true;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.customer = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.customer = json_data;
                var next_url = response.data.data.next;
                $scope.next_url = next_url.replace(/^.*\/\/[^\/]+/, '');
                var previous_url = response.data.data.previous;
                $scope.previous_url = previous_url.replace(/^.*\/\/[^\/]+/, '');
                $scope.showLoader = false;
                $.apply();
            })
            setTimeout(function() {
                $scope.showLoader = false;
                $scope.$apply();
            }, 1000);
            $scope.getUser();
            $scope.check_all = false;
    }

    $scope.toggle_check_all = function(abc) {
        if ($scope.check_all) {
            $(".toggle-check").prop("checked", true);
        } else {
            $(".toggle-check").prop("checked", false);
        }
        var len = $(".toggle-check:checked").length;
        if (len) {
            $scope.is_disable = false;
        } else {
            $scope.is_disable = true;
        }
    };

    $scope.toggle_check = function(m_id) {
        var len = $(".toggle-check:checked").length;
        if (len) {
            $scope.is_disable = false;
        } else {
            $scope.is_disable = true;
        }
        if (len == $(".toggle-check").length)
            $scope.check_all = true;
        else
            $scope.check_all = false;
    };

    $scope.getUser = function(){
        $http.get("/api/email-user", {headers: header_config})
            .then(function(response) {
                $scope.user_list = [];
                var json_data = angular.fromJson(response.data.data.results);
                $scope.user_list = json_data;
                $.apply();
            })
    }

    $scope.assignUser = function(){
        $scope.showLoader = true;
        $scope.is_disable = true;
        var application_id_list = [];
        $(".toggle-check:checked").each(function() {
           application_id_list.push($(this).data("w_id"))
        });

        var post_data = JSON.stringify({
            "application_id_list": application_id_list,
            "user_id": $scope.user,
        });

        $http({
            url: "/api/assign-lead/",
            method: "POST",
            data: post_data,
            headers: header_config,
        }).then(function(result) {
            if (result.data.status_code == 200) {
                $scope.showLoader = false
                swal({
                    title: "Success!",
                    text: result.data.message,
                    type: "success",
                }, function() {
                    window.location = "/manage-lead/";
                });
            } else {
                $scope.showLoader = false
                swal("Error", result.data.message, "error")
            }
        }, function (result) {
            $scope.showLoader = false
            swal("Error", result.data.message, "error")
        }).then(function() {
           $scope.showLoader = false;
           $scope.$apply();
        });

    }

    var inputChangedPromise;
    $scope.search = function(){
        if(inputChangedPromise){
            $timeout.cancel(inputChangedPromise);
        }
        inputChangedPromise = $timeout($scope.doSearch,1000);
    }

    $scope.doSearch = function(){
        url = "/api/manage-lead/?q=" + $scope.search_keyword;
        $scope.getCustomer(url);
    }

    $scope.filechange = function (files) {
        $scope.files.push(files[0]);
    }

    $scope.doSaveFile = function() {
	        $scope.showLoader = true;
	        var header_cfg = angular.extend({
                'Content-Type': undefined
            }, header_config)
            var file = $scope.files[0]
            var post_data = new FormData();
            post_data.append("file", file);
            $http({
                url: "/api/file-assign-lead/",
                method: "POST",
                data: post_data,
                transformRequest: angular.identity,
                headers: header_cfg,
            }).then(function(result) {
                if (result.data.status_code == 200) {
                    $scope.showLoader = false
					swal({
                    title: "Success!",
                    text: result.data.message,
                    type: "success",
                    }, function() {
                        window.location = "/manage-lead/";
                    });
                } else {
                    $scope.showLoader = false
					swal({
                        title: "Error!",
                        text: result.data.message,
                        type: "error",
                    }, function() {
                        window.location = '/manage-lead/';
                    });
                }
            }, function (result) {
                $scope.showLoader = false
                swal({
                    title: "Error!",
                    text: result.data.message,
                    type: "error",
                }, function() {
                    window.location = '/manage-lead/';
                });
			}).then(function() {
               $scope.$apply();
               $scope.showLoader = false;
               $scope.files = [];
            });
    }

});