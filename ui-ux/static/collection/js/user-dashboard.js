var app = angular.module('myApp', []);
app.controller('user_dashboardCtl', function ($scope, $http, $location,$filter ) {
    $scope.showLoader = false
    $scope.followup_data = []

    $scope.getDashboard = function(url="/api/user-dashboard/") {
        $scope.showLoader = true;
        $http.get(url, {headers: header_config})
            .then(function(response) {
                $scope.followup_data = [];
                var json_data = angular.fromJson(response.data.data);
                $scope.followup_data = json_data;
                $scope.showLoader = false;
                $.apply();
            })
    }

    $scope.GetCount = function(){
        if($scope.from_date && $scope.to_date){
            var from_dt =  $filter('date')($scope.from_date, "y-MM-dd")
            var to_dt =  $filter('date')($scope.to_date, "y-MM-dd")
            url = "/api/user-dashboard/?from_date=" + from_dt + "&to_date=" + to_dt;
            $scope.getDashboard(url);
        }else{
            swal("Error", "Please select valid date!", "error");
        }
    }

    $scope.resetDateFilter = function(){
        window.location = '/user/dashboard/'
    }

});